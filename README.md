# Athanasios Markou Diploma Thesis - CSLAB

## Performance Modelling of High-Intensity Applications on Heterogeneous Architectures

Our main goal is to develop a model that predicts whether
or not an OpenMP application would benefit from an OpenMP-to-CUDA
transformation. Such a prediction is especially valuable to CUDA developers
as they can evaluate the trade-off between potential speedup and application
development effort.


## Introduction

### 1. What is the problem?

In the fields of HPC and Datacenter Computing researchers come across a crucial dilemma when developing
their parallel applications. The dilemma is whether their application should be targeted for a CPU-architecture or
a GPU-architecture. A common strategy for researchers is to first implement an OpenMP version of their application 
to have a quantitive estimation of how well their algorithm leverages from the use of a multithreaded CPU-node. 
If the performance attained is not sufficient one has to reconsider his/her approach. Tuning perfectly your OpenMP code 
to utilize special architectural features of the target CPU-node might not always be the solution that yields the optimal 
performance, maybe another architecture is more suitable for leveraging from the application's scalability and 
thus yielding the optimal performance. 

Since the modern landscape of HPC and Datacenter Computing has drastically changed thanks to cloud server vendors (Amazon, Google,
Microsoft, etc), researchers are now able to develop and run their applications on a wide variety of different CPUs and different GPUs 
(GCP for example offers: [cpus](https://cloud.google.com/compute/docs/machine-types), [gpus](https://cloud.google.com/compute/docs/gpus))
.

So the above question/dilemma can be stated as: ***"Given an OpenMP implementation of an application and the available CPUs
and GPUs, is it worth proceeding to an OpenMP-to-CUDA transformation? What order of magnitude of performance improvement should one 
expect from such a transformation?"*** 
>The problem becomes significantly more complex when you have a wide variety of available CPUs and GPUs.  
---

### 2. Motivation 

For complicated applications OpenMP-to-CUDA transformation is not a straightforward procedure, on the opposite it is proven to be 
a time-consuming and rather demanding procedure. To attain optimal performance the programmer has to have a solid background on 
CUDA API and in-depth knowledge of the target GPU's architercture. 

Providing a **performance and power prediction** as an answer to the aforementioned dilemma is essential. One has to take 
into account that selecting the proper target-architecture vitally affects a number of factors closely related to the 
researchers' workflow. 
More specifically:
- coding effort
- achieved execution time
- financial cost of using a specific node ($/hour in cloud servers: AWS, GCP, Azure, etc.)
- power consumption of the application (in cases where power efficiency is essential)
---

### 3. Challenges 

#### i. Software Diversity

Parallel high-intensity applications may vary significantly depending on the scientific field they are targeted for. This diversity 
highlights the need of a model that is aware of those variations.   

#### ii. Hardware Diversity 

It is critical to understand that a pure hardware-agnostic CPU-to-GPU comparison for an application (based only on 
microarchitecture-independent metrics measured for the application) is not a thorough approach. This becomes apparent
if we consider the following:
>Assume there is an *application* that is implemented both on OpenMP and CUDA. A highly likely scenario is that the *application* 
>performs better on GPU-node-A than on CPU-node-A. At the same time there is CPU-node-B (different generation from CPU-node-A) where our 
>application performs better than GPU-node-A. Thus the answer to the question "what should be my target architecture?" is not simply
> "CPU" or "GPU", we have to be specific. 
---

### 4. Related Work

There is published work on this specific topic:

1. "Should I Use a GPU? Predicting GPU Performance from CPU Runs" by Ioana Baldini, Stephen J. Fink, Erik Altman, [here](https://dominoweb.draco.res.ibm.com/reports/rc25487.pdf)

2. "Cross-Architecture Performance Prediction (XAPP) Using CPU Code to Predict GPU Performance" by Newsha Ardalani, Clint Lestourgeon,
Karthikeyan Sankaralingam, Xiaojin Zhu, [here](https://www.researchgate.net/publication/299408303_Cross-architecture_performance_prediction_XAPP_using_CPU_code_to_predict_GPU_performance)

There are two problems with the above approches:

The first problem is that there is a silent assumption that the CUDA kernels from the suites (used for obtaining 
MICA metrics) are optimized and thus the speedups obtained are the optimal. Such an assumption is false due to the fact that they are 
not specifically tuned for the architectures they are tested on. 

The second problem is that they rely on a *one-CPU, two-GPUs* testing set up. This tactic although useful it does not provide a valid
answer for the situation where there are multiple CPUs and GPUs avalaible.  

>*Both include some interesting ideas that we followed in our approach*


Important work has also been done in the following:

3. "Microarchitecture-Independent Workload Characterization" by Kenneth Hoste, Lieven Eeckhout, [here](https://users.elis.ugent.be/~leeckhou/papers/micro07-hot-tutorial.pdf)

4. "AIWC: OpenCL-based Architecture-Independent Workload Characterization" by Beau Johnston, Josh Milthorpe, [here](https://www.researchgate.net/publication/325118253_AIWC_OpenCL_based_Architecture_Independent_Workload_Characterisation)

>Problem: SADLY OpenCL is somewhat dead for gpu computing [check here](https://streamhpc.com/blog/2012-09-10/nvidias-industry-leading-support-for-opencl/)
---

### 5. Key Components & Methodology

First and foremost, to tackle the software and hardware diversity challenges we used Rodinia Suite 3.1 which includes applications
from many different research domains, many different dwarves [rodinia](http://www.cs.virginia.edu/rodinia/doku.php). This benchmark 
suite contains a wide variety of application implemented both on OpenMP and CUDA.

The second step was to gather microarchitecture-independent metrics for each application. We used MICA-1.0 with intel's pintool [MICA](https://github.com/boegel/MICA). 

The third step was to obtain the memory footprint of each application-dataset tuple. We used valgrind massif tool [massif](https://valgrind.org/docs/manual/ms-manual.html).
>This step was essential since during MICA measurements we observed delays greater than x3000 execution times for specific applications.
>More specifically there were tuples where the MICA needed more than 99 hours. Given that we had to measure with MICA up to 90 different 
>tuples it is clear that we had to shut down MICA before completion, and without getting the memory footprint. 

Next we included some hardware coefficients for a fair and on point comparison between our testing architectures. The 
full coefficient analysis will be writen in future commits.

Finally we trained our models with the information gathered above. The results of our methodology can be found on MODELS directory.

*More specifics about the exact model analysis we followed will be commited soon*
