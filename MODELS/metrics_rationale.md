# Metrics & Hardware Coefficients Used For Training Our Models

For training our models we utilized three types of metrics: 
- The first category consists of a variety of application-specific microarchitecture-independent metrics. This set of metrics provides an insight on each application's characteristics in a microarchitecture-agnostic manner. 
- The second category consists of some hardware coefficients we introduced to quantify the effect of some major architectural differences, of our experimental setup, on the execution of the tested applications. These coefficients were not used directly but they helped us produce some fused metrics.
- Based on the previous two categories we produce some fused metrics that express the behavior of our applications. 

In short, in every approach our final model includes a dataset with the MICA metrics and the fused metrics.

---
---

## I. Microarchicture-Independent Metrics

### A. MICA metrics

MICA is a [Pin](https://software.intel.com/content/www/us/en/develop/articles/pin-a-dynamic-binary-instrumentation-tool.html) tool for "Microarhitecture-Independent Characterization of Applications". MICA allows us to collect a number of program characteristics to quantify runtime program behavior. These program characteristics are totally independent of the microarchitecture (cache configuration, branch predictor, ...) on which the measurements are done, in contrast to other workload characterization techniques using simulation or hardware performance counters (MICA presentation [here](https://users.ugent.be/~kehoste/ELIS/mica/) and implementation [here](https://github.com/boegel/MICA)). This tool was based on the previous research MIWC:"Microarchitecture-Independendent Workload Characterization" ([here](https://users.elis.ugent.be/~leeckhou/papers/micro07-hot-tutorial.pdf)).

MICA supports various types of microarchitecture-independent characteristics.
It also allows to measure the characteristics either for the entire execution, or per interval of N dynamic instructions. Our results were taken with interval size equal to 10'000'000 dynamic instructions. Moreover, the analysis type we selected was "all".  

>Note: 
>After explaining our rationale and testing our methodology, it will be clear that we can target on a specific 
>subset of MICA metrics to develop this model. This is essential because specifying the analysis type 
>(not to "all"), can effectively reduce the time-consuming procedure of applying MICA tool to a 
>program-dataset. Therefore we expect to have more accurate MICA measurements in the same time 
>window.     


The interval execution metrics are:

**1.  ILP:**

```
Instruction-Level Parallelism (ILP) is available for four different instruction 
window sizes (32, 64, 128, 256). This is measured by assuming perfect caches, perfect 
branch prediction, etc. The only limitations are the instruction window size and the data 
dependences.
```
Based on these metrics we define *ILP rate*. *ILP rate* is the ratio of ILP_256 divided 
by ILP_32. 
This metric captures how well a program benefits from a larger instruction window size.
At this point let's not forget that different architecture families often imply different 
issue widths. Consequently, we consider *ILP rate* to be a handfull metric for our model.

---

**2. Instruction mix:** 
```
The instruction mix is evaluated by categorizing the executed instructions.
Because the x86 architecture isn't a load-store architecture, MICA counts memory
reads/writes seperately. The following categories are used by default (in order 
of output):

- memory read (instructions which read from memory)
- memory write (instructions which write to memory)
- control flow
- arithmetic
- floating-point
- stack
- shift
- string
- sse
- system
- nop
- other
```

> "other" in our case refers specifically to: CONVERT, SETCC, XSAVE instructions
> 
> As found in the output file itypes_other_group_categories.txt .

---

**3. Branch predictability:**
```
The branch predictability of the conditional branches in the program is
evaluated using a Prediction-by-Partial-Match (PPM) predictor, in 4 different
configurations (global/local branch history, shared/seperate prediction
table(s)), using 3 different history length (4,8,12 bits).  Additionally,
average taken and transition count are also being measured.
```
Given our research target, this set of measurements does not provide significant 
information. In fact, we know that CPUs provide complex and well optimized branch 
predictors while GPUs do not provide branch/return predictors. So we come to the 
conclusion that ***control-flow metric (from instruction mix) is sufficient for a 
fair CPU-GPU comparison***.

---

**4. Register traffic:**
```
This set of metrics consists of 
    i)average number of register operands, 
    ii) average degree of use, 
    iii) dependency distances (probability <= D)
```
During our tests we observed that our models' accuracy did not benefit from including this set of metrics. 

Our intuition is that due to the fact that many of the cuda kernels tested are not 
optimized, the register-traffic information cannot be matched to an executional 
behavior in a cause and effect manner. From a register memory utilization perspective, 
cuda kernels of Rodinia are not developed to exploit the full GPUs' potential. Such an 
exploitation demands in-depth knowledge of a GPU's register memory specifics. 

*So we do not include this set of metrics in the model we propose.*

--- 

**5. Data stream strides:**

```
The distances between subsequent memory accesses are characterised by:

- local load (memory read) strides
- global load (memory read) strides
- local store (memory write) strides
- global store (memory write) strides

Local means per static instruction accesses, global means over all
instructions. The strides are characterized by powers of 8 (prob. <= 0, 8, 64,
512, 4096, 32768, 262144)
```
To further understand the difference between "local" and "global" we have to take into account
the following general definition:
  
Dynamic instruction mix is counted for the entire execution of the application; 
static instruction mix is counted per iteration. 
Thus "Global strides" refer to the entirety of memory reads and memory writes
that are finally executed. We want to speculate the actual runtime behavior of our application.

So we consider that the most valuable metrics of this set are strictly:
1.  global load (memory read) strides 
2.  global store (memory write) strides

>**Note:**
>Due to the fact that GPUs' cache line is larger than CPUs' cache line (64 bytes) the metric mem_read_global_stride_64 can be translated as a cache miss for CPU and a cache hit for GPU. So a high percentage for this metric is considered beneficial for the GPUs.


**mrgs32k-oo rationale:**
We grouped mem_read_global_stride_32768 and mem_read_global_stride_262144 to
a new metric **mrgs32k-oo**. As memory global read stride of size equal or greater than 32768 
can be considered a compulsory miss region. In other words the probability of "cache miss"  for all the tested architectures  is high when a read instruction of the above region occurs.  Moreover, this metric-reduction can lead to more readable Decision Trees. Indeed the accuracy after this substitution remains the same as you can see in every one of the models proposed.

>More info on global-local differentiation:
>- Static instructions: Simply the fixed “file” of instructions making up the program
>- Dynamic instructions: The actual instructions fetched &executed by the processor [(check this)](http://www.pitt.edu/~kmram/CoE0147/lectures/concepts.pdf)

---

**6. Memory footprint:**

```
Instruction and data memory footprint.
The size of the instruction and data memory footprint is characterized by
counting the number of blocks (64-byte) and pages (4KB) touched. This
is done seperately for data and instruction addresses.
```
In section *B. Memory footprint* you will find an thorough explanation of why we do not include this set of metrics. 
*Hint: practical reasons*

---

**7. Memory reuse distances:**

```
This is a highly valuable set of numbers to characterize the cache behavior
of the application of interest. For each memory read, the corresponding
64-byte cache block is determined. For each cache block accessed, the number
of unique cache blocks accessed since the last time it was referenced is 
determined, using a LRU stack. 
The reuse distances for all memory reads are reported in buckets. The first
bucket is used for so called 'cold references'. The subsequent buckets capture reuse 
distances of [2^n, 2^(n+1)[, where n ranges from 0 to 18. The first of these
actually captures [0,2[ (not [1,2[), while the last bucket, [2^18, 2^19[, captures all 
reuse distances larger then or equal to 2^18, so it's in fact [2^18, oo[.
In total, this delivers 20 buckets, and the total number of memory accesses 
(the first number in the output), thus 21 numbers.

For example: the fifth bucket, corresponds to accesses with reuse distance
between 2^3 and 2^4 (or 8 64-byte cache blocks to 16 64-byte cache blocks).
```

The metrics of this category are:
- cold-references
- memReuseDistance0-2
- memReuseDistance2-4

    ...

- memReuseDist128k-256k
- memReuseDist256k-oo


In some of our models we grouped together:
1.  memReuseDist1k-2k, ..., memReuseDist16k-32k    to *mRD1k-32k*
2.  memReuseDist32k-64k, ..., memReuseDist256k-32k to *mRD32k-oo*


This grouping is not without reason.

**1. mRD1k-32k rationale**:
We examined the hardware specifications of the GPUs of our testing setup. 
One of the main architectural differences between Fermi, Kepler and Maxwell GPUs is 
the L2 cache size. For Fermi L2 cache size is equal to 786432 bytes (768 KB). 
For Kepler L2 cache size is 1572864 bytes (1.5MB). For Maxwell L2 cache size is 
2097152 bytes (2MB).

To correlate the cache sizes with the grouped metrics we introduced one has to question 
himself/herself:<br />
"How many cpu cache lines can L2 cache fit?". <br />
Fermi's L2 cache fits 12288 cpu cache lines.<br />
Kepler's L2 cache fits 24576 cpu cache lines.<br />
Maxwell's L2 cache fits 32768 cpu cache lines. <br />
The window 1k-32k rougly defines a region of memory reuse distances where the different
L2 cache sizes can lead to GPU-cache-misses on one GPU and GPU-cache-hit on another.


**2. mRD32k-oo rationale**:
The window 32k-oo can be translated as compulsory misses for all three GPUs we tested.
Most likely compulsory misses will be observed in CPUs too.
The architectural criterion in this case is obviously the memory bandwidth of the tested architecture.

> The metric substitution we introduced leads to more readable Decision Trees 
> and as you can see did not affect negatively our models' accuracies.

---
#### MICA metrics summary:
-   ILP rate
-  mem-read, mem-write, control-flow, arithmetic, floating-point, stack, shift, string, sse, system, nop, other
-  cold-references, memReuseDist0-2, ..., memReuseDist512-1k, mRD1k-32k, mRD32k-oo
-  mem_read_global_stride_[8,64,512,4096], mrgs32k-oo
-  mem_write_global_stride_[8,64,512,4096,32768,262144]

---

### B. Memory footprint

Due to fact that MICA tool drastically slows down the execution of the application, we try to analyze we applied interval analysis and in some cases we force the termination of the MICA measurements. For example there were executions that the overall runtime exceeded 99 hours. We had to test ~90 (application-dataset) tuples for three CPU architectures, so termination was the only option in some cases.  Thanks to the interval analysis we don't lose all the selected data. The majority of the above MICA metrics are percentages or rates so if we force the termination of the MICA after several hours the characteristics of the programs are captured with some minor error (given that without MICA our applications were executed orders of magnitude faster). To quantify the impact of different input datasets to a given application we need the memory footprint. This measurement in the cases that we force the termination is not properly obtained.

To capture memory footprint we use [valgrind massif tool](https://valgrind.org/docs/manual/ms-manual.html). The measured value is in MB.

---
---

## II. Hardware Coefficients

Assume there is an application that is implemented both on OpenMP and CUDA. A highly likely scenario is that the application performs better on GPU-node-A than on CPU-node-A. At the same time there is CPU-node-B (different generation from CPU-node-A) where our application performs better than GPU-node-A. Thus the answer to the question "what should be my target architecture?" is not simply
"CPU" or "GPU", we have to be specific.

Microarchitecture-independent metrics cannot provide a solution to the problem above. We have to insert some *hardware coefficients* to our models. The cross-platform coefficients we introduced are:
- coef_IW (Issue-Width Coefficient)
- coef_BW (Bandwidth Coefficient)
- coef_SIMD  (SIMD Coefficient)
- coef_L2_gpu (GPUs' L2 Coefficient)
- coef_TLP_min (TLP1 Coefficient)
- coef_TLP_max (TLP2 Coefficient)

**Important Note 1:**
Based on the MICA results we obtained for our applications, we observe that the computational part mainly consists of **single precision** and **sse** instructions. This is a significant observation, because it crucially affects the quantification of the proposed coefficients. A radically different image of the instruction-mix would lead to a slightly different coefficient definition.  

**Important Note 2:**
We will use the above coefficients  to produce a set of **fused** metrics. So we will correlate specific MICA-measurements to specific architectural differences (in the form of coefficients). In the final model, we provide both the fused and the coefficient-free metrics.

---
#### GPUs-key features:

We consider that the reader is already familiar with the programming model and the memory model of cuda [(both here)](https://accel.cs.vt.edu/files/lecture8.pdf) . This concept is widely known so we will not expand on it. It is a prerequisite before proceeding to the following analysis.

Before exploring the proposed coefficients it is vital to understand that a CPU-GPU comparison seems a little bit off. It is more or less an apple to orange comparison. The compared families of architectures are designed to tackle different problems. CPUs tackle more general problems with parallel problems being only a fraction of the whole scenery. On the other hand, GPUs are designed strictly for parallel problems. Therefore, there are major differences between the cores, caches etc. of these architectures. 

We have to be aware that the branding of the architecture-features can be (and sometimes is) misleading. With no intentions of discrediting any of the vendors, we believe that *cuda core* is not the best candidate for a comparison with the *CPU core*. 

It seems more accurate to think of Streaming-Multiprocessors (SMs) as an exotic computing core with a vast variety of architecture features dedicated to it. Undoubtedly, this approach does not fully capture the architectural changes from generation to generation. SMs progressively become more and more complex and efficient. The below images depict Fermi's SM, Kepler's SM (SMX), Maxwell's SM (SMM).

![Fermi](./storage/images/SMs/Fermi_SM.jpg)

![Kepler](./storage/images/SMs/Kepler_SMX.jpg)

![Maxwell](./storage/images/SMs/Maxwell_SMM.jpg)

> *Friendly reminder: our testing setup includes Tesla-M2050(Fermi), Tesla K40c(Kepler), Quadro M4000(Maxwell)*

To search detailed information for each generation's SMs we suggest that you look on the following links:
- [Fermi, look at page 9](https://www.nvidia.com/content/PDF/fermi_white_papers/P.Glaskowsky_NVIDIA's_Fermi-The_First_Complete_GPU_Architecture.pdf)
- [Kepler, look at page 8](https://www.nvidia.com/content/dam/en-zz/Solutions/Data-Center/tesla-product-literature/NVIDIA-Kepler-GK110-GK210-Architecture-Whitepaper.pdf)
- [Maxwell, look at page 8](https://www.nvidia.com/content/dam/en-zz/Solutions/Data-Center/tesla-product-literature/NVIDIA-Kepler-GK110-GK210-Architecture-Whitepaper.pdf)

Yet we have to search for a common ground to provide some valid comparisons. So we look at SMs in an abstract way, we focus on cross-generation essentials and finally draw our conclusions. Generally, the Streaming-Multiprocessor can be viewed in an abstract manner in three main phases: 1) Front-end, 2) Back-end,  3) Memory Interface. 
1.  Front-end: Instruction Cache, Warp Scheduler, Dispatch Units 
2.  Back-end: Register, Functional Units (ALUs, FPUs S.P, FPUs D.P., Load/Store Units, SFUs etc)
3.  Memory Interface: L1 cache, Read Only cache, Texture Memory etc

Based on the above list we have to quantify the potentials of exploiting Instruction-Level Parallelism (ILP), Thread-Level Parallelism (TLP), Data-Level Parallelism (DLP) and finally the potential of accessing memory efficiently. *Let's not forget [Flynn's Taxonomy](https://www.geeksforgeeks.org/computer-architecture-flynns-taxonomy/).

---
### coef_IW

To compare the ILP potential of the tested CPU to the ILP potential of the tested GPU we introduce **Issue-Width** (IW) **Coefficient**:

![coef_IW](./storage/images/formulas/coef_IW.jpg)

Both in the nominator and denominator, we multiply the issue-widths with the corresponding number of SMs/cores. This allows as capture how each architecture as a whole, can leverage from a highly Instruction-Level parallel program.

*This coefficient will be used with ILP rate.*

---
### coef_BW

We quantify the difference between the memory bandwidths of the tested CPU and GPU. This coefficient is straight-forward and can be utilized to further explore the cost of memory accesses to the main memory in each case. 

 ![coef_BW](./storage/images/formulas/coef_BW.jpg)

*This coefficient will be used with mrgs32k-oo, mRD32k-oo.*

---
### coef_SIMD

In order to explore the applications' DLP potential on the compared architectures we introduce SIMD coefficient. 

 ![coef_SIMD](./storage/images/formulas/coef_SIMD.jpg)

**Note:**
This metric has a drawback. It has a common nominator for all GPUs and thus can not express the different processing power between the GPUs. 
>How many *"32-wide Vector SIMDs* lie in every SM of the tested GPU?


---
### coef_L2_gpu

As we explained in part I. section A. "Memory Reuse Distance", *mRD1k-32k* metric indicates a window of reuse distances  where the GPU's L2 cache size makes the difference. To express this key-difference between the GPUs we introduce this coefficient.

 ![coef_L2_gpu](./storage/images/formulas/coef_L2_gpu.jpg)

The reference L2 cache size is equal to the minimum L2 cache size among the tested GPUs.

---
### coef_TLP_min

To capture a spectrum of TLP potential, we calculate the minimum number of **in-flight warps** (*mifw*) we need to fully utilize the available cuda cores per SM. (Think of it as a concurrent execution window)
>According to "Important note 1" we focus on cuda cores as these are the pipelined ALUs destined for Arithmetic, Single Precision and SSE instructions. If the dominant set of instructions was Double Precision we would focus on  the available FPUs DP.

Remember that GPU enables multiple warps to be active in each SM. To hide latency -caused by stalls, data dependencies etc- the warp schedulers can swap warps in a very efficient way. 

![coef_TLP_min](./storage/images/formulas/coef_TLP_min.jpg)

The CPUs of are 2-hyperthreaded, therefore *hyperthreading degree*=2.

[Here](https://forums.developer.nvidia.com/t/how-to-schedule-warps/12944/4) you can find an example of the in-flight warps per SM concept. The dispatch units imply that the Method 3 suggested in the link is the use case we have in Tesla K40c.

---
### coef_TLP_max

To capture another spectrum of TLP potential, we calculate the maximum number of **active warps**  per SM. By using [nvidia-smi](https://developer.nvidia.com/nvidia-system-management-interface) we can find the upper limit of active threads per SM, then we divide this value by the warp-size and we have the number of active warps per SM.

Remember that GPU enables multiple warps to be active in each SM. To hide latency -caused by stalls, data dependencies etc- the warp schedulers can swap warps in a very efficient way. 

![coef_TLP_max](./storage/images/formulas/coef_TLP_max.jpg)

The CPUs of are 2-hyperthreaded, therefore *hyperthreading degree*=2.

>[Here](https://forums.developer.nvidia.com/t/how-to-schedule-warps/12944/4) you can find an example of the in-flight warps per SM concept. The dispatch units imply that the Method 3 suggested in the link is the use case we have in Tesla K40c.


---
---

## III. Fused Metrics

Both *Random Forest Classification* and *Decision Tree Classification* are supervised ML methods. Part of supervising these models is providing them meaningful values that can actually contribute to the overall classification effort. Towards this goal we introduce the following fused metrics:

- ILP fused
- mrgs fused
- mRDoo fused
- mRD1k-32k fused
- SSE fused A \& B

We use the coeffiecients  as factors and multiply them to specific metrics. This multiplication can express the correlation between the measured value and the architectural features that determine its effect. To be more thorough we do not substitute the values with their fused versions. We provide both versions (unfused, "unfused") of each metric to our model. According to our tests this yields sufficient accuracy to all our predictions.

---
### ILP fused:

![ILP_fused](./storage/images/formulas/ILP_fused.jpg)

So for a high *ILP rate* value and a high value for *coef_IW* we have a clear indication that the GPU tested can leverage from the ILP character of the corresponding application. 

---
### mrgs fused:

![mrgs_fused](./storage/images/formulas/mrgs_fused.jpg)

The *mrgs32k-oo* can be partially translated as "accesses to the main memory" for both CPUs and GPUs. The key to see which architectures dominates is the memory bandwidth of each architecture. 

>Obviously due to the more complex (k-way associative) caches of CPUs it is likely that the above accesses may not always be translated as compulsory misses. To tackle this we provide the pure *mrgs32k-oo* to our model too.

---
### mRDoo fused:

![mRDoo_fused](./storage/images/formulas/mRDoo_fused.jpg)

The rationale is similar to the one explained for *mrgs fused*.

---
### mRD1k-32k fused:

![mRD1k-32k_fused](./storage/images/formulas/mRD1k_32k_fused.jpg)

The rationale is similar to the one explained for *mrgs fused*.

---
### SSE fused A \& SSE fused B:

![SSE_fused_AB](./storage/images/formulas/SSE_fused_AB.jpg)

SSE-instructions percentage can be seen as the heavily parallel part of our program. So we assess both the TLP and DLP potentials.

---
---
## IV. Conclusion

The metrics we use for our models are:

- Memory Footprint 
- memory read, memory write, control flow, arithmetic, floating-point, stack, shift, string, sse, system, nop, other
-  ILP rate
-  cold-references, memReuseDist0-2, ..., memReuseDist512-1k, mRD1k-32k, mRD32k-oo
-  mem_read_global_stride_[8,64,512,4096], mrgs32k-oo
-  mem_write_global_stride_[8,64,512,4096,32768,262144]
- ILP fused
- mrgs fused
- mRDoo fused
- mRD1k-32k fused
- SSE fused A \& B

---
---

```
Markdown on GITLAB apparently has some issues with mathematical formulas:
FORMULAS:
$coef\_IW = \frac{(number \  of \ SMs) \cdot (IW_{GPU})}{(number \ of \ CPU \ cores) \cdot (IW_{CPU})}$
$coef\_BW = \frac{Mem\_BW_{GPU}}{Mem\_BW_{CPU}}$
$coef\_SIMD = \frac{warp\_size_{GPU}}{vector\_width_{CPU}}$
$coef\_L2\_gpu = \frac{L2\_size_{tested \ GPU}}{L2\_size_{reference}}$
$coef\_TLP\_min = \frac{(number\_of\_SMs)\cdot(mifw)}{(number\ of\ CPU\ cores)\cdot(hyperthreading\ degree)}$
$coef\_TLP\_max= \frac{(number\_of\_SMs)\cdot(number\ of\ active-warps)}{(number\ of\ CPU\ cores)\cdot(hyperthreading\ degree)}$
$ILP_{fused} = {(coef\_IW)\cdot(ILP \ rate)  }$
$mrgs_{fused} = {(coef\_BW)\cdot(mrgs[32k,\infty))  }$
$mRDoo_{fused}= {(coef\_BW)\cdot(mRD[32k,oo))  }$
$mRD[1k,32k]_{fused}= {(coef\_L2\_gpu)\cdot(mRD[1k,32k])  }$
$SSE\ fused\ A= {(coef\_TLP\_min)\cdot(coef\_SIMD)\cdot(sse)  }$
$SSE\ fused\ B= {(coef\_TLP\_max)\cdot(coef\_SIMD)\cdot(sse)  }$
```
