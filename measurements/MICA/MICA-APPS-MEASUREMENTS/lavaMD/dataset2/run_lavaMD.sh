#!/bin/bash

## Give the Job a descriptive name
#PBS -N lavaMD_2

## Output and error files
#PBS -o lavaMD_mica_2.out
#PBS -e lavaMD_mica_2.err

## Limit memory, runtime etc.
#PBS -l walltime=10:00:00

## How many nodes:processors_per_node should we get?
#PBS -l nodes=haci3:ppn=14


cd /home/users/amarkou/DIPLOMA/rodinia_3.1/MICA/lavaMD_2

make clean
make

/home/users/amarkou/tools/pin-3.7-97619-g0d0c92f4f-gcc-linux/pin -follow_execv 1 -t ~/tools/pin-3.7-97619-g0d0c92f4f-gcc-linux/source/tools/MICA-1.0.1/obj-intel64/mica.so -- ./lavaMD -cores 1 -boxes1d 20



