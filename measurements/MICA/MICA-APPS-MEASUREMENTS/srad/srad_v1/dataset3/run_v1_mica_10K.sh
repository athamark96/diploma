#!/bin/bash

## Give the Job a descriptive name
#PBS -N srad_v1_10K

## Output and error files
#PBS -o srad_v1_mica_10K.out
#PBS -e srad_v1_mica_10K.err

## Limit memory, runtime etc.
#PBS -l walltime=5:00:00

## How many nodes:processors_per_node should we get?
#PBS -l nodes=haci3:ppn=14


cd /home/users/amarkou/DIPLOMA/rodinia_3.1/MICA/srad/srad_v1_3

make clean
make

/home/users/amarkou/tools/pin-3.7-97619-g0d0c92f4f-gcc-linux/pin -follow_execv 1 -t ~/tools/pin-3.7-97619-g0d0c92f4f-gcc-linux/source/tools/MICA-1.0.1/obj-intel64/mica.so -- ./srad 10000 0.5 502 458 1




