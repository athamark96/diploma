#!/usr/bin/python

import sys, csv

ifile    = sys.argv[1] 
num_cols = int(sys.argv[2])

#for i in range(num_cols):
#    print total[i]

i = 0
faulty_row = 0

### check if the csv is corrupted:
#i) create reference line list
rf = open(ifile,"r")
lines = rf.readlines()
rf.close()




with open(ifile) as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    for row in reader:
        i+=1
        if (len(row) != num_cols and len(row) != 0):
            faulty_row=i-1
            print ("Length of row is: %r" % len(row) )
            print row,faulty_row
            break

#ii) locate the line/row that need to be patched(if it exists)
#cf = open(ifile,"r")
#for row in csv.reader(cf):
#    i+=1
#    print row.count(",")
#    if num_cols != len(row) :        
#        faulty_row = i
#        break

#cf.close()

#exit()

#iii)patch
if faulty_row != 0 :
    wf = open(ifile,"w+")
    del lines[faulty_row]

    j = 0
    for line in lines:
        wf.write(line)
        if j == faulty_row-1:   #replace the corrupt line with 2 copies of the last valid line
            wf.write(line)
            wf.write(line)
        j+=1
            
    wf.close()

########################
