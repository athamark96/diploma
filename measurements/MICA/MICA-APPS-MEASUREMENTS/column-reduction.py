#!/usr/bin/python

import sys, csv

ifile    = sys.argv[1] 
num_cols = int(sys.argv[2])

#print num_cols
total = [0]*num_cols

#for i in range(num_cols):
#    print total[i]
    
with open(ifile,"r+") as fin:
    # headerline = fin.next()
    for row in csv.reader(fin):
        if row:          
            for index in range(num_cols):      
                total[index] += int(row[index])

for index2 in range(0,num_cols-1):
    print total[index2], ',',

print total[num_cols-1]    


