#!/bin/bash

## Give the Job a descriptive name
#PBS -N streamcluster_mica_4

## Output and error files
#PBS -o streamcluster_mica4.out
#PBS -e streamcluster_mica4.err

## Limit memory, runtime etc.
#PBS -l walltime=4:00:00

## How many nodes:processors_per_node should we get?
#PBS -l nodes=haci3:ppn=7

cd /home/users/amarkou/DIPLOMA/rodinia_3.1/MICA/sc_dataset4
make clean
make

echo -e "omp threads,pgain,pgain_dist,pgain_init,pselect,pspeedy,pshuffle,local search,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"

/home/users/amarkou/tools/pin-3.7-97619-g0d0c92f4f-gcc-linux/pin -follow_execv 1 -t /home/users/amarkou/tools/pin-3.7-97619-g0d0c92f4f-gcc-linux/source/tools/MICA-1.0.1/obj-intel64/mica.so -- ./sc_omp 10 20 256 262144 65536 1000 none output.txt 1

echo -e "---------------------------------------------------------------------------\n"


