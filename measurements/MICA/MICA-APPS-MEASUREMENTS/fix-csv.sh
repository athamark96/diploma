## cd to the folder /app/dataset you want to fix and execute ../../fix-csv.sh $1
##TRAMSFORM to .csv format
mkdir CSVs

cat ilp_phases_int_$1_pin.out | tr -s '[:blank:]' ','          > ilp_phases_int_$1_pin.csv
cat itypes_phases_int_$1_pin.out | tr -s '[:blank:]' ','       > itypes_phases_int_$1_pin.csv
cat memfootprint_phases_int_$1_pin.out | tr -s '[:blank:]' ',' > memfootprint_phases_int_$1_pin.csv
cat memstackdist_phases_int_$1_pin.out | tr -s '[:blank:]' ',' > memstackdist_phases_int_$1_pin.csv
cat ppm_phases_int_$1_pin.out | tr -s '[:blank:]' ','          > ppm_phases_int_$1_pin.csv
cat reg_phases_int_$1_pin.out | tr -s '[:blank:]' ','          > reg_phases_int_$1_pin.csv
cat stride_phases_int_$1_pin.out | tr -s '[:blank:]' ','       > stride_phases_int_$1_pin.csv

##Clean 'number of instructions=', last line in each csv file

sed '$d' ilp_phases_int_$1_pin.csv 	    > ilp_phases.csv
sed '$d' itypes_phases_int_$1_pin.csv       > itypes_phases.csv
sed '$d' memfootprint_phases_int_$1_pin.csv > memfootprint_phases.csv
sed '$d' memstackdist_phases_int_$1_pin.csv > memstackdist_phases.csv
sed '$d' ppm_phases_int_$1_pin.csv	    > ppm_phases.csv
sed '$d' reg_phases_int_$1_pin.csv	    > reg_phases.csv
sed '$d' stride_phases_int_$1_pin.csv	    > stride_phases.csv

mv *.csv ./CSVs

cd ./CSVs

##fix corrupted csv files: patch.py file.csv number-of-columns 
echo -e "\nPATCH CORRUPTED .csv FILES:"
echo -e "\nilp_phases:\n-------------------------------------------\n"
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/patch.py ilp_phases.csv 5
echo -e "\n\nitypes_phases:\n-------------------------------------------\n"
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/patch.py itypes_phases.csv 13
echo -e "\n\nmemfootprint_phases:\n-------------------------------------------\n"
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/patch.py memfootprint_phases.csv 4
echo -e "\n\nmemstackdist_phases:\n-------------------------------------------\n"
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/patch.py memstackdist_phases.csv 21
echo -e "\n\nppm_phases:\n-------------------------------------------\n"
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/patch.py ppm_phases.csv 16
echo -e "\n\nreg_phases:\n-------------------------------------------\n"
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/patch.py reg_phases.csv 12
echo -e "\n\nstride_phases:\n-------------------------------------------\n"
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/patch.py stride_phases.csv 30
echo -e "----------------------------------------------------------------------------\n\n"

##REDUCE .csv files

##headers for reduced files
echo "totInstruction,ILP32,ILP64,ILP128,ILP256" >> ilp_phases_reduced.csv
echo "instr_cnt,mem-read,mem-write,control-flow,arithmetic,floating-point,stack,shift,string,sse,system,nop,other" >> itypes_phases_reduced.csv
echo "DataFootprint64,DataFootprint4k,InstrFootprint64,InstrFootprint4k" >> memfootprint_phases_reduced.csv
echo "mem_access,cold_references,memReuseDist0-2,memReuseDist2-4,memReuseDist4-8,memReuseDist8-16,memReuseDist16-32,memReuseDist32-64,memReuseDist64-128,memReuseDist128-256,memReuseDist256-512,memReuseDist512-1k,memReuseDist1k-2k,memReuseDist2k-4k,memReuseDist4k-8k,memReuseDist8k-16k,memReuseDist16k-32k,memReuseDist32k-64k,memReuseDist64k-128k,memReuseDist128k-256k,memReuseDist256k-00" >> memstackdist_phases_reduced.csv
echo "instr_cnt,GAg_mispred_cnt_4bits,PAg_mispred_cnt_4bits,GAs_mispred_cnt_4bits,PAs_mispred_cnt_4bits,GAg_mispred_cnt_8bits,PAg_mispred_cnt_8bits,GAs_mispred_cnt_8bits,PAs_mispred_cnt_8bits,GAg_mispred_cnt_12bits,PAg_mispred_cnt_12bits,GAs_mispred_cnt_12bits,PAs_mispred_cnt_12bits,total_brCount,total_transactionCount,total_takenCount" >> ppm_phases_reduced.csv
echo "instr_cnt,total_num_ops,instr_reg_cnt,total_reg_use_cnt,total_reg_age,reg_age_cnt_1,reg_age_cnt_2,reg_age_cnt_4,reg_age_cnt_8,reg_age_cnt_16,reg_age_cnt_32,reg_age_cnt_64" >> reg_phases_reduced.csv
echo "mem_read_cnt,mem_read_local_stride_0,mem_read_local_stride_8,mem_read_local_stride_64,mem_read_local_stride_512,mem_read_local_stride_4096,mem_read_local_stride_32768,mem_read_local_stride_262144,mem_read_global_stride_0,mem_read_global_stride_8,mem_read_global_stride_64,mem_read_global_stride_512,mem_read_global_stride_4096,mem_read_global_stride_32768,mem_read_global_stride_262144,mem_write_cnt,mem_write_local_stride_0,mem_write_local_stride_8,mem_write_local_stride_64,mem_write_local_stride_512,mem_write_local_stride_4096,mem_write_local_stride_32768,mem_write_local_stride_262144,mem_write_global_stride_0,mem_write_global_stride_8,mem_write_global_stride_64,mem_write_global_stride_512,mem_write_global_stride_4096,mem_write_global_stride_32768,mem_write_global_stride_262144" >> stride_phases_reduced.csv

##reductions:
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/column-reduction.py ilp_phases.csv 5 >> ilp_phases_reduced.csv
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/column-reduction.py itypes_phases.csv 13  >> itypes_phases_reduced.csv
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/column-reduction.py memfootprint_phases.csv 4 >> memfootprint_phases_reduced.csv
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/column-reduction.py memstackdist_phases.csv 21 >> memstackdist_phases_reduced.csv
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/column-reduction.py ppm_phases.csv 16 >> ppm_phases_reduced.csv
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/column-reduction.py reg_phases.csv 12 >> reg_phases_reduced.csv
/home/athamark/DIPLOMA_2019_2020/diploma/measurements/MICA/MICA-APPS-MEASUREMENTS/column-reduction.py stride_phases.csv 30 >> stride_phases_reduced.csv

mkdir reduced

mv *reduced.csv ./reduced


