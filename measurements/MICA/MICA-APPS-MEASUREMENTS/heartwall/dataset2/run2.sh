#!/bin/bash

## Give the Job a descriptive name
#PBS -N heartwall_mica_remote

## Output and error files
#PBS -o heartwall_mica.out
#PBS -e heartwall_mica.err

## Limit memory, runtime etc.
#PBS -l walltime=05:00:00

## How many nodes:processors_per_node should we get?
#PBS -l nodes=haci3:ppn=14

cd /home/users/amarkou/DIPLOMA/rodinia_3.1/MICA/heartwall/

echo -e "omp threads, kernel time, total_time" >> time_succint.csv

echo -e "for 30 layers in total:" 
echo -e "------------------------\n"
echo -e "number of threads = 1  \n"

export OMP_NUM_THREADS=1
/home/users/amarkou/tools/pin-3.7-97619-g0d0c92f4f-gcc-linux/pin -follow_execv 1 -t /home/users/amarkou/tools/pin-3.7-97619-g0d0c92f4f-gcc-linux/source/tools/MICA-1.0.1/obj-intel64/mica.so -- ./../heartwall /home/users/amarkou/DIPLOMA/rodinia_3.1/data/heartwall/test.avi 30 1

echo -e "========================\n"

