#!/bin/sh

##Author: Athamark , 18/10/2020
#######################################################################################
##Update the file /local-path-to-mica-measurements/measurements/MICA/SUMMARIZED 
##to include the new results in the mica-overall.csv
#######################################################################################
##The structure shall be:
##	./App/dataset1,dataset2,...,datasetN
#######################################################################################
##Important NOTE:
## DO NOT RUN THIS SCRIPT WHILST INSIDE /MICA/SUMMARIZED FOLDER!
#######################################################################################
# Run this script when in folder: /local-path-to-mica-measurements/measurements/MICA
# which contains SUMMARIZED directory us a subdirectory
#######################################################################################
cd ./SUMMARIZED

echo -n "APPLICATION NAME dataset,"
echo -n "totInstruction,ILP32,ILP64,ILP128,ILP256," 
echo -n "total_ins_count,mem-read,mem-write,control-flow,arithmetic,floating-point,stack,shift,string,sse,system,nop,other,"
echo -n "DataFootprint64,DataFootprint4k,InstrFootprint64,InstrFootprint4k,"
echo -n "mem_access,cold-references, memReuseDist0-2,memReuseDist2-4,memReuseDist4-8,memReuseDist8-16,memReuseDist16-32,memReuseDist32-64,memReuseDist64-128,memReuseDist128-256,memReuseDist256-512,memReuseDist512-1k,memReuseDist1k-2k,memReuseDist2k-4k,memReuseDist4k-8k,memReuseDist8k-16k,memReuseDist16k-32k,memReuseDist32k-64k,memReuseDist64k-128k,memReuseDist128k-256k,memReuseDist256k-00,"
echo -n "instr_cnt ,GAg_mispred_cnt_4bits,PAg_mispred_cnt_4bits,GAs_mispred_cnt_4bits,PAs_mispred_cnt_4bits,GAg_mispred_cnt_8bits,PAg_mispred_cnt_8bits,GAs_mispred_cnt_8bits,PAs_mispred_cnt_8bits,GAg_mispred_cnt_12bits,PAg_mispred_cnt_12bits,GAs_mispred_cnt_12bits,PAs_mispred_cnt_12bits,total_brCount,total_transitionCount,total_takenCount,"
echo -n "instr_cnt,total_num_ops,instr_reg_cnt,total_reg_use_cnt,total_reg_age,reg_age_cnt_1,reg_age_cnt_2,reg_age_cnt_4,reg_age_cnt_8,reg_age_cnt_16,reg_age_cnt_32,reg_age_cnt_64,"
echo "mem_read_cnt,mem_read_local_stride_0,mem_read_local_stride_8,mem_read_local_stride_64,mem_read_local_stride_512,mem_read_local_stride_4096,mem_read_local_stride_32768,mem_read_local_stride_262144,mem_read_global_stride_0,mem_read_global_stride_8,mem_read_global_stride_64,mem_read_global_stride_512,mem_read_global_stride_4096,mem_read_global_stride_32768,mem_read_global_stride_262144,mem_write_cnt,mem_write_local_stride_0,mem_write_local_stride_8,mem_write_local_stride_64,mem_write_local_stride_512,mem_write_local_stride_4096,mem_write_local_stride_32768,mem_write_local_stride_262144,mem_write_global_stride_0,mem_write_global_stride_8,mem_write_global_stride_64,mem_write_global_stride_512,mem_write_global_stride_4096,mem_write_global_stride_32768,mem_write_global_stride_262144" 


for app in * 
do
	cd ./$app
	for set in *
	do
		echo -n "$app $set,"
		sed -n 2p  ./$set/ilp_*.csv | tr '\n' ' '
                echo -n ","
                sed -n 2p ./$set/itypes_*.csv | tr '\n' ' '
                echo -n ","
                sed -n 2p ./$set/memfootprint_*.csv | tr '\n' ' '
                echo -n ","
                sed -n 2p ./$set/memstackdist_*.csv | tr '\n' ' '
                echo -n ","
                sed -n 2p ./$set/ppm_*.csv | tr '\n' ' '
                echo -n ","
                sed -n 2p ./$set/reg_*.csv | tr '\n' ' '
		echo -n ","
                sed -n 2p ./$set/stride_*.csv | tr '\n' ' '		
		echo ""
	done

	cd ..
done
echo ""

