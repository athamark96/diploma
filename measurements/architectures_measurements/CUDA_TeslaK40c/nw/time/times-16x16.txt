Problem size = 512x512
----------------------------------------------

WG size of kernel = 16 
Start Needleman-Wunsch
Processing top-left matrix
Processing bottom-right matrix
Setup time:2.366665 s
NW Initialization time:0.001102 s
Cuda Mallocs time:0.097901 s
Processing top-left matrix [cuda kernel 1 calls] time:0.000399 s
Processing bottom-right matrix [cuda kernel 2 calls] time:0.000317 s
CudaMemCopy time:0.001921 s
Traceback time(if defined so):0.000000 s
CudaFrees time:0.000672 s

-----------------------------------------
Total time:2.468977 s

Total cuda execution and commynication time:0.101210 s






===================================================

Problem size =1024x1024
----------------------------------------------

WG size of kernel = 16 
Start Needleman-Wunsch
Processing top-left matrix
Processing bottom-right matrix
Setup time:2.374192 s
NW Initialization time:0.004381 s
Cuda Mallocs time:0.084420 s
Processing top-left matrix [cuda kernel 1 calls] time:0.000272 s
Processing bottom-right matrix [cuda kernel 2 calls] time:0.000214 s
CudaMemCopy time:0.004239 s
Traceback time(if defined so):0.000000 s
CudaFrees time:0.000708 s

-----------------------------------------
Total time:2.468426 s

Total cuda execution and commynication time:0.089853 s






===================================================

Problem size =2048x2048
----------------------------------------------

WG size of kernel = 16 
Start Needleman-Wunsch
Processing top-left matrix
Processing bottom-right matrix
Setup time:2.533036 s
NW Initialization time:0.017433 s
Cuda Mallocs time:0.088013 s
Processing top-left matrix [cuda kernel 1 calls] time:0.000501 s
Processing bottom-right matrix [cuda kernel 2 calls] time:0.000446 s
CudaMemCopy time:0.011270 s
Traceback time(if defined so):0.000000 s
CudaFrees time:0.001966 s

-----------------------------------------
Total time:2.652665 s

Total cuda execution and commynication time:0.102196 s






===================================================

Problem size =4096x4096
----------------------------------------------

WG size of kernel = 16 
Start Needleman-Wunsch
Processing top-left matrix
Processing bottom-right matrix
Setup time:2.403844 s
NW Initialization time:0.068844 s
Cuda Mallocs time:0.125499 s
Processing top-left matrix [cuda kernel 1 calls] time:0.000948 s
Processing bottom-right matrix [cuda kernel 2 calls] time:0.000876 s
CudaMemCopy time:0.036218 s
Traceback time(if defined so):0.000001 s
CudaFrees time:0.006804 s

-----------------------------------------
Total time:2.643034 s

Total cuda execution and commynication time:0.170346 s






===================================================

Problem size =8192x8192
----------------------------------------------

WG size of kernel = 16 
Start Needleman-Wunsch
Processing top-left matrix
Processing bottom-right matrix
Setup time:2.586677 s
NW Initialization time:0.274449 s
Cuda Mallocs time:0.256224 s
Processing top-left matrix [cuda kernel 1 calls] time:0.001841 s
Processing bottom-right matrix [cuda kernel 2 calls] time:0.001745 s
CudaMemCopy time:0.138333 s
Traceback time(if defined so):0.000001 s
CudaFrees time:0.026180 s

-----------------------------------------
Total time:3.285450 s

Total cuda execution and commynication time:0.424324 s






===================================================

Problem size =16384x16384
----------------------------------------------

WG size of kernel = 16 
Start Needleman-Wunsch
Processing top-left matrix
Processing bottom-right matrix
Setup time:3.257839 s
NW Initialization time:1.095326 s
Cuda Mallocs time:0.760383 s
Processing top-left matrix [cuda kernel 1 calls] time:0.003516 s
Processing bottom-right matrix [cuda kernel 2 calls] time:0.060404 s
CudaMemCopy time:0.489210 s
Traceback time(if defined so):0.000000 s
CudaFrees time:0.103794 s

-----------------------------------------
Total time:5.770472 s

Total cuda execution and commynication time:1.417307 s






===================================================

