Default settings results:

[][][][][][][][][][][][][][][][][][][][][][][]

THREADS_PER_DIM 16
BLOCKS_PER_DIM 16
THREADS_PER_BLOCK THREADS_PER_DIM*THREADS_PER_DIM



dim3 blockSize(16,16,1)
gridSize(16,16,1)

[][][][][][][][][][][][][][][][][][][][][][][]

100:


I/O completed

Number of objects: 100
Number of features: 34


:TIME MEASUREMENTS:
----------------------------------------------------
Device set time: 0.003174 (ms)
Copy from host to device: 0.039062 (ms)
Set up Textures:      0 (ms)
Copying Clusters to constant gpu-memory: 0.003906 (ms)
Kernel run time: 0.087158 (ms)
Copy results from device to host: 0.035889 (ms)
Overall reduction time: 0.010010 (ms)
Total time: 0.213135 (ms)


:TIME MEASUREMENTS:
----------------------------------------------------
Device set time: 0.001953 (ms)
Copy from host to device: 0.027100 (ms)
Set up Textures:      0 (ms)
Copying Clusters to constant gpu-memory: 0.001221 (ms)
Kernel run time: 0.052979 (ms)
Copy results from device to host: 0.021973 (ms)
Overall reduction time: 0.011963 (ms)
Total time: 0.125000 (ms)
iterated 2 times
Number of Iteration: 1


 RESULTS:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Setup overall time (prior printf included): 137.859863 (ms)
Cluster(some printf+KERNELxITERATIONS): 135.604980 (ms)
----------------------------------------------

204800.txt:


I/O completed

Number of objects: 204800
Number of features: 34


:TIME MEASUREMENTS:
----------------------------------------------------
Device set time: 0.001953 (ms)
Copy from host to device: 2.080078 (ms)
Set up Textures:      0 (ms)
Copying Clusters to constant gpu-memory: 0.001953 (ms)
Kernel run time: 0.832031 (ms)
Copy results from device to host: 0.398926 (ms)
Overall reduction time: 6.461182 (ms)
Total time: 9.791992 (ms)


:TIME MEASUREMENTS:
----------------------------------------------------
Device set time: 0.006104 (ms)
Copy from host to device: 0.406006 (ms)
Set up Textures:      0 (ms)
Copying Clusters to constant gpu-memory: 0.000977 (ms)
Kernel run time: 0.831055 (ms)
Copy results from device to host: 0.393799 (ms)
Overall reduction time: 6.793213 (ms)
Total time: 8.438232 (ms)
iterated 2 times
Number of Iteration: 1


 RESULTS:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Setup overall time (prior printf included): 505.527100 (ms)
Cluster(some printf+KERNELxITERATIONS): 125.522949 (ms)
----------------------------------------------

819200.txt:


I/O completed

Number of objects: 819200
Number of features: 34


:TIME MEASUREMENTS:
----------------------------------------------------
Device set time: 0.002930 (ms)
Copy from host to device: 10.306152 (ms)
Set up Textures:      0 (ms)
Copying Clusters to constant gpu-memory: 0.002197 (ms)
Kernel run time: 3.097900 (ms)
Copy results from device to host: 1.145020 (ms)
Overall reduction time: 25.830078 (ms)
Total time: 40.402100 (ms)


:TIME MEASUREMENTS:
----------------------------------------------------
Device set time: 0.006104 (ms)
Copy from host to device: 1.218994 (ms)
Set up Textures:      0 (ms)
Copying Clusters to constant gpu-memory: 0.000977 (ms)
Kernel run time: 3.101074 (ms)
Copy results from device to host: 1.147949 (ms)
Overall reduction time: 26.685059 (ms)
Total time: 32.167236 (ms)
iterated 2 times
Number of Iteration: 1


 RESULTS:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Setup overall time (prior printf included): 1675.454834 (ms)
Cluster(some printf+KERNELxITERATIONS): 220.044189 (ms)
----------------------------------------------

kdd_cup:


I/O completed

Number of objects: 494020
Number of features: 34


:TIME MEASUREMENTS:
----------------------------------------------------
Device set time: 0.001953 (ms)
Copy from host to device: 6.805908 (ms)
Set up Textures:      0 (ms)
Copying Clusters to constant gpu-memory: 0.001953 (ms)
Kernel run time: 2.560059 (ms)
Copy results from device to host: 0.784912 (ms)
Overall reduction time: 15.677002 (ms)
Total time: 25.847900 (ms)


:TIME MEASUREMENTS:
----------------------------------------------------
Device set time: 0.005859 (ms)
Copy from host to device: 0.804199 (ms)
Set up Textures:      0 (ms)
Copying Clusters to constant gpu-memory: 0.000977 (ms)
Kernel run time: 2.524170 (ms)
Copy results from device to host: 0.757812 (ms)
Overall reduction time: 16.103027 (ms)
Total time: 20.203857 (ms)
iterated 2 times
Number of Iteration: 1


 RESULTS:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Setup overall time (prior printf included): 1070.903076 (ms)
Cluster(some printf+KERNELxITERATIONS): 170.383057 (ms)
----------------------------------------------

