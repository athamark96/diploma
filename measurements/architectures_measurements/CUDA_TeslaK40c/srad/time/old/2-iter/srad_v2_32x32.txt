

Block=(32,32,1)

-----------------------------------------------------------------------

WG size of kernel = 32 X 32
Start the SRAD main loop
Time spent in different stages of the application:
  0.000976562500 ms,  0.000027731798 % : SETUP VARIABLES
  0.024169921875 ms,  0.000686361978 % : READ COMMAND LINE PARAMETERS
3498.686767578125 ms, 99.353469848633 % : GPU DRIVER INIT, CPU/GPU SETUP, MEMORY ALLOCATION
  4.929199218750 ms,  0.139976248145 % : COPY DATA TO CPU->GPU
  0.020996093750 ms,  0.000596233644 % : SRAD_KERNEL_1
  0.005859375000 ms,  0.000166390790 % : SRAD_KERNEL_2
  7.757080078125 ms,  0.220280602574 % : COPY DATA TO GPU->CPU
  0.012939453125 ms,  0.000367446308 % : CUDA SYNCHRONIZE
 10.016113281250 ms,  0.284431189299 % : FREE MEMORY
Total time:
3521.454101562500 ms
=======================================================================

