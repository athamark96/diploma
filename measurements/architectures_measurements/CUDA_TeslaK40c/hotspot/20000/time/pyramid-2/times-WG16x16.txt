-----SIZE=1024x1024, ITERATIONS=20000, WITH ...--------------

results:

WG size of kernel = 16 X 16
pyramidHeight: 2
gridSize: [1024, 1024]
border:[2, 2]
blockGrid:[86, 86]
targetBlock:[12, 12]
Start computing the transient temperature
Ending simulation

Time total: 	5.295703 sec
Time for CUDA kernel:	3.448657 sec
Time for CUDA kernel+cudamallocs+memcopies+frees:	4.773980 sec
=========================================================

-------SIZE=512x512, ITERATIONS=20000, WITH ...--------------

results:

WG size of kernel = 16 X 16
pyramidHeight: 2
gridSize: [512, 512]
border:[2, 2]
blockGrid:[43, 43]
targetBlock:[12, 12]
Start computing the transient temperature
Ending simulation

Time total: 	1.458522 sec
Time for CUDA kernel:	0.910030 sec
Time for CUDA kernel+cudamallocs+memcopies+frees:	1.317486 sec
=========================================================

---------SIZE=64x64, ITERATIONS=20000, WITH ...--------------

results:

WG size of kernel = 16 X 16
pyramidHeight: 2
gridSize: [64, 64]
border:[2, 2]
blockGrid:[6, 6]
targetBlock:[12, 12]
Start computing the transient temperature
Ending simulation

Time total: 	0.176253 sec
Time for CUDA kernel:	0.074966 sec
Time for CUDA kernel+cudamallocs+memcopies+frees:	0.160856 sec
=========================================================

