-----SIZE=1024x1024, ITERATIONS=20000, WITH ...--------------

results:

WG size of kernel = 32 X 32
pyramidHeight: 2
gridSize: [1024, 1024]
border:[2, 2]
blockGrid:[37, 37]
targetBlock:[28, 28]
Start computing the transient temperature
Ending simulation

Time total: 	6.117439 sec
Time for CUDA kernel:	4.180591 sec
Time for CUDA kernel+cudamallocs+memcopies+frees:	5.595285 sec
=========================================================

-------SIZE=512x512, ITERATIONS=20000, WITH ...--------------

results:

WG size of kernel = 32 X 32
pyramidHeight: 2
gridSize: [512, 512]
border:[2, 2]
blockGrid:[19, 19]
targetBlock:[28, 28]
Start computing the transient temperature
Ending simulation

Time total: 	1.721027 sec
Time for CUDA kernel:	1.149990 sec
Time for CUDA kernel+cudamallocs+memcopies+frees:	1.579940 sec
=========================================================

---------SIZE=64x64, ITERATIONS=20000, WITH ...--------------

results:

WG size of kernel = 32 X 32
pyramidHeight: 2
gridSize: [64, 64]
border:[2, 2]
blockGrid:[3, 3]
targetBlock:[28, 28]
Start computing the transient temperature
Ending simulation

Time total: 	0.187729 sec
Time for CUDA kernel:	0.083977 sec
Time for CUDA kernel+cudamallocs+memcopies+frees:	0.172121 sec
=========================================================

