-----SIZE=1024x1024, ITERATIONS=20000, WITH ...--------------

results:

WG size of kernel = 16 X 16
pyramidHeight: 1
gridSize: [1024, 1024]
border:[1, 1]
blockGrid:[74, 74]
targetBlock:[14, 14]
Start computing the transient temperature
Ending simulation

Time total: 	6.122989 sec
Time for CUDA kernel:	4.213311 sec
Time for CUDA kernel+cudamallocs+memcopies+frees:	5.599910 sec
=========================================================

-------SIZE=512x512, ITERATIONS=20000, WITH ...--------------

results:

WG size of kernel = 16 X 16
pyramidHeight: 1
gridSize: [512, 512]
border:[1, 1]
blockGrid:[37, 37]
targetBlock:[14, 14]
Start computing the transient temperature
Ending simulation

Time total: 	1.673617 sec
Time for CUDA kernel:	1.156850 sec
Time for CUDA kernel+cudamallocs+memcopies+frees:	1.530083 sec
=========================================================

---------SIZE=64x64, ITERATIONS=20000, WITH ...--------------

results:

WG size of kernel = 16 X 16
pyramidHeight: 1
gridSize: [64, 64]
border:[1, 1]
blockGrid:[5, 5]
targetBlock:[14, 14]
Start computing the transient temperature
Ending simulation

Time total: 	0.242022 sec
Time for CUDA kernel:	0.141411 sec
Time for CUDA kernel+cudamallocs+memcopies+frees:	0.226846 sec
=========================================================

