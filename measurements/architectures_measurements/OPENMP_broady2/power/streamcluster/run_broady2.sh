cd /home/users/amarkou/DIPLOMA/rodinia_3.1/POWER_ENERGY/cpu/streamcluster

make clean
make

echo -e "\n========================================================\n"
echo -e "Dataset1:\n\n"
echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv
echo -e "omp threads,pgain,pgain_dist,pgain_init,pselect,pspeedy,pshuffle,local search,total time" >> time_succint.csv

./sc_omp 10 20 256 65536 65536 1000 none output.txt 1

for N in 2 4 8 10 20 30 40
do

        echo -e "OMP_NUM_THREADS=$N \n"
        ./sc_omp 10 20 256 65536 65536 1000 none output.txt $N
        echo -e "--------------------------\n"

done

rm output.txt
mv papi_measurements_succint.csv pdata1.csv

#####################################################################################

echo -e "\n\n\n\nDataset2:\n\n"

echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv

echo -e "omp threads,pgain,pgain_dist,pgain_init,pselect,pspeedy,pshuffle,local search,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./sc_omp 10 20 256 131072 65536 1000 none output.txt 1
echo -e "--------------------------\n"

for KN in 2 4 8 10 20 30 40 
do

	echo -e "OMP_NUM_THREADS=$KN \n"
	./sc_omp 10 20 256 131072 65536 1000 none output.txt $KN
	echo -e "--------------------------\n"

done 

rm output.txt
mv papi_measurements_succint.csv pdata2.csv


#####################################################################################

echo -e "\n\n\n\nDataset3:\n\n"

echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv


echo -e "omp threads,pgain,pgain_dist,pgain_init,pselect,pspeedy,pshuffle,local search,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./sc_omp 10 20 256 196608 65536 1000 none output.txt 1
echo -e "--------------------------\n"

for LN in 2 4 8 10 20 30 40 
do

	echo -e "OMP_NUM_THREADS=$LN \n"
	./sc_omp 10 20 256 196608 65536 1000 none output.txt $LN
	echo -e "--------------------------\n"

done
 
rm output.txt
mv papi_measurements_succint.csv pdata3.csv

#####################################################################################

echo -e "\n\n\n\nDataset4:\n\n"

echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv

echo -e "omp threads,pgain,pgain_dist,pgain_init,pselect,pspeedy,pshuffle,local search,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./sc_omp 10 20 256 262144 65536 1000 none output.txt 1
echo -e "--------------------------\n"

for TN in 2 4 8 10 20 30 40 
do

	echo -e "OMP_NUM_THREADS=$TN \n"
	./sc_omp 10 20 256 262144 65536 1000 none output.txt $TN
	echo -e "--------------------------\n"

done 
rm output.txt
mv papi_measurements_succint.csv pdata4.csv


