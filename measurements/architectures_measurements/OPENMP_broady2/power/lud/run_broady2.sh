cd /home/users/amarkou/DIPLOMA/rodinia_3.1/POWER_ENERGY/cpu/lud

make clean 
make

echo -e "================================================================\n"
echo -e "PROBLEM SIZE=64\n"
		echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv
echo -e "---------------\n"
echo -e "with OMP_NUM_THREADS= 1 \n \n"
./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat

for N in 2 4 8 10 20 30 40 
do
        echo -e "-------------------------------------------------------------------------------------------\n"
        echo -e "with OMP_NUM_THREADS= $N \n \n"
        ./omp/lud_omp -n $N -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
done 
mv papi_measurements_succint.csv pdata_64.csv




echo -e "===========================================================================================\n"
echo -e "PROBLEM SIZE=256\n"
                echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv
echo -e "----------------\n"
echo -e "with OMP_NUM_THREADS= 1 \n \n"
./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat

for Z in 2 4 8 10 20 30 40
do
        echo -e "-------------------------------------------------------------------------------------------\n"
        echo -e "with OMP_NUM_THREADS= $Z \n \n"
        ./omp/lud_omp -n $Z -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
done
mv papi_measurements_succint.csv pdata_256.csv




echo -e "============================================================================================\n"
echo -e "PROBLEM SIZE=512\n"
                echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv
echo -e "----------------\n"
echo -e "with OMP_NUM_THREADS= 1 \n \n"
./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat

for T in 2 4 8 10 20 30 40
do
        echo -e "--------------------------------------------------------------------------------------------\n"
        echo -e "with OMP_NUM_THREADS= $T \n \n"
        ./omp/lud_omp -n $T -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
done
mv papi_measurements_succint.csv pdata_512.csv




echo -e "=============================================================================================\n"
echo -e "PROBLEM SIZE=2048\n"
                echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv
echo -e "-----------------\n"
echo -e "    with OMP_NUM_THREADS= 1 \n \n"
./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat

for K in 2 4 8 10 20 30 40
do
        echo -e "---------------------------------------------------------------------------------------------\n"
        echo -e "    with OMP_NUM_THREADS= $K \n \n"
        ./omp/lud_omp -n $K -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
done
mv papi_measurements_succint.csv pdata_2048.csv

