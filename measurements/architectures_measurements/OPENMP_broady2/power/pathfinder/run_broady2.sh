cd /home/users/amarkou/DIPLOMA/rodinia_3.1/POWER_ENERGY/cpu/pathfinder


make clean
make


echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv
echo -e "[CASE1: Size = 100k x 100]\n"
echo -e "-------------------------------------------------------------------------------------------------------------\n"

echo -e "OMP_NUM_THREADS=1 \n"
export OMP_NUM_THREADS=1
./pathfinder 100000 100
echo -e "---------------------------------------------------------------------------\n"


for N in 2 4 8 10 20 30 40  
do
	echo -e "OMP_NUM_THREADS=$N\n"
	export OMP_NUM_THREADS=$N
	./pathfinder 100000 100
	echo -e "------------------------------------------------------------------------------\n"
done 
mv papi_measurements_succint.csv pdata_100kx100.csv



echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv
echo -e "\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
echo -e "[CASE2: Size = 100k x 1k]\n"
echo -e "-------------------------------------------------------------------------------------------------------------\n"

echo -e "OMP_NUM_THREADS=1 \n"
export OMP_NUM_THREADS=1
./pathfinder 100000 1000
echo -e "-----------------------------------------------------------------------------\n"


for M in 2 4 8 10 20 30 40
do
        echo -e "OMP_NUM_THREADS=$M\n"
        export OMP_NUM_THREADS=$M
        ./pathfinder 100000 1000
        echo -e "---------------------------------------------------------------------------------\n"
done
mv papi_measurements_succint.csv pdata_100kx1k.csv







echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv
echo -e "\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
echo -e "[CASE3: Size = 100k x 10k]\n"
echo -e "--------------------------------------------------------------------------------------------------------------\n"

echo -e "OMP_NUM_THREADS=1 \n"
export OMP_NUM_THREADS=1
./pathfinder 100000 10000
echo -e "-----------------------------------------------------------------------------\n"


for P in 2 4 8 10 20 30 40
do
        echo -e "OMP_NUM_THREADS=$P\n"
        export OMP_NUM_THREADS=$P
        ./pathfinder 100000 10000
        echo -e "---------------------------------------------------------------------------------\n"
done
mv papi_measurements_succint.csv pdata_100kx10k.csv








echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv
echo -e "\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
echo -e "[CASE4: Size = 100k x 20k]\n"
echo -e "----------------------------------------------------------------------------------------------------------------\n"

echo -e "OMP_NUM_THREADS=1 \n"
export OMP_NUM_THREADS=1
./pathfinder 100000 20000
echo -e "-----------------------------------------------------------------------------\n"


for T in 2 4 8 10 20 30 40
do
        echo -e "OMP_NUM_THREADS=$T\n"
        export OMP_NUM_THREADS=$T
        ./pathfinder 100000 20000
        echo -e "--------------------------------------------------------------------------------\n"
done
mv papi_measurements_succint.csv pdata_100kx20k.csv



