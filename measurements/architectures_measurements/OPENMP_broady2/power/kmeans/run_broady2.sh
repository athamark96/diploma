cd /home/users/amarkou/DIPLOMA/rodinia_3.1/POWER_ENERGY/cpu/kmeans

make clean
make

##CASE 1: Input 100:
	echo -e "===============================================================================================\nFor input file:100\n"
	echo -e "-----------------------------------------------------------------------------------------------\n"

echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv
	
	echo -e "----------------------------------\n"

	## Estimate the overhead of spawning a single omp thread:
	./kmeans_openmp/kmeans -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/100
	echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
	
	for N in 2 4 8 10 20 30 40 
	do
		./kmeans_openmp/kmeans -n $N -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/100
		echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
	done 

mv papi_measurements_succint.csv papi_measurements_succint_100.csv















##CASE 2: Input 204800.txt
	echo -e "===============================================================================================\nFor input file:204800.txt\n"
	echo -e "-----------------------------------------------------------------------------------------------\n"

echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv

	echo -e "----------------------------------\n"

        ## Estimate the overhead of spawning a single omp thread:
        ./kmeans_openmp/kmeans -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/204800.txt
        echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"

	for K in 2 4 8 10 20 30 40
	do
        	./kmeans_openmp/kmeans -n $K -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/204800.txt
        	echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
	done

mv papi_measurements_succint.csv papi_measurements_succint_204800.csv












##CASE 3: Input 819200.txt
        echo -e "===============================================================================================\nFor input file:819200.txt\n"
        echo -e "-----------------------------------------------------------------------------------------------\n"
echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv

        echo -e "----------------------------------\n"

        ## Estimate the overhead of spawning a single omp thread:
        ./kmeans_openmp/kmeans -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/819200.txt
        echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"

        for L in 2 4 8 10 20 30 40
        do
                ./kmeans_openmp/kmeans -n $L -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/819200.txt
                echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
        done


mv papi_measurements_succint.csv papi_measurements_succint_819200.csv


















##CASE 4: Input kdd_cup
        echo -e "===============================================================================================\nFor input file:kdd_cup\n"
        echo -e "-----------------------------------------------------------------------------------------------\n"
        echo -e "Base Case:\n kmeans_serial execution for this input\n\n"
        
echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv

        echo -e "---------------------------------------------------------------------------\n"

        ## Estimate the overhead of spawning a single omp thread:
        ./kmeans_openmp/kmeans -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/kdd_cup
        echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"

        for M in 2 4 8 10 20 30 40
        do
                ./kmeans_openmp/kmeans -n $M -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/kdd_cup
                echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
        done

mv papi_measurements_succint.csv papi_measurements_succint_kdd_cup.csv

