cd ~/DIPLOMA/rodinia_3.1/POWER_ENERGY/cpu/nw

echo -e "==================================\nFor matrix:$1x$1\n"

##################################################################
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++\n"
echo -e "++++++++++++++OMP_NUM_THREADS=1++++++++++++++++\n" 
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++\n\n"

echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_succint.csv

./needle $1 10 1

echo -e "***********************************************\n"

####################################################################################
for t in 2 4 8 10 20 30 40
do

	########################################EXECUTIONS##########################################
	echo -e "+++++++++++++++++++++++++++++++++++++++++++++++\n" 
	echo -e "++++++++++++++OMP_NUM_THREADS=$t+++++++++++++\n"     
	echo -e "+++++++++++++++++++++++++++++++++++++++++++++++\n\n" 

        ./needle $1 10 $t
        echo -e "- - - - - - - - - - - - - - - - - - - - -\n"

done

mv papi_succint.csv papi_succint_$1.csv
