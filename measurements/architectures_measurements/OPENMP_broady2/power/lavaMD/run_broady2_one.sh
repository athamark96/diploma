openmp_exe=/home/users/amarkou/DIPLOMA/rodinia_3.1/POWER_ENERGY/cpu/lavaMD/lavaMD

echo -e "omp threads,var time, input arg time, dim cpu, sys mem/alo ,kernel,sys mem free,total time" >> time_succint.csv

echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv


echo -e "-------------------------------------------\n"
echo -e "omp_num_threads=1\n"
$openmp_exe -cores 1 -boxes1d $1
echo -e "===========================================\n"

for N in 2 4 8 10 20 30 40 
do
	echo -e "-------------------------------------------\n"
	echo -e "omp_num_threads=$N\n"
	$openmp_exe -cores $N -boxes1d $1
	echo -e "===========================================\n"
done 
