cd /home/users/amarkou/DIPLOMA/rodinia_3.1/POWER_ENERGY/cpu/hotspot3D

#######################################################################################################################################
echo -e "###########################################################################################################################\n"
echo -e "###################################CASE:512x8 [size:512x512, layers=8, iterations:10000]#####################################\n"
echo -e "###########################################################################################################################\n"

echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv

export OMP_NUM_THREADS=1
echo -e "num of omp threads = 1 \n"
./3D 512 8 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x8 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x8 output_512x8.out
echo -e "===========================================================================================\n"

for N in 2 4 8 10 20 30 40
do
	export OMP_NUM_THREADS=$N
	echo -e "num of omp threads = $N \n"
	./3D 512 8 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x8 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x8 output_512x8.out
	echo -e "===========================================================================================\n"
done
mv papi_measurements_succint.csv papi_succint_512x8.csv


######################################################################################################################################################
echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv

echo -e "\n\n\n\n\n"
echo -e "###########################################################################################################################\n"
echo -e "################################CASE:512x4 [size:512x512, layers=4, iterations:10000]########################################\n"
echo -e "###########################################################################################################################\n"
export OMP_NUM_THREADS=1
echo -e "num of omp threads = 1 \n"
./3D 512 4 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x4 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x4 output_512x4.out
echo -e "===========================================================================================\n"

for M in 2 4 8 10 20 30 40
do
	export OMP_NUM_THREADS=$M
        echo -e "num of omp threads = $M \n"
        ./3D 512 4 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x4 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x4 output_512x4.out
        echo -e "===========================================================================================\n"
done
mv papi_measurements_succint.csv papi_succint_512x4.csv


#######################################################################################################################################
echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv

echo -e "\n\n\n\n\n"
echo -e "###########################################################################################################################\n"
echo -e "##############################CASE:512x2 [size:512x512, layers=2, iterations:10000]##########################################\n"
echo -e "###########################################################################################################################\n"

export OMP_NUM_THREADS=1
echo -e "num of omp threads = 1 \n"
./3D 512 2 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x2 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x2 output_512x2.out
echo -e "===========================================================================================\n"
for K in 2 4 8 10 20 30 40
do
	export OMP_NUM_THREADS=$K
        echo -e "num of omp threads = $K \n"
        ./3D 512 2 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x2 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x2 output_512x2.out
        echo -e "===========================================================================================\n"
done
mv papi_measurements_succint.csv papi_succint_512x2.csv


#######################################################################################################################################
echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv

echo -e "\n\n\n\n\n"
echo -e "###########################################################################################################################\n"
echo -e "##################################CASE:64x8 [size:64x64, layers=8, iterations:10000]#########################################\n"
echo -e "###########################################################################################################################\n"

export OMP_NUM_THREADS=1
echo -e "num of omp threads = 1 \n"
./3D 64 8 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_64x8 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_64x8 output_64x8.out
echo -e "=========================================================\n"

for L in  2 4 8 10 20 30 40
do
     	export OMP_NUM_THREADS=$L
	echo -e "num of omp threads = $L \n"
        ./3D 64 8 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_64x8 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_64x8 output_64x8.out
        echo -e "=========================================================\n"
done
mv papi_measurements_succint.csv papi_succint_64x8.csv


#######################################################################################################################################
