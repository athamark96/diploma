cd ~/DIPLOMA/rodinia_3.1/POWER_ENERGY/cpu/srad/srad_v1

make clean
make

echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv

echo -e "omp threads,setup var time,read commands time,read image time,resize time,mem alloc time,extract time,kernel time,compress time,save time,free time,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./srad 100 0.5 502 458 1
echo -e "--------------------------\n"

for N in 2 4 8 10 20 30 40
do

        echo -e "OMP_NUM_THREADS=$N \n"
        ./srad 100 0.5 502 458 $N
        echo -e "--------------------------\n"

done

mv time_succint.csv time_succint_v1_100.csv
mv papi_measurements_succint.csv papi_succint_100.csv



#####################################################################################


echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv


echo -e "omp threads,setup var time,read commands time,read image time,resize time,mem alloc time,extract time,kernel time,compress time,save time,free time,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./srad 1000 0.5 502 458 1
echo -e "--------------------------\n"

for KN in 2 4 8 10 20 30 40
do

	echo -e "OMP_NUM_THREADS=$KN \n"
	./srad 1000 0.5 502 458 $KN
	echo -e "--------------------------\n"

done 

mv time_succint.csv time_succint_v1_1k.csv
mv papi_measurements_succint.csv papi_succint_1k.csv



#####################################################################################


echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv


echo -e "omp threads,setup var time,read commands time,read image time,resize time,mem alloc time,extract time,kernel time,compress time,save time,free time,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./srad 10000 0.5 502 458 1
echo -e "--------------------------\n"

for LN in 2 4 8 10 20 30 40
do

	echo -e "OMP_NUM_THREADS=$LN \n"
	./srad 10000 0.5 502 458 $LN
	echo -e "--------------------------\n"

done 
mv time_succint.csv time_succint_v1_10k.csv
mv papi_measurements_succint.csv papi_succint_10k.csv




#####################################################################################


echo -e "PACKAGE_ENERGY:PACKAGE0,PACKAGE_ENERGY:PACKAGE1,DRAM_ENERGY:PACKAGE0,DRAM_ENERGY:PACKAGE1,TOTAL_ENERGY,TOTAL_TIME,POWER" >> papi_measurements_succint.csv


echo -e "omp threads,setup var time,read commands time,read image time,resize time,mem alloc time,extract time,kernel time,compress time,save time,free time,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./srad 100000 0.5 502 458 1
echo -e "--------------------------\n"

for MN in 2 4 8 10 20 30 40
do

	echo -e "OMP_NUM_THREADS=$MN \n"
	./srad 100000 0.5 502 458 $MN
	echo -e "--------------------------\n"

done 

mv time_succint.csv time_succint_v1_100k.csv
mv papi_measurements_succint.csv papi_succint_100k.csv


