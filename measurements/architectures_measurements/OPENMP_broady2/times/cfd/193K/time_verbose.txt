+++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++OMP_NUM_THREADS=1++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++





-----ompnthreads=1-----


Starting...
Compute time: 252.274
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=1-----


Starting...
0.131231 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=1-----


Starting...
0.120054 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=1-----


Starting...
0.227246 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************

+++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++OMP_NUM_THREADS=2++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++





-----ompnthreads=2-----


Starting...
Compute time: 112.602
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=2-----


Starting...
0.0629007 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=2-----


Starting...
0.05987 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=2-----


Starting...
0.104477 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************

+++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++OMP_NUM_THREADS=4++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++





-----ompnthreads=4-----


Starting...
Compute time: 58.1854
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=4-----


Starting...
0.0324836 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=4-----


Starting...
0.0292332 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=4-----


Starting...
0.056517 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************

+++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++OMP_NUM_THREADS=8++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++





-----ompnthreads=8-----


Starting...
Compute time: 30.1304
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=8-----


Starting...
0.0165966 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=8-----


Starting...
0.0161973 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=8-----


Starting...
0.0289148 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************

+++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++OMP_NUM_THREADS=10++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++





-----ompnthreads=10-----


Starting...
Compute time: 23.6831
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=10-----


Starting...
0.0145583 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=10-----


Starting...
0.0127217 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=10-----


Starting...
0.0239514 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************

+++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++OMP_NUM_THREADS=20++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++





-----ompnthreads=20-----


Starting...
Compute time: 13.0865
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=20-----


Starting...
0.0075975 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=20-----


Starting...
0.0075701 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=20-----


Starting...
0.0147659 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************

+++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++OMP_NUM_THREADS=30++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++





-----ompnthreads=30-----


Starting...
Compute time: 14.7755
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=30-----


Starting...
0.00815552 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=30-----


Starting...
0.00803369 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=30-----


Starting...
0.0134812 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************

+++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++OMP_NUM_THREADS=40++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++





-----ompnthreads=40-----


Starting...
Compute time: 11.8499
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=40-----


Starting...
0.006508 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=40-----


Starting...
0.00620046 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************




-----ompnthreads=40-----


Starting...
0.0119248 seconds per iteration
Saving solution...
Saved solution...
Cleaning up...
Done...
*********************************************************************************************

