=====================================

PROBLEM SIZE=64

--------------------------------

    with OMP_NUM_THREADS= 1 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
running OMP on host
Time consumed(ms): 0.071000
--------------------------------

    with OMP_NUM_THREADS= 2 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
running OMP on host
Time consumed(ms): 0.143000
--------------------------------

    with OMP_NUM_THREADS= 4 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
running OMP on host
Time consumed(ms): 0.186000
--------------------------------

    with OMP_NUM_THREADS= 8 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
running OMP on host
Time consumed(ms): 0.281000
--------------------------------

    with OMP_NUM_THREADS= 10 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
running OMP on host
Time consumed(ms): 0.279000
--------------------------------

    with OMP_NUM_THREADS= 20 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
running OMP on host
Time consumed(ms): 2.597000
--------------------------------

    with OMP_NUM_THREADS= 30 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
running OMP on host
Time consumed(ms): 7.016000
--------------------------------

    with OMP_NUM_THREADS= 40 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
running OMP on host
Time consumed(ms): 5.096000
====================================

PROBLEM SIZE=256

--------------------------------

    with OMP_NUM_THREADS= 1 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
running OMP on host
Time consumed(ms): 2.167000
--------------------------------

    with OMP_NUM_THREADS= 2 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
running OMP on host
Time consumed(ms): 1.294000
--------------------------------

    with OMP_NUM_THREADS= 4 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
running OMP on host
Time consumed(ms): 0.835000
--------------------------------

    with OMP_NUM_THREADS= 8 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
running OMP on host
Time consumed(ms): 0.759000
--------------------------------

    with OMP_NUM_THREADS= 10 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
running OMP on host
Time consumed(ms): 0.894000
--------------------------------

    with OMP_NUM_THREADS= 20 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
running OMP on host
Time consumed(ms): 3.606000
--------------------------------

    with OMP_NUM_THREADS= 30 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
running OMP on host
Time consumed(ms): 5.725000
--------------------------------

    with OMP_NUM_THREADS= 40 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
running OMP on host
Time consumed(ms): 6.916000
====================================

PROBLEM SIZE=512

--------------------------------

    with OMP_NUM_THREADS= 1 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
running OMP on host
Time consumed(ms): 15.311000
--------------------------------

    with OMP_NUM_THREADS= 2 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
running OMP on host
Time consumed(ms): 7.992000
--------------------------------

    with OMP_NUM_THREADS= 4 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
running OMP on host
Time consumed(ms): 4.358000
--------------------------------

    with OMP_NUM_THREADS= 8 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
running OMP on host
Time consumed(ms): 2.695000
--------------------------------

    with OMP_NUM_THREADS= 10 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
running OMP on host
Time consumed(ms): 2.460000
--------------------------------

    with OMP_NUM_THREADS= 20 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
running OMP on host
Time consumed(ms): 3.656000
--------------------------------

    with OMP_NUM_THREADS= 30 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
running OMP on host
Time consumed(ms): 5.931000
--------------------------------

    with OMP_NUM_THREADS= 40 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
running OMP on host
Time consumed(ms): 8.136000
====================================

PROBLEM SIZE=2048

--------------------------------

    with OMP_NUM_THREADS= 1 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
running OMP on host
Time consumed(ms): 887.122000
--------------------------------

    with OMP_NUM_THREADS= 2 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
running OMP on host
Time consumed(ms): 446.846000
--------------------------------

    with OMP_NUM_THREADS= 4 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
running OMP on host
Time consumed(ms): 226.229000
--------------------------------

    with OMP_NUM_THREADS= 8 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
running OMP on host
Time consumed(ms): 114.204000
--------------------------------

    with OMP_NUM_THREADS= 10 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
running OMP on host
Time consumed(ms): 92.390000
--------------------------------

    with OMP_NUM_THREADS= 20 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
running OMP on host
Time consumed(ms): 89.582000
--------------------------------

    with OMP_NUM_THREADS= 30 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
running OMP on host
Time consumed(ms): 70.777000
--------------------------------

    with OMP_NUM_THREADS= 40 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
running OMP on host
Time consumed(ms): 112.300000
