[CASE1: Size = 100k x 100]

-----------------------------------------------------

OMP_NUM_THREADS set to DEFAULT:

Time: 29049 (micro seconds)
-----------------------------------------------------

OMP_NUM_THREADS=1 

Time: 208665 (micro seconds)
-----------------------------------------------------

OMP_NUM_THREADS=2

Time: 104825 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=4

Time: 53867 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=8

Time: 27180 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=10

Time: 22028 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=20

Time: 17477 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=30

Time: 14160 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=40

Time: 14448 (micro seconds)
--------------------------------------------------------

+++++++++++++++++++++++++++++++++++++++++++++++++++++

[CASE2: Size = 100k x 1k]

-----------------------------------------------------

OMP_NUM_THREADS set to DEFAULT:

Time: 103853 (micro seconds)
-----------------------------------------------------

OMP_NUM_THREADS=1 

Time: 2119465 (micro seconds)
-----------------------------------------------------

OMP_NUM_THREADS=2

Time: 1067958 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=4

Time: 541974 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=8

Time: 274522 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=10

Time: 220528 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=20

Time: 172550 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=30

Time: 128026 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=40

Time: 106317 (micro seconds)
--------------------------------------------------------

+++++++++++++++++++++++++++++++++++++++++++++++++++++

[CASE3: Size = 100k x 10k]

-----------------------------------------------------

OMP_NUM_THREADS set to DEFAULT:

Time: 1016540 (micro seconds)
-----------------------------------------------------

OMP_NUM_THREADS=1 

Time: 21142834 (micro seconds)
-----------------------------------------------------

OMP_NUM_THREADS=2

Time: 10674204 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=4

Time: 5398125 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=8

Time: 2757059 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=10

Time: 2222103 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=20

Time: 1459618 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=30

Time: 1208545 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=40

Time: 998685 (micro seconds)
--------------------------------------------------------

+++++++++++++++++++++++++++++++++++++++++++++++++++++

[CASE4: Size = 100k x 20k]

-----------------------------------------------------

OMP_NUM_THREADS set to DEFAULT:

Time: 2011042 (micro seconds)
-----------------------------------------------------

OMP_NUM_THREADS=1 

Time: 42347371 (micro seconds)
-----------------------------------------------------

OMP_NUM_THREADS=2

Time: 21367346 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=4

Time: 10799875 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=8

Time: 5488968 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=10

Time: 4430274 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=20

Time: 2728047 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=30

Time: 2429793 (micro seconds)
--------------------------------------------------------

OMP_NUM_THREADS=40

Time: 2002315 (micro seconds)
--------------------------------------------------------

+++++++++++++++++++++++++++++++++++++++++++++++++++++

[CASE5: Size = 100k x 100k]

-----------------------------------------------------

OMP_NUM_THREADS set to DEFAULT:

./run2.sh: line 130: 39138 Segmentation fault      ./pathfinder 100000 100000
-----------------------------------------------------

OMP_NUM_THREADS=1 

./run2.sh: line 135: 39141 Segmentation fault      ./pathfinder 100000 100000
-----------------------------------------------------

OMP_NUM_THREADS=2

./run2.sh: line 139: 39142 Segmentation fault      ./pathfinder 100000 100000
--------------------------------------------------------

OMP_NUM_THREADS=4

./run2.sh: line 139: 39145 Segmentation fault      ./pathfinder 100000 100000
--------------------------------------------------------

OMP_NUM_THREADS=8

./run2.sh: line 139: 39148 Segmentation fault      ./pathfinder 100000 100000
--------------------------------------------------------

OMP_NUM_THREADS=10

./run2.sh: line 139: 39151 Segmentation fault      ./pathfinder 100000 100000
--------------------------------------------------------

OMP_NUM_THREADS=20

./run2.sh: line 139: 39152 Segmentation fault      ./pathfinder 100000 100000
--------------------------------------------------------

OMP_NUM_THREADS=30

./run2.sh: line 139: 39155 Segmentation fault      ./pathfinder 100000 100000
--------------------------------------------------------

OMP_NUM_THREADS=40

./run2.sh: line 139: 39158 Segmentation fault      ./pathfinder 100000 100000
--------------------------------------------------------

