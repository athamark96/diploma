OMP_NUM_THREADS=1 

PARSEC Benchmark Suite
read 65536 points
finish local search
time = 51.693991
time pgain = 50.506473
time pgain_dist = 49.904951
time pgain_init = 0.204703
time pselect = 0.000290
time pspeedy = 0.953680
time pshuffle = 0.006488
time localSearch = 51.490926
loops=7
--------------------------

OMP_NUM_THREADS=2 

PARSEC Benchmark Suite
read 65536 points
finish local search
time = 55.689111
time pgain = 54.873512
time pgain_dist = 54.265314
time pgain_init = 0.208955
time pselect = 0.000293
time pspeedy = 0.560477
time pshuffle = 0.006452
time localSearch = 55.464041
loops=7
--------------------------

OMP_NUM_THREADS=4 

PARSEC Benchmark Suite
read 65536 points
finish local search
time = 18.275401
time pgain = 17.480038
time pgain_dist = 17.044990
time pgain_init = 0.206222
time pselect = 0.000289
time pspeedy = 0.560379
time pshuffle = 0.006464
time localSearch = 18.070461
loops=7
--------------------------

OMP_NUM_THREADS=8 

PARSEC Benchmark Suite
read 65536 points
finish local search
time = 12.818198
time pgain = 12.021380
time pgain_dist = 11.493889
time pgain_init = 0.228928
time pselect = 0.000292
time pspeedy = 0.561835
time pshuffle = 0.006459
time localSearch = 12.613291
loops=7
--------------------------

OMP_NUM_THREADS=10 

PARSEC Benchmark Suite
read 65536 points
finish local search
time = 12.931540
time pgain = 12.136284
time pgain_dist = 10.780253
time pgain_init = 0.264684
time pselect = 0.000289
time pspeedy = 0.561032
time pshuffle = 0.006436
time localSearch = 12.727200
loops=7
--------------------------

OMP_NUM_THREADS=20 

PARSEC Benchmark Suite
read 65536 points
finish local search
time = 9.163560
time pgain = 8.360415
time pgain_dist = 6.184839
time pgain_init = 0.211653
time pselect = 0.000291
time pspeedy = 0.560694
time pshuffle = 0.006436
time localSearch = 8.951424
loops=7
--------------------------

OMP_NUM_THREADS=30 

PARSEC Benchmark Suite
read 65536 points
finish local search
time = 6.890850
time pgain = 6.096104
time pgain_dist = 4.039001
time pgain_init = 0.189258
time pselect = 0.000298
time pspeedy = 0.559175
time pshuffle = 0.006423
time localSearch = 6.684817
loops=7
--------------------------

OMP_NUM_THREADS=40 

PARSEC Benchmark Suite
read 65536 points
finish local search
time = 6.185495
time pgain = 5.390517
time pgain_dist = 3.318494
time pgain_init = 0.277117
time pselect = 0.000287
time pspeedy = 0.560929
time pshuffle = 0.006392
time localSearch = 5.980625
loops=7
--------------------------

