


Kernel dimension=128:

thread block size of kernel = 128 
Configuration used: boxes1d = 10
Time spent in different stages of GPU_CUDA KERNEL:
 0.064859002829 s, 50.000770568848 % : GPU: SET DEVICE / DRIVER INIT
 0.000479000009 s,  0.369268238544 % : GPU MEM: ALO
 0.002347999951 s,  1.810108304024 % : GPU MEM: COPY IN
 0.060653999448 s, 46.759075164795 % : GPU: KERNEL
 0.001025000005 s,  0.790187835693 % : GPU MEM: COPY OUT
 0.000350999995 s,  0.270591139793 % : GPU MEM: FRE
Total time:
0.129715994000 s
thread block size of kernel = 128 
Configuration used: boxes1d = 20
Time spent in different stages of GPU_CUDA KERNEL:
 0.054930001497 s,  9.027353286743 % : GPU: SET DEVICE / DRIVER INIT
 0.000500000024 s,  0.082171425223 % : GPU MEM: ALO
 0.016092000529 s,  2.644605159760 % : GPU MEM: COPY IN
 0.530454993248 s, 87.176490783691 % : GPU: KERNEL
 0.006118000019 s,  1.005449652672 % : GPU MEM: COPY OUT
 0.000388999993 s,  0.063929371536 % : GPU MEM: FRE
Total time:
0.608484029770 s
thread block size of kernel = 128 
Configuration used: boxes1d = 30
Time spent in different stages of GPU_CUDA KERNEL:
 0.056258000433 s,  2.791424036026 % : GPU: SET DEVICE / DRIVER INIT
 0.000615000026 s,  0.030515233055 % : GPU MEM: ALO
 0.067892000079 s,  3.368683099747 % : GPU MEM: COPY IN
 1.852084994316 s, 91.897239685059 % : GPU: KERNEL
 0.038013000041 s,  1.886139035225 % : GPU MEM: COPY OUT
 0.000523999974 s,  0.025999968871 % : GPU MEM: FRE
Total time:
2.015387058258 s
thread block size of kernel = 128 
Configuration used: boxes1d = 40
Time spent in different stages of GPU_CUDA KERNEL:
 0.054712999612 s,  1.140832185745 % : GPU: SET DEVICE / DRIVER INIT
 0.001254000003 s,  0.026147417724 % : GPU MEM: ALO
 0.195423007011 s,  4.074806213379 % : GPU MEM: COPY IN
 4.463786125183 s, 93.075332641602 % : GPU: KERNEL
 0.080220997334 s,  1.672704935074 % : GPU MEM: COPY OUT
 0.000487999991 s,  0.010175389238 % : GPU MEM: FRE
Total time:
4.795885086060 s
thread block size of kernel = 128 
Configuration used: boxes1d = 50
Cuda error: Start: invalid configuration argument.
=================================================


=================================================





Kernel dimension=256:

thread block size of kernel = 256 
Configuration used: boxes1d = 10
Time spent in different stages of GPU_CUDA KERNEL:
 0.068341001868 s, 48.548332214355 % : GPU: SET DEVICE / DRIVER INIT
 0.000483999989 s,  0.343825697899 % : GPU MEM: ALO
 0.002525999909 s,  1.794429063797 % : GPU MEM: COPY IN
 0.067849002779 s, 48.198822021484 % : GPU: KERNEL
 0.001201999956 s,  0.853881120682 % : GPU MEM: COPY OUT
 0.000367000001 s,  0.260710805655 % : GPU MEM: FRE
Total time:
0.140769004822 s
thread block size of kernel = 256 
Configuration used: boxes1d = 20
Time spent in different stages of GPU_CUDA KERNEL:
 0.055757001042 s,  8.216547966003 % : GPU: SET DEVICE / DRIVER INIT
 0.000510999991 s,  0.075302757323 % : GPU MEM: ALO
 0.015217999928 s,  2.242578029633 % : GPU MEM: COPY IN
 0.597931981087 s, 88.113365173340 % : GPU: KERNEL
 0.008797000162 s,  1.296356916428 % : GPU MEM: COPY OUT
 0.000379000005 s,  0.055850774050 % : GPU MEM: FRE
Total time:
0.678593993187 s
thread block size of kernel = 256 
Configuration used: boxes1d = 30
Time spent in different stages of GPU_CUDA KERNEL:
 0.055178001523 s,  2.444762706757 % : GPU: SET DEVICE / DRIVER INIT
 0.000791999977 s,  0.035091016442 % : GPU MEM: ALO
 0.075942002237 s,  3.364749908447 % : GPU MEM: COPY IN
 2.087707042694 s, 92.499694824219 % : GPU: KERNEL
 0.036890000105 s,  1.634479284286 % : GPU MEM: COPY OUT
 0.000479000009 s,  0.021222975105 % : GPU MEM: FRE
Total time:
2.256988048553 s
thread block size of kernel = 256 
Configuration used: boxes1d = 40
Time spent in different stages of GPU_CUDA KERNEL:
 0.056444000453 s,  1.058493137360 % : GPU: SET DEVICE / DRIVER INIT
 0.000845999981 s,  0.015865020454 % : GPU MEM: ALO
 0.160459995270 s,  3.009103059769 % : GPU MEM: COPY IN
 5.034084796906 s, 94.404090881348 % : GPU: KERNEL
 0.080182999372 s,  1.503670096397 % : GPU MEM: COPY OUT
 0.000468000013 s,  0.008776394650 % : GPU MEM: FRE
Total time:
5.332486152649 s
thread block size of kernel = 256 
Configuration used: boxes1d = 50
Cuda error: Start: invalid configuration argument.
=================================================


=================================================





Kernel dimension=512:

thread block size of kernel = 512 
Configuration used: boxes1d = 10
Time spent in different stages of GPU_CUDA KERNEL:
 1.140676021576 s, 90.782882690430 % : GPU: SET DEVICE / DRIVER INIT
 0.000513000006 s,  0.040828082711 % : GPU MEM: ALO
 0.003644000040 s,  0.290014714003 % : GPU MEM: COPY IN
 0.110142998397 s,  8.765940666199 % : GPU: KERNEL
 0.001149000018 s,  0.091445364058 % : GPU MEM: COPY OUT
 0.000362999999 s,  0.028890049085 % : GPU MEM: FRE
Total time:
1.256487965584 s
thread block size of kernel = 512 
Configuration used: boxes1d = 20
Time spent in different stages of GPU_CUDA KERNEL:
 1.209179997444 s, 54.633438110352 % : GPU: SET DEVICE / DRIVER INIT
 0.000630999974 s,  0.028509981930 % : GPU MEM: ALO
 0.016077000648 s,  0.726394534111 % : GPU MEM: COPY IN
 0.974988996983 s, 44.052169799805 % : GPU: KERNEL
 0.011909999885 s,  0.538120210171 % : GPU MEM: COPY OUT
 0.000472999993 s,  0.021371189505 % : GPU MEM: FRE
Total time:
2.213259935379 s
thread block size of kernel = 512 
Configuration used: boxes1d = 30
Time spent in different stages of GPU_CUDA KERNEL:
 0.054558999836 s,  1.528862357140 % : GPU: SET DEVICE / DRIVER INIT
 0.000829000026 s,  0.023230392486 % : GPU MEM: ALO
 0.068371996284 s,  1.915932774544 % : GPU MEM: COPY IN
 3.407445907593 s, 95.484085083008 % : GPU: KERNEL
 0.036906998605 s,  1.034214854240 % : GPU MEM: COPY OUT
 0.000487999991 s,  0.013674826361 % : GPU MEM: FRE
Total time:
3.568600893021 s
thread block size of kernel = 512 
Configuration used: boxes1d = 40
Time spent in different stages of GPU_CUDA KERNEL:
 0.053732000291 s,  0.630951583385 % : GPU: SET DEVICE / DRIVER INIT
 0.000855999999 s,  0.010051636025 % : GPU MEM: ALO
 0.167772993445 s,  1.970085501671 % : GPU MEM: COPY IN
 8.212909698486 s, 96.440643310547 % : GPU: KERNEL
 0.080278001726 s,  0.942669689655 % : GPU MEM: COPY OUT
 0.000476999994 s,  0.005601204466 % : GPU MEM: FRE
Total time:
8.516025543213 s
thread block size of kernel = 512 
Configuration used: boxes1d = 50
Cuda error: Start: invalid configuration argument.
=================================================


=================================================





Kernel dimension=1024:

thread block size of kernel = 1024 
Configuration used: boxes1d = 10
Cuda error: Start: too many resources requested for launch.
thread block size of kernel = 1024 
Configuration used: boxes1d = 20
Cuda error: Start: too many resources requested for launch.
thread block size of kernel = 1024 
Configuration used: boxes1d = 30
Cuda error: Start: too many resources requested for launch.
thread block size of kernel = 1024 
Configuration used: boxes1d = 40
Cuda error: Start: too many resources requested for launch.
thread block size of kernel = 1024 
Configuration used: boxes1d = 50
Cuda error: Start: invalid configuration argument.
=================================================


=================================================


