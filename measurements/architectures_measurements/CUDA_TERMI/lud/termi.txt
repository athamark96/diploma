==============================

WG= 16x16



	64:

WG size of kernel = 16 X 16
Generate input matrix internally, size =64
Creating matrix internally size=64
Before LUD
Time consumed(ms): 0.496000
After LUD
>>>Verify<<<<
---------------------

	256:

WG size of kernel = 16 X 16
Generate input matrix internally, size =256
Creating matrix internally size=256
Before LUD
Time consumed(ms): 2.508000
After LUD
>>>Verify<<<<
---------------------

	512:

WG size of kernel = 16 X 16
Generate input matrix internally, size =512
Creating matrix internally size=512
Before LUD
Time consumed(ms): 6.057000
After LUD
>>>Verify<<<<
---------------------

	1024:

WG size of kernel = 16 X 16
Generate input matrix internally, size =1024
Creating matrix internally size=1024
Before LUD
Time consumed(ms): 15.776000
After LUD
>>>Verify<<<<
---------------------

	2048:

WG size of kernel = 16 X 16
Generate input matrix internally, size =2048
Creating matrix internally size=2048
Before LUD
Time consumed(ms): 75.224000
After LUD
>>>Verify<<<<
---------------------

	4096:

WG size of kernel = 16 X 16
Generate input matrix internally, size =4096
Creating matrix internally size=4096
Before LUD
Time consumed(ms): 379.222000
After LUD
>>>Verify<<<<
---------------------

	8192:

==============================

WG= 32x32



	64:

WG size of kernel = 32 X 32
Generate input matrix internally, size =64
Creating matrix internally size=64
Before LUD
Time consumed(ms): 1.814000
After LUD
>>>Verify<<<<
---------------------

	256:

WG size of kernel = 32 X 32
Generate input matrix internally, size =256
Creating matrix internally size=256
Before LUD
Time consumed(ms): 10.465000
After LUD
>>>Verify<<<<
---------------------

	512:

WG size of kernel = 32 X 32
Generate input matrix internally, size =512
Creating matrix internally size=512
Before LUD
Time consumed(ms): 22.781000
After LUD
>>>Verify<<<<
---------------------

	1024:

WG size of kernel = 32 X 32
Generate input matrix internally, size =1024
Creating matrix internally size=1024
Before LUD
Time consumed(ms): 50.045000
After LUD
>>>Verify<<<<
---------------------

	2048:

WG size of kernel = 32 X 32
Generate input matrix internally, size =2048
Creating matrix internally size=2048
Before LUD
Time consumed(ms): 139.530000
After LUD
>>>Verify<<<<
---------------------

	4096:

WG size of kernel = 32 X 32
Generate input matrix internally, size =4096
Creating matrix internally size=4096
Before LUD
Time consumed(ms): 584.181000
After LUD
>>>Verify<<<<
---------------------

