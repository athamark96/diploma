openmp_exe=/home/users/amarkou/DIPLOMA/rodinia_3.1/openmp/sandman_1_node/hotspot3D/3D

####################################################################################################################################
echo -e "512x8\nomp threads, time, accuracy " >> time_succint.csv
echo -e "CASE:512x8 [size:512x512, layers=8, iterations:10000]\n"
export OMP_NUM_THREADS=1
echo -e "num of omp threads = 1 \n"
$openmp_exe 512 8 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x8 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x8 output_512x8.out
echo -e "=========================================================\n"

for N in 2 4 8 12 16
do
	export OMP_NUM_THREADS=$N
	echo -e "num of omp threads = $N \n"
	$openmp_exe 512 8 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x8 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x8 output_512x8.out
	echo -e "=========================================================\n"
done
echo -e "\n\n\n\n\n" >> time_succint.csv
####################################################################################################################################



echo -e "512x4\nomp threads, time, accuracy " >> time_succint.csv
echo -e "\n\nCASE:512x4 [size:512x512, layers=4, iterations:10000] \n"
export OMP_NUM_THREADS=1
echo -e "num of omp threads = 1 \n"
$openmp_exe 512 4 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x4 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x4 output_512x4.out
echo -e "=========================================================\n"

for M in 2 4 8 12 16
do
	export OMP_NUM_THREADS=$M
        echo -e "num of omp threads = $M \n"
        $openmp_exe 512 4 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x4 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x4 output_512x4.out
        echo -e "=========================================================\n"
done
echo -e "\n\n\n\n\n" >> time_succint.csv
####################################################################################################################################




echo -e "512x2\nomp threads, time, accuracy " >> time_succint.csv
echo -e "\n\nCASE:512x2 [size:512x512, layers=2, iterations:10000] \n"
export OMP_NUM_THREADS=1
echo -e "num of omp threads = 1 \n"
$openmp_exe 512 2 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x2 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x2 output_512x2.out
echo -e "=========================================================\n"
for K in 2 4 8 12 16
do
	export OMP_NUM_THREADS=$K
        echo -e "num of omp threads = $K \n"
        $openmp_exe 512 2 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_512x2 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_512x2 output_512x2.out
        echo -e "=========================================================\n"
done
echo -e "\n\n\n\n\n" >> time_succint.csv

####################################################################################################################################



echo -e "64x8\nomp threads, time, accuracy " >> time_succint.csv
echo -e "\n\nCASE:64x8 [size:64x64, layers=8, iterations:10000] \n"
export OMP_NUM_THREADS=1
echo -e "num of omp threads = 1 \n"
$openmp_exe 64 8 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_64x8 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_64x8 output_64x8.out
echo -e "=========================================================\n"

for L in  2 4 8 12 16
do
     	export OMP_NUM_THREADS=$L
	echo -e "num of omp threads = $L \n"
        $openmp_exe 64 8 10000 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/power_64x8 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot3D/temp_64x8 output_64x8.out
        echo -e "=========================================================\n"
done
####################################################################################################################################
