-----SIZE=1024x1024, ITERATIONS=20000, WITH ...--------------

- - - - - - - - - - - - - - - - - - - - - - - - - - - - -

num of omp threads = 1 

results:

Start computing the transient temperature


start time:1613331645527094


end time:1613331689763272
Ending simulation
Total time: 44.23617554 seconds
=========================================================

num of omp threads = 2 

results:

Start computing the transient temperature


start time:1613331691681453


end time:1613331712137292
Ending simulation
Total time: 20.45583916 seconds
=========================================================

num of omp threads = 4 

results:

Start computing the transient temperature


start time:1613331714024911


end time:1613331724488593
Ending simulation
Total time: 10.46368217 seconds
=========================================================

num of omp threads = 8 

results:

Start computing the transient temperature


start time:1613331726349700


end time:1613331732167838
Ending simulation
Total time: 5.81813812 seconds
=========================================================

num of omp threads = 12 

results:

Start computing the transient temperature


start time:1613331733994357


end time:1613331738394524
Ending simulation
Total time: 4.40016699 seconds
=========================================================

num of omp threads = 16 

results:

Start computing the transient temperature


start time:1613331740251899


end time:1613331744203493
Ending simulation
Total time: 3.95159411 seconds
=========================================================

|||||||||||||||||||||||||||||||||||||||||||||||||||||||||

-      SIZE=512x512, ITERATIONS=20000, WITH ...             -

- - - - - - - - - - - - - - - - - - - - - - - - - - - - -

num of omp threads = 1 

results:

Start computing the transient temperature


start time:1613331745799596


end time:1613331755102632
Ending simulation
Total time: 9.30303574 seconds
=========================================================

num of omp threads = 2 

results:

Start computing the transient temperature


start time:1613331755613732


end time:1613331760366452
Ending simulation
Total time: 4.75271988 seconds
=========================================================

num of omp threads = 4 

results:

Start computing the transient temperature


start time:1613331760860165


end time:1613331763558689
Ending simulation
Total time: 2.69852400 seconds
=========================================================

num of omp threads = 8 

results:

Start computing the transient temperature


start time:1613331764047920


end time:1613331765739504
Ending simulation
Total time: 1.69158399 seconds
=========================================================

num of omp threads = 12 

results:

Start computing the transient temperature


start time:1613331766207140


end time:1613331767587994
Ending simulation
Total time: 1.38085401 seconds
=========================================================

num of omp threads = 16 

results:

Start computing the transient temperature


start time:1613331768061531


end time:1613331769485709
Ending simulation
Total time: 1.42417800 seconds
=========================================================

|||||||||||||||||||||||||||||||||||||||||||||||||||||||||

---------SIZE=64x64, ITERATIONS=20000, WITH ...--------------

- - - - - - - - - - - - - - - - - - - - - - - - - - - - -

num of omp threads = 1 

results:

Start computing the transient temperature


start time:1613331769793459


end time:1613331770071102
Ending simulation
Total time: 0.27764300 seconds
=========================================================

num of omp threads = 2 

results:

Start computing the transient temperature


start time:1613331770096410


end time:1613331770380677
Ending simulation
Total time: 0.28426701 seconds
=========================================================

num of omp threads = 4 

results:

Start computing the transient temperature


start time:1613331770403369


end time:1613331770665233
Ending simulation
Total time: 0.26186401 seconds
=========================================================

num of omp threads = 8 

results:

Start computing the transient temperature


start time:1613331770685068


end time:1613331771007807
Ending simulation
Total time: 0.32273901 seconds
=========================================================

num of omp threads = 12 

results:

Start computing the transient temperature


start time:1613331771022951


end time:1613331771458791
Ending simulation
Total time: 0.43584001 seconds
=========================================================

num of omp threads = 16 

results:

Start computing the transient temperature


start time:1613331771487611


end time:1613331771921374
Ending simulation
Total time: 0.43376300 seconds
=========================================================

