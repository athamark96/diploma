openmp_exe=./hotspot

####################################################################################################################################
echo -e "omp threads,kernel time" >> time_succint.csv
echo -e "-----SIZE=1024x1024, ITERATIONS=20000, WITH ...--------------\n"
echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
echo -e "num of omp threads = 1 \n"
echo -e "results:\n"
$openmp_exe 1024 1024 20000 1 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/temp_1024 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/power_1024 hotspot.out
echo -e "=========================================================\n"

for N in 2 4 8 12 16
do
	echo -e "num of omp threads = $N \n"
	echo -e "results:\n"
	$openmp_exe 1024 1024 20000 $N /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/temp_1024 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/power_1024 hotspot.out
	echo -e "=========================================================\n"
done
echo -e "1024x1024 DONE\n\n\n" >> time_succint.csv

####################################################################################################################################
echo -e "omp threads,kernel time" >> time_succint.csv
echo -e "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n"
echo -e "-      SIZE=512x512, ITERATIONS=20000, WITH ...             -\n"
echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
echo -e "num of omp threads = 1 \n"
echo -e "results:\n"
$openmp_exe 512 512 20000 1 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/temp_512 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/power_512 hotspot.out
echo -e "=========================================================\n"

for M in 2 4 8 12 16
do
        echo -e "num of omp threads = $M \n"
        echo -e "results:\n"
        $openmp_exe 512 512 20000 $M /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/temp_512 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/power_512 hotspot.out
        echo -e "=========================================================\n"
done
echo -e "512x512 DONE\n\n\n" >> time_succint.csv


####################################################################################################################################
echo -e "omp threads,kernel time" >> time_succint.csv
echo -e "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n"
echo -e "---------SIZE=64x64, ITERATIONS=20000, WITH ...--------------\n"
echo -e "- - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
echo -e "num of omp threads = 1 \n"
echo -e "results:\n"
$openmp_exe 64 64 20000 1 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/temp_64 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/power_64 hotspot.out
echo -e "=========================================================\n"

for K in 2 4 8 12 16
do
        echo -e "num of omp threads = $K \n"
        echo -e "results:\n"
        $openmp_exe 64 64 20000 $K /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/temp_64 /home/users/amarkou/DIPLOMA/rodinia_3.1/data/hotspot/power_64 hotspot.out
        echo -e "=========================================================\n"
done
echo -e "64x64 DONE\n\n\n" >> time_succint.csv
 
