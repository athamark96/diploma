echo -e "|||||||||||||||||SRAD VERSION 2|||||||||||||||\n"

echo -e "|||||||||||||||||2 ITERATIONS|||||||||||||||||\n"
./run_v2_2k.sh 2 
./run_v2_4k.sh 2
./run_v2_8k.sh 2
./run_v2_16k.sh 2
./run_v2_32k.sh 2


echo -e "|||||||||||||||||20 ITERATIONS|||||||||||||||||\n"
./run_v2_2k.sh 20 
./run_v2_4k.sh 20
./run_v2_8k.sh 20
./run_v2_16k.sh 20
./run_v2_32k.sh 20


