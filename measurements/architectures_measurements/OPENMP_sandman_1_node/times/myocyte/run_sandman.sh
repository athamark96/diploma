##################MODE:0

for sim0 in 100 
do
	for inst0 in 100 1000
	do
		echo -e "omp threads, time, mode=0, sim=$sim0, instances=$inst0" >> times.csv
		for thr0 in 1 2 4 8 12 16
		do 
			echo -e "\nMOD:0, SIM:$sim0, INSTANCES:$inst0,threads:$thr0 \n" 
			./myocyte.out $sim0 $inst0 0 $thr0
			echo -e "--------------------------------------\n\n"
		done
		echo -e "\n\n\n" >> times.csv
	done
done

echo -e "omp threads, time, mode=0, sim=1000, instances=100" >> times.csv
for thr1 in 1 2 4 8 12 16
do 
	echo -e "\nMOD:0, SIM:1000, INSTANCES:100,threads:$thr1 \n" 
	./myocyte.out 1000 100 0 $thr1
	echo -e "--------------------------------------\n\n"
done
echo -e "\n\n\n" >> times.csv


##################MODE:1

for sim2 in 100 
do
	for inst2 in 100 1000
	do
		echo -e "omp threads, time, mode=1, sim=$sim2, instances=$inst2" >> times.csv
		for thr2 in 1 2 4 8 12 16
		do 
			echo -e "\nMOD:1, SIM:$sim2, INSTANCES:$inst2,threads:$thr2 \n" 
			./myocyte.out $sim2 $inst2 1 $thr2
			echo -e "--------------------------------------\n\n"
		done
		echo -e "\n\n\n" >> times.csv
	done
done

echo -e "omp threads, time, mode=1, sim=1000, instances=100" >> times.csv
for thr3 in 1 2 4 8 12 16
do 
	echo -e "\nMOD:1, SIM:1000, INSTANCES:100,threads:$thr3 \n" 
	./myocyte.out 1000 100 1 $thr3
	echo -e "--------------------------------------\n\n"
done
echo -e "\n\n\n" >> times.csv

