
echo -e "\ndataset1\nomp threads,total_time" >> ./OpenMP/time_succint.csv
./run.sh 10


echo -e "\n\n\ndataset2\nomp threads,total_time" >> ./OpenMP/time_succint.csv
./run.sh 50


echo -e "\n\n\ndataset3\nomp threads,total_time" >> ./OpenMP/time_succint.csv
./run.sh 100

echo -e "\n\n\ndataset4\nomp threads,total_time" >> ./OpenMP/time_succint.csv
./run.sh 200 


