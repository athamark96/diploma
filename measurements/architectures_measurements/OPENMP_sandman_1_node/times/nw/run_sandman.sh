
echo -e "\n\n512x512\nomp threads, kernel time" >> time_succint.csv
##512x512
echo -e "==================================\nFor matrix:512x512\n"
echo -e "----------------------------------\n"
./needle 512 10 1
echo -e "----------------------------------\n"

for Z in 2 4 8 12 16
do
        ./needle 512 10 $Z
        echo -e "- - - - - - - - - - - - - - - - - - - - -\n"
done



echo -e "\n\n1024x1024\nomp threads, kernel time" >> time_succint.csv
##1024x1024
echo -e "==================================\nFor matrix:1024x1024\n"
echo -e "----------------------------------\n"
./needle 1024 10 1
echo -e "----------------------------------\n"
	
for N in 2 4 8 12 16 
do
	./needle 1024 10 $N 
	echo -e "- - - - - - - - - - - - - - - - - - - - -\n"
done 




echo -e "\n\n2048x2048\nomp threads, kernel time" >> time_succint.csv
##2048x2048
echo -e "==================================\nFor matrix:2048x2048\n"
echo -e "----------------------------------\n"
./needle 2048 10 1
echo -e "----------------------------------\n"

for K in 2 4 8 12 16
do
        ./needle 2048 10 $K
        echo -e "- - - - - - - - - - - - - - - - - - - - -\n"
done






echo -e "\n\n4096x4096\nomp threads, kernel time" >> time_succint.csv
##4096x4096
echo -e "==================================\nFor matrix:4096x4096\n"
echo -e "----------------------------------\n"
./needle 4096 10 1
echo -e "----------------------------------\n"

for L in 2 4 8 12 16
do
        ./needle 4096 10 $L
        echo -e "- - - - - - - - - - - - - - - - - - - - -\n"
done







echo -e "\n\n8192x8192\nomp threads, kernel time" >> time_succint.csv
##8192x8192
echo -e "==================================\nFor matrix:8192x8192\n"
echo -e "----------------------------------\n"
./needle 8192 10 1
echo -e "----------------------------------\n"

for M in 2 4 8 12 16
do
        ./needle 8192 10 $M
        echo -e "- - - - - - - - - - - - - - - - - - - - -\n"
done






echo -e "\n\n16384x16384\nomp threads, kernel time" >> time_succint.csv
##16384x16384
echo -e "==================================\nFor matrix:16384x16384\n"
echo -e "----------------------------------\n"
./needle 16384 10 1
echo -e "----------------------------------\n"

for T in 2 4 8 12 16
do
        ./needle 16384 10 $T
        echo -e "- - - - - - - - - - - - - - - - - - - - -\n"
done











