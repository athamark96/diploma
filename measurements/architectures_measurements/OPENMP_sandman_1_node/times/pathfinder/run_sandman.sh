echo -e "\n100kx100\nomp threads, total time" >> time_succint.csv
echo -e "[CASE1: Size = 100k x 100]\n"
echo -e "-----------------------------------------------------\n"
echo -e "OMP_NUM_THREADS=1 \n"
export OMP_NUM_THREADS=1
./pathfinder 100000 100
echo -e "-----------------------------------------------------\n"


for N in 2 4 8 12 16  
do
	echo -e "OMP_NUM_THREADS=$N\n"
	export OMP_NUM_THREADS=$N
	./pathfinder 100000 100
	echo -e "--------------------------------------------------------\n"
done 




echo -e "\n\n100kx1k\nomp threads, total time" >> time_succint.csv
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
echo -e "[CASE2: Size = 100k x 1k]\n"
echo -e "-----------------------------------------------------\n"

echo -e "OMP_NUM_THREADS=1 \n"
export OMP_NUM_THREADS=1
./pathfinder 100000 1000
echo -e "-----------------------------------------------------\n"


for M in 2 4 8 12 16
do
        echo -e "OMP_NUM_THREADS=$M\n"
        export OMP_NUM_THREADS=$M
        ./pathfinder 100000 1000
        echo -e "--------------------------------------------------------\n"
done







echo -e "\n\n100kx10k\nomp threads, total time" >> time_succint.csv
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
echo -e "[CASE3: Size = 100k x 10k]\n"
echo -e "-----------------------------------------------------\n"

echo -e "OMP_NUM_THREADS=1 \n"
export OMP_NUM_THREADS=1
./pathfinder 100000 10000
echo -e "-----------------------------------------------------\n"


for P in 2 4 8 12 16
do
        echo -e "OMP_NUM_THREADS=$P\n"
        export OMP_NUM_THREADS=$P
        ./pathfinder 100000 10000
        echo -e "--------------------------------------------------------\n"
done








echo -e "\n\n100kx20k\nomp threads, total time" >> time_succint.csv
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
echo -e "[CASE4: Size = 100k x 20k]\n"
echo -e "-----------------------------------------------------\n"
echo -e "OMP_NUM_THREADS=1 \n"
export OMP_NUM_THREADS=1
./pathfinder 100000 20000
echo -e "-----------------------------------------------------\n"


for T in 2 4 8 12 16
do
        echo -e "OMP_NUM_THREADS=$T\n"
        export OMP_NUM_THREADS=$T
        ./pathfinder 100000 20000
        echo -e "--------------------------------------------------------\n"
done





exit





echo -e "omp threads, total time" >> time_succint.csv
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
echo -e "[CASE5: Size = 100k x 100k]\n"
echo -e "-----------------------------------------------------\n"
echo -e "OMP_NUM_THREADS set to DEFAULT:\n"
./pathfinder 100000 100000
echo -e "-----------------------------------------------------\n"

echo -e "OMP_NUM_THREADS=1 \n"
export OMP_NUM_THREADS=1
./pathfinder 100000 100000
echo -e "-----------------------------------------------------\n"


for L in 2 4 8 12 16
do
        echo -e "OMP_NUM_THREADS=$L\n"
        export OMP_NUM_THREADS=$L
        ./pathfinder 100000 100000
        echo -e "--------------------------------------------------------\n"
done
mv time_succint.csv time_succint_100kx100k.csv

