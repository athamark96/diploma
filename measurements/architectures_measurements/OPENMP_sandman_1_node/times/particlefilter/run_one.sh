
echo -e "-----------------------------------------------------\n"
echo -e "	ATTEMPT with number of particles= $1 ,z=2 \n\n "
echo -e " - - - - - - - - - - - - - - - - - - - - - - - - - - \n"

echo -e "\n\n$1\nnthreads, particlefilter, total_time" >> pf.csv

echo -e "OMP_NUM_THREADS=1 \n"
export OMP_NUM_THREADS=1
./particle_filter -x 128 -y 128 -z 2 -np $1
echo -e "-----------------------------------------------------\n"


for N in 2 4 8 12 16  
do
	echo -e "OMP_NUM_THREADS=$N\n"
	export OMP_NUM_THREADS=$N
	./particle_filter -x 128 -y 128 -z 2 -np $1
	echo -e "--------------------------------------------------------\n"
done 

