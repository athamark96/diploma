echo -e "\n100\nomp threads,setup var time,read commands time,read image time,resize time,mem alloc time,extract time,kernel time,compress time,save time,free time,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./srad 100 0.5 502 458 1
echo -e "--------------------------\n"

for N1 in 2 4 8 12 16
do

	echo -e "OMP_NUM_THREADS=$N1 \n"
	./srad 100 0.5 502 458 $N1
	echo -e "--------------------------\n"

done 
############################################################


echo -e "\n\n1K\nomp threads,setup var time,read commands time,read image time,resize time,mem alloc time,extract time,kernel time,compress time,save time,free time,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./srad 1000 0.5 502 458 1
echo -e "--------------------------\n"

for N2 in 2 4 8 12 16
do

	echo -e "OMP_NUM_THREADS=$N2 \n"
	./srad 1000 0.5 502 458 $N2
	echo -e "--------------------------\n"

done 
##########################################################


echo -e "\n\n10K\nomp threads,setup var time,read commands time,read image time,resize time,mem alloc time,extract time,kernel time,compress time,save time,free time,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./srad 10000 0.5 502 458 1
echo -e "--------------------------\n"

for N3 in 2 4 8 12 16
do

	echo -e "OMP_NUM_THREADS=$N3 \n"
	./srad 10000 0.5 502 458 $N3
	echo -e "--------------------------\n"

done 
##########################################################


echo -e "\n\n100K\nomp threads,setup var time,read commands time,read image time,resize time,mem alloc time,extract time,kernel time,compress time,save time,free time,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./srad 100000 0.5 502 458 1
echo -e "--------------------------\n"

for N4 in 2 4 8 12 16
do

	echo -e "OMP_NUM_THREADS=$N4 \n"
	./srad 100000 0.5 502 458 $N4
	echo -e "--------------------------\n"

done 
