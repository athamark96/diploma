##CASE 1: Input 100:
echo -e "100\nomp threads,time,kernel_time" >> time_succint.csv
	echo -e "==================================\nFor input file:100\n"
	echo -e "----------------------------------\n"
##	echo -e "Base Case:\n kmeans_serial execution for this input\n\n"
	
##	./kmeans_serial/kmeans -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/100
	
	echo -e "----------------------------------\n"

	## Estimate the overhead of spawning a single omp thread:
	./kmeans_openmp/kmeans -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/100
	echo -e "- - - - - - - - - - - - - - - - - - -\n"
	
	for N in 2 4 8 12 16 
	do
		./kmeans_openmp/kmeans -n $N -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/100
		echo -e "- - - - - - - - - - - - - - - - - - - - -\n"
	done 


##CASE 2: Input 204800.txt
echo -e "\n\n\n204800.txt\nomp threads,time,kernel_time" >> time_succint.csv
	echo -e "==================================\nFor input file:204800.txt\n"
	echo -e "----------------------------------\n"
##	echo -e "Base Case:\n kmeans_serial execution for this input\n\n"

##	./kmeans_serial/kmeans -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/204800.txt
	
	echo -e "----------------------------------\n"

        ## Estimate the overhead of spawning a single omp thread:
        ./kmeans_openmp/kmeans -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/204800.txt
        echo -e "- - - - - - - - - - - - - - - - - - -\n"

	for K in 2 4 8 12 16
	do
        	./kmeans_openmp/kmeans -n $K -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/204800.txt
        	echo -e "- - - - - - - - - - - - - - - - - - - - -\n"
	done


##CASE 3: Input 819200.txt
echo -e "\n\n\n819200.txt\nomp threads,time,kernel_time" >> time_succint.csv
        echo -e "==================================\nFor input file:819200.txt\n"
        echo -e "----------------------------------\n"
##        echo -e "Base Case:\n kmeans_serial execution for this input\n\n"
        
##        ./kmeans_serial/kmeans -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/819200.txt

        echo -e "----------------------------------\n"

        ## Estimate the overhead of spawning a single omp thread:
        ./kmeans_openmp/kmeans -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/819200.txt
        echo -e "- - - - - - - - - - - - - - - - - - -\n"

        for L in 2 4 8 12 16
        do
                ./kmeans_openmp/kmeans -n $L -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/819200.txt
                echo -e "- - - - - - - - - - - - - - - - - - - - -\n"
        done

##CASE 4: Input kdd_cup
echo -e "\n\n\nkdd_cup\nomp threads,time,kernel_time" >> time_succint.csv
        echo -e "==================================\nFor input file:kdd_cup\n"
        echo -e "----------------------------------\n"
##        echo -e "Base Case:\n kmeans_serial execution for this input\n\n"
        
##        ./kmeans_serial/kmeans -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/kdd_cup

        echo -e "----------------------------------\n"

        ## Estimate the overhead of spawning a single omp thread:
        ./kmeans_openmp/kmeans -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/kdd_cup
        echo -e "- - - - - - - - - - - - - - - - - - -\n"

        for M in 2 4 8 12 16
        do
                ./kmeans_openmp/kmeans -n $M -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/kmeans/kdd_cup
                echo -e "- - - - - - - - - - - - - - - - - - - - -\n"
        done

