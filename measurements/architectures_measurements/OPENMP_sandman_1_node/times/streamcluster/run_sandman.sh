
echo -e "\n\ndata1\nomp threads,pgain,pgain_dist,pgain_init,pselect,pspeedy,pshuffle,local search,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./sc_omp 10 20 256 65536 65536 1000 none output.txt 1
echo -e "--------------------------\n"

for N in 2 4 8 12 16
do

	echo -e "OMP_NUM_THREADS=$N \n"
	./sc_omp 10 20 256 65536 65536 1000 none output.txt $N
	echo -e "--------------------------\n"

done 
rm output.txt 
#############################################
echo -e "\n\ndata2\nomp threads,pgain,pgain_dist,pgain_init,pselect,pspeedy,pshuffle,local search,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./sc_omp 10 20 256 131072 65536 1000 none output.txt 1
echo -e "--------------------------\n"

for N2 in 2 4 8 12 16 
do

	echo -e "OMP_NUM_THREADS=$N2 \n"
	./sc_omp 10 20 256 131072 65536 1000 none output.txt $N2
	echo -e "--------------------------\n"

done 
rm output.txt 
##############################################
echo -e "\n\ndata3\nomp threads,pgain,pgain_dist,pgain_init,pselect,pspeedy,pshuffle,local search,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./sc_omp 10 20 256 196608 65536 1000 none output.txt 1
echo -e "--------------------------\n"

for N3 in 2 4 8 12 16 
do

	echo -e "OMP_NUM_THREADS=$N3 \n"
	./sc_omp 10 20 256 196608 65536 1000 none output.txt $N3
	echo -e "--------------------------\n"

done 
rm output.txt 
##############################################
echo -e "\n\ndata4\nomp threads,pgain,pgain_dist,pgain_init,pselect,pspeedy,pshuffle,local search,total time" >> time_succint.csv
echo -e "OMP_NUM_THREADS=1 \n"
./sc_omp 10 20 256 262144 65536 1000 none output.txt 1
echo -e "--------------------------\n"

for N4 in 2 4 8 12 16 
do

	echo -e "OMP_NUM_THREADS=$N4 \n"
	./sc_omp 10 20 256 262144 65536 1000 none output.txt $N4
	echo -e "--------------------------\n"

done 
rm output.txt 
