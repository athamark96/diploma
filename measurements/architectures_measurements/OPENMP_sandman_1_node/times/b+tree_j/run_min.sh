echo -e "MIN\n" >> times_succint_k.csv
echo -e "MIN\n" >> times_succint_j.csv
echo -e "omp threads,set device time,set device percentage,kernel time,kernel percentage,total time" >> times_succint_k.csv
echo -e "omp threads,set device time,set device percentage,kernel time,kernel percentage,total time" >> times_succint_j.csv

echo -e "---------------------------------------------------------\n"
for N in 1 2 4 8 12 16 
do
	echo -e "OMP_NUM_THREADS= $N : \n"
	./b+tree.out cores $N file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/mil.txt command /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/command.txt
	echo -e "---------------------------------------------------------\n"
done 

echo -e "\n\n\n" >> times_succint_k.csv
echo -e "\n\n\n" >> times_succint_j.csv

