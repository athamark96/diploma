
echo -e "\nPROBLEM 64\n" >> time_succint.csv

echo -e "=====================================\n"
echo -e "PROBLEM SIZE=64\n"

echo -e "--------------------------------\n"
echo -e "    with OMP_NUM_THREADS= 1 \n \n"
./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat

for N in 2 4 8 12 16 
do
        echo -e "--------------------------------\n"
        echo -e "    with OMP_NUM_THREADS= $N \n \n"
        ./omp/lud_omp -n $N -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
done 


echo -e "\n\nPROBLEM 256\n" >> time_succint.csv
echo -e "====================================\n"
echo -e "PROBLEM SIZE=256\n"

echo -e "--------------------------------\n"
echo -e "    with OMP_NUM_THREADS= 1 \n \n"
./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat

for T in 2 4 8 12 16
do
        echo -e "--------------------------------\n"
        echo -e "    with OMP_NUM_THREADS= $T \n \n"
        ./omp/lud_omp -n $T -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
done


echo -e "\n\nPROBLEM 512\n" >> time_succint.csv
echo -e "====================================\n"
echo -e "PROBLEM SIZE=512\n"

echo -e "--------------------------------\n"
echo -e "    with OMP_NUM_THREADS= 1 \n \n"
./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat

for K in 2 4 8 12 16
do
        echo -e "--------------------------------\n"
        echo -e "    with OMP_NUM_THREADS= $K \n \n"
        ./omp/lud_omp -n $K -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
done

echo -e "\n\nPROBLEM 2048\n" >> time_succint.csv
echo -e "====================================\n"
echo -e "PROBLEM SIZE=2048\n"

echo -e "--------------------------------\n"
echo -e "    with OMP_NUM_THREADS= 1 \n \n"
./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat

for L in 2 4 8 12 16
do
        echo -e "--------------------------------\n"
        echo -e "    with OMP_NUM_THREADS= $L \n \n"
        ./omp/lud_omp -n $L -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
done

