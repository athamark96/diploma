
echo -e "======================================\n"
echo -e "test_1: 5 neighbours\n"
echo -e "\n5_neighbors\nompThreads, time" >> times_succint.csv
for i in 1 2 4 8 12 16
do
	export OMP_NUM_THREADS=$i
	./nn filelist_4 5 30 90 
done




echo -e "\n\n10neighbors\nompThreads, time" >> times_succint.csv
echo -e "======================================\n"
echo -e "test_2: 10 neighbours\n"
for j in 1 2 4 8 12 16
do      
        export OMP_NUM_THREADS=$j
	./nn filelist_4 10 30 90
done



echo -e "\n\n100 neighbors\nompThreads, time" >> times_succint.csv
echo -e "======================================\n"
echo -e "test_3: 100 neighbours\n"
for k in 1 2 4 8 12 16
do      
        export OMP_NUM_THREADS=$k
	./nn filelist_4 100 30 90
done



echo -e "\n\n1000 neighbors\nompThreads, time" >> times_succint.csv
echo -e "======================================\n"
echo -e "test_4: 1000 neighbours\n"
for l in 1 2 4 8 12 16
do      
        export OMP_NUM_THREADS=$l
	./nn filelist_4 1000 30 90
done



echo -e "\n\n10K neighbors\nompThreads, time" >> times_succint.csv
echo -e "======================================\n"
echo -e "test_5: 10000 neighbours\n"
for m in 1 2 4 8 12 16
do
        export OMP_NUM_THREADS=$m
        ./nn filelist_4 10000 30 90 
done

