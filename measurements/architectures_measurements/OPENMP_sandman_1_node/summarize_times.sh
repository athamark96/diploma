#!/bin/bash

cd ~/DIPLOMA_2019_2020/diploma/measurements/architectures_measurements/sandman_1_node/times/


for app in *
do
	cd $app
	echo -e "\n\n$app\n" 
	cat time*
	echo -e "\n-,-,-,-,-,-,-,-,-,-,-,-,-\n"	
	cd ..
done	
