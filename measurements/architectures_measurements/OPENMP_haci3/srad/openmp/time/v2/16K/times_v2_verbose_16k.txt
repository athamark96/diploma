OMP_NUM_THREADS=1 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 10.4893488884 (sec)
Computation Done

real	0m32.319s
user	0m28.836s
sys	0m3.492s
--------------------------

OMP_NUM_THREADS=2 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 5.3405439854 (sec)
Computation Done

real	0m21.388s
user	0m28.956s
sys	0m3.540s
--------------------------

OMP_NUM_THREADS=4 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 2.6763818264 (sec)
Computation Done

real	0m15.897s
user	0m29.152s
sys	0m3.704s
--------------------------

OMP_NUM_THREADS=8 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 1.3531138897 (sec)
Computation Done

real	0m13.110s
user	0m29.396s
sys	0m3.860s
--------------------------

OMP_NUM_THREADS=14 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 0.7769818306 (sec)
Computation Done

real	0m11.897s
user	0m29.428s
sys	0m4.256s
--------------------------

OMP_NUM_THREADS=28 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 0.4308979511 (sec)
Computation Done

real	0m11.270s
user	0m31.948s
sys	0m8.324s
--------------------------

OMP_NUM_THREADS=42 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 0.4458491802 (sec)
Computation Done

real	0m11.350s
user	0m42.184s
sys	0m10.708s
--------------------------

OMP_NUM_THREADS=56 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 0.3888170719 (sec)
Computation Done

real	0m11.277s
user	0m51.464s
sys	0m14.948s
--------------------------

