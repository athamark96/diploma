OMP_NUM_THREADS=1 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 50.4204130173 (sec)
Computation Done

real	0m52.866s
user	0m52.104s
sys	0m0.776s
--------------------------

OMP_NUM_THREADS=2 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 26.2127571106 (sec)
Computation Done

real	0m28.689s
user	0m53.708s
sys	0m0.752s
--------------------------

OMP_NUM_THREADS=4 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 13.1289749146 (sec)
Computation Done

real	0m15.628s
user	0m53.748s
sys	0m0.884s
--------------------------

OMP_NUM_THREADS=8 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 6.7969121933 (sec)
Computation Done

real	0m9.292s
user	0m54.160s
sys	0m0.840s
--------------------------

OMP_NUM_THREADS=14 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 3.8647680283 (sec)
Computation Done

real	0m6.361s
user	0m55.040s
sys	0m0.992s
--------------------------

OMP_NUM_THREADS=28 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 2.2072920799 (sec)
Computation Done

real	0m4.709s
user	1m0.696s
sys	0m2.712s
--------------------------

OMP_NUM_THREADS=42 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 2.5896019936 (sec)
Computation Done

real	0m5.078s
user	1m30.156s
sys	0m3.124s
--------------------------

OMP_NUM_THREADS=56 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 2.0290899277 (sec)
Computation Done

real	0m4.515s
user	1m49.224s
sys	0m2.984s
--------------------------

