OMP_NUM_THREADS=1 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 3.1715371609 (sec)
Computation Done

real	0m3.335s
user	0m3.288s
sys	0m0.044s
--------------------------

OMP_NUM_THREADS=2 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 1.6246550083 (sec)
Computation Done

real	0m1.786s
user	0m3.360s
sys	0m0.052s
--------------------------

OMP_NUM_THREADS=4 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 0.8121290207 (sec)
Computation Done

real	0m0.971s
user	0m3.340s
sys	0m0.072s
--------------------------

OMP_NUM_THREADS=8 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 0.4090681076 (sec)
Computation Done

real	0m0.569s
user	0m3.396s
sys	0m0.048s
--------------------------

OMP_NUM_THREADS=14 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 0.2418990135 (sec)
Computation Done

real	0m0.402s
user	0m3.520s
sys	0m0.052s
--------------------------

OMP_NUM_THREADS=28 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 0.1404690742 (sec)
Computation Done

real	0m0.300s
user	0m3.888s
sys	0m0.260s
--------------------------

OMP_NUM_THREADS=42 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 0.1619870663 (sec)
Computation Done

real	0m0.321s
user	0m6.492s
sys	0m0.256s
--------------------------

OMP_NUM_THREADS=56 

Randomizing the input matrix
Start the SRAD main loop
Computation took: 0.2098300457 (sec)
Computation Done

real	0m0.371s
user	0m10.304s
sys	0m0.152s
--------------------------

