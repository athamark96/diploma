============================================================================================

PROBLEM SIZE=512

----------------

with OMP_NUM_THREADS= 1 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 88815470209 
Total Cycles:       34805393886 
IPC:                2.551773  



-----ompnthreads=1-----


Time consumed(ms): 16238.9110000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 2 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 89196013622 
Total Cycles:       19689519090 
IPC:                2.265063  



-----ompnthreads=2-----


Time consumed(ms): 11449.0860000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 4 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 90724066175 
Total Cycles:       11692313768 
IPC:                1.939823  



-----ompnthreads=4-----


Time consumed(ms): 9053.3580000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 6 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 92730798834 
Total Cycles:       9556263262 
IPC:                1.617278  



-----ompnthreads=6-----


Time consumed(ms): 9282.7060000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 8 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 96153648857 
Total Cycles:       9277563597 
IPC:                1.295513  



-----ompnthreads=8-----


Time consumed(ms): 10701.5660000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 10 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 100566568542 
Total Cycles:       10012739935 
IPC:                1.004386  



-----ompnthreads=10-----


Time consumed(ms): 11947.1200000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 12 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 105774347873 
Total Cycles:       11428858295 
IPC:                0.771252  



-----ompnthreads=12-----


Time consumed(ms): 14366.2400000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 14 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 112632975074 
Total Cycles:       13022032754 
IPC:                0.617815  



-----ompnthreads=14-----


Time consumed(ms): 16938.0160000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 16 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 121876293950 
Total Cycles:       15298883966 
IPC:                0.497897  



-----ompnthreads=16-----


Time consumed(ms): 19787.8650000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 18 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 132958309368 
Total Cycles:       16790774438 
IPC:                0.439919  



-----ompnthreads=18-----


Time consumed(ms): 22377.2830000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 20 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 140099785379 
Total Cycles:       17880695514 
IPC:                0.391763  



-----ompnthreads=20-----


Time consumed(ms): 24147.8160000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 22 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 154842631063 
Total Cycles:       19042042008 
IPC:                0.369619  



-----ompnthreads=22-----


Time consumed(ms): 26993.7150000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 24 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 171902983175 
Total Cycles:       21998402452 
IPC:                0.325597  



-----ompnthreads=24-----


Time consumed(ms): 30675.8220000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 26 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 187730308695 
Total Cycles:       23389240781 
IPC:                0.308706  



-----ompnthreads=26-----


Time consumed(ms): 34099.7790000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 28 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 205235525280 
Total Cycles:       25663390028 
IPC:                0.285615  



-----ompnthreads=28-----


Time consumed(ms): 36759.1960000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 30 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 217679517521 
Total Cycles:       26373781142 
IPC:                0.275121  



-----ompnthreads=30-----


Time consumed(ms): 41615.7950000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 32 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 236297550538 
Total Cycles:       29197445350 
IPC:                0.252909  



-----ompnthreads=32-----


Time consumed(ms): 51180.6140000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 34 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 249981900281 
Total Cycles:       30658841494 
IPC:                0.239814  



-----ompnthreads=34-----


Time consumed(ms): 56443.8740000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 36 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 256250924707 
Total Cycles:       31422609149 
IPC:                0.226527  



-----ompnthreads=36-----


Time consumed(ms): 60016.2090000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 38 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 259976227859 
Total Cycles:       31013225004 
IPC:                0.220599  



-----ompnthreads=38-----


Time consumed(ms): 64957.6430000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 40 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 270913847012 
Total Cycles:       32683688057 
IPC:                0.207224  



-----ompnthreads=40-----


Time consumed(ms): 73426.9310000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 42 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 286917907114 
Total Cycles:       34587840999 
IPC:                0.197508  



-----ompnthreads=42-----


Time consumed(ms): 76062.9360000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 44 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 306527667642 
Total Cycles:       36418118458 
IPC:                0.191293  



-----ompnthreads=44-----


Time consumed(ms): 81314.7980000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 46 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 315116500330 
Total Cycles:       36720636976 
IPC:                0.186553  



-----ompnthreads=46-----


Time consumed(ms): 87786.9720000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 48 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 322948200657 
Total Cycles:       37423822084 
IPC:                0.179781  



-----ompnthreads=48-----


Time consumed(ms): 92089.4630000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 50 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 325007591925 
Total Cycles:       35750171457 
IPC:                0.181822  



-----ompnthreads=50-----


Time consumed(ms): 90722.1630000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 52 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 369781791000 
Total Cycles:       41167682022 
IPC:                0.172737  



-----ompnthreads=52-----


Time consumed(ms): 101176.4220000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 54 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 368855315816 
Total Cycles:       40810219973 
IPC:                0.167376  



-----ompnthreads=54-----


Time consumed(ms): 105185.5320000000
--------------------------------------------------------------------------------------------

with OMP_NUM_THREADS= 56 
 

Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
Total Instructions: 378541263282 
Total Cycles:       37499466752 
IPC:                0.180260  



-----ompnthreads=56-----


Time consumed(ms): 108239.1870000000
