for 30 layers in total:
------------------------

number of threads = 1  

num of threads: 1
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 

Time total :	32.249095 sec
Time for parallel region:	32.247051 sec
========================

------------------------

number of threads = 2  

num of threads: 2
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 

Time total :	16.525491 sec
Time for parallel region:	16.522996 sec
========================

------------------------

number of threads = 4  

num of threads: 4
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 

Time total :	8.364017 sec
Time for parallel region:	8.361092 sec
========================

------------------------

number of threads = 8  

num of threads: 8
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 

Time total :	4.592018 sec
Time for parallel region:	4.589722 sec
========================

------------------------

number of threads = 14  

num of threads: 14
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 

Time total :	2.706262 sec
Time for parallel region:	2.703859 sec
========================

------------------------

number of threads = 28  

num of threads: 28
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 

Time total :	1.456544 sec
Time for parallel region:	1.453863 sec
========================

------------------------

number of threads = 42  

num of threads: 42
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 

Time total :	1.654067 sec
Time for parallel region:	1.650431 sec
========================

------------------------

number of threads = 56  

num of threads: 56
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 

Time total :	1.159063 sec
Time for parallel region:	1.155670 sec
========================

