for 40 layers in total:
------------------------

number of threads = 1  

num of threads: 1
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 

Time total :	43.361080 sec
Time for parallel region:	43.359118 sec
========================

------------------------

number of threads = 2  

num of threads: 2
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 

Time total :	22.230272 sec
Time for parallel region:	22.219539 sec
========================

------------------------

number of threads = 4  

num of threads: 4
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 

Time total :	11.242497 sec
Time for parallel region:	11.240197 sec
========================

------------------------

number of threads = 8  

num of threads: 8
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 

Time total :	6.169166 sec
Time for parallel region:	6.166917 sec
========================

------------------------

number of threads = 14  

num of threads: 14
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 

Time total :	3.637968 sec
Time for parallel region:	3.635431 sec
========================

------------------------

number of threads = 28  

num of threads: 28
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 

Time total :	1.988197 sec
Time for parallel region:	1.985339 sec
========================

------------------------

number of threads = 42  

num of threads: 42
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 

Time total :	2.332809 sec
Time for parallel region:	2.329637 sec
========================

------------------------

number of threads = 56  

num of threads: 56
frame progress: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 

Time total :	1.588868 sec
Time for parallel region:	1.585384 sec
========================

