openmp_exe=/home/users/amarkou/DIPLOMA/rodinia_3.1/openmp/haci3/lavaMD/lavaMD

echo -e "omp threads,var time, input arg time, dim cpu, sys mem/alo ,kernel,sys mem free,total time" >> time_succint.csv

echo -e "-------------------------------------------\n"
echo -e "omp_num_threads=1\n"
$openmp_exe -cores 1 -boxes1d 10
echo -e "===========================================\n"


for N in {2..56..2} 
do
	echo -e "-------------------------------------------\n"
	echo -e "omp_num_threads=$N\n"
	$openmp_exe -cores $N -boxes1d 10
	echo -e "===========================================\n"
done 
