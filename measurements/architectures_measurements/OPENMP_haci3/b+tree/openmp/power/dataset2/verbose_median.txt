--------------------------------------------------------

OMP_NUM_THREADS=1:

Input File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/mil.txt 
Command File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/command_intermediate.txt 
Command Buffer: 
j 500000 500000
k 32767

�Getting input from file cores...
Transforming data to a GPU suitable structure...
Tree transformation took 0.186077
Waiting for command
> 
******command: j count=500000, rSize=500000 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000006000000 s,  0.000412119611 % : MCPU: SET DEVICE
 1.455881953239 s, 99.999588012695 % : CPU/MCPU: KERNEL
Total time:
1.455888032913 s
> > > > > > > > > > > > > > > > 
 ******command: k count=32767 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000001000000 s,  0.001806586748 % : MCPU: SET DEVICE
 0.055351998657 s, 99.998191833496 % : CPU/MCPU: KERNEL
Total time:
0.055353000760 s
> > > > > > > > > > > > PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.001489133000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.001249336000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000824641000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000700104000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 90.889465332000
PACKAGE_ENERGY:PACKAGE1 (J) 76.253417968000
DRAM_ENERGY:PACKAGE0    (J) 12.583023071000
DRAM_ENERGY:PACKAGE1    (J) 10.682739257000

Total time:  1.938403010368 (s)
Power (W) 98.229648122460
==================================================================


---------------------------------------------------------

OMP_NUM_THREADS= 2 : 

Input File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/mil.txt 
Command File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/command_intermediate.txt 
Command Buffer: 
j 500000 500000
k 32767

�Getting input from file cores...
Transforming data to a GPU suitable structure...
Tree transformation took 0.186835
Waiting for command
> 
******command: j count=500000, rSize=500000 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000006000000 s,  0.000758274633 % : MCPU: SET DEVICE
 0.791263997555 s, 99.999244689941 % : CPU/MCPU: KERNEL
Total time:
0.791270017624 s
> > > > > > > > > > > > > > > > 
 ******command: k count=32767 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000001000000 s,  0.003110323101 % : MCPU: SET DEVICE
 0.032150000334 s, 99.996887207031 % : CPU/MCPU: KERNEL
Total time:
0.032150998712 s
> > > > > > > > > > > > PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000824041000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000804619000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000512204000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000469461000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 50.295471191000
PACKAGE_ENERGY:PACKAGE1 (J) 49.110046386000
DRAM_ENERGY:PACKAGE0    (J) 7.815612792000
DRAM_ENERGY:PACKAGE1    (J) 7.163406372000

Total time:  1.201104044914 (s)
Power (W) 95.232829516586
==================================================================


----------------------------------------------------------

OMP_NUM_THREADS= 4 : 

Input File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/mil.txt 
Command File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/command_intermediate.txt 
Command Buffer: 
j 500000 500000
k 32767

�Getting input from file cores...
Transforming data to a GPU suitable structure...
Tree transformation took 0.184024
Waiting for command
> 
******command: j count=500000, rSize=500000 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000006000000 s,  0.001695322338 % : MCPU: SET DEVICE
 0.353908985853 s, 99.998306274414 % : CPU/MCPU: KERNEL
Total time:
0.353915005922 s
> > > > > > > > > > > > > > > > 
 ******command: k count=32767 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000001000000 s,  0.006114338059 % : MCPU: SET DEVICE
 0.016354000196 s, 99.993881225586 % : CPU/MCPU: KERNEL
Total time:
0.016355000436 s
> > > > > > > > > > > > PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000653775000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000673037000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000330573000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000299435000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 39.903259277000
PACKAGE_ENERGY:PACKAGE1 (J) 41.078918457000
DRAM_ENERGY:PACKAGE0    (J) 5.044143676000
DRAM_ENERGY:PACKAGE1    (J) 4.569015502000

Total time:  0.780332982540 (s)
Power (W) 116.098305388932
==================================================================


----------------------------------------------------------

OMP_NUM_THREADS= 8 : 

Input File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/mil.txt 
Command File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/command_intermediate.txt 
Command Buffer: 
j 500000 500000
k 32767

�Getting input from file cores...
Transforming data to a GPU suitable structure...
Tree transformation took 0.187197
Waiting for command
> 
******command: j count=500000, rSize=500000 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000006000000 s,  0.003044310026 % : MCPU: SET DEVICE
 0.197082996368 s, 99.996955871582 % : CPU/MCPU: KERNEL
Total time:
0.197089001536 s
> > > > > > > > > > > > > > > > 
 ******command: k count=32767 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000001000000 s,  0.008388557471 % : MCPU: SET DEVICE
 0.011920000426 s, 99.991615295410 % : CPU/MCPU: KERNEL
Total time:
0.011920999736 s
> > > > > > > > > > > > PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000393475000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000426799000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000264765000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000233757000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 24.041015625000
PACKAGE_ENERGY:PACKAGE1 (J) 26.049743652000
DRAM_ENERGY:PACKAGE0    (J) 4.046768188000
DRAM_ENERGY:PACKAGE1    (J) 3.566848754000

Total time:  0.625102996826 (s)
Power (W) 92.311789436272
==================================================================


----------------------------------------------------------

OMP_NUM_THREADS= 14 : 

Input File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/mil.txt 
Command File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/command_intermediate.txt 
Command Buffer: 
j 500000 500000
k 32767

�Getting input from file cores...
Transforming data to a GPU suitable structure...
Tree transformation took 0.187967
Waiting for command
> 
******command: j count=500000, rSize=500000 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000006000000 s,  0.005562869832 % : MCPU: SET DEVICE
 0.107851997018 s, 99.994438171387 % : CPU/MCPU: KERNEL
Total time:
0.107858002186 s
> > > > > > > > > > > > > > > > 
 ******command: k count=32767 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000001000000 s,  0.020721092820 % : MCPU: SET DEVICE
 0.004825000186 s, 99.979278564453 % : CPU/MCPU: KERNEL
Total time:
0.004825999960 s
> > > > > > > > > > > > PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000504190000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000508575000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000240352000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000213420000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 30.773315429000
PACKAGE_ENERGY:PACKAGE1 (J) 31.040954589000
DRAM_ENERGY:PACKAGE0    (J) 3.667480468000
DRAM_ENERGY:PACKAGE1    (J) 3.256530761000

Total time:  0.575730025768 (s)
Power (W) 119.393254078198
==================================================================


----------------------------------------------------------

OMP_NUM_THREADS= 28 : 

Input File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/mil.txt 
Command File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/command_intermediate.txt 
Command Buffer: 
j 500000 500000
k 32767

�Getting input from file cores...
Transforming data to a GPU suitable structure...
Tree transformation took 0.184526
Waiting for command
> 
******command: j count=500000, rSize=500000 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000005000000 s,  0.008982305415 % : MCPU: SET DEVICE
 0.055659998208 s, 99.991020202637 % : CPU/MCPU: KERNEL
Total time:
0.055665001273 s
> > > > > > > > > > > > > > > > 
 ******command: k count=32767 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000000000000 s,  0.000000000000 % : MCPU: SET DEVICE
 0.003642000025 s, 100.000000000000 % : CPU/MCPU: KERNEL
Total time:
0.003642000025 s
> > > > > > > > > > > > PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000405604000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000353825000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000208564000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000181478000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 24.756103515000
PACKAGE_ENERGY:PACKAGE1 (J) 21.595764160000
DRAM_ENERGY:PACKAGE0    (J) 3.182434082000
DRAM_ENERGY:PACKAGE1    (J) 2.769134521000

Total time:  0.493784993887 (s)
Power (W) 105.923502993238
==================================================================


----------------------------------------------------------

OMP_NUM_THREADS= 42 : 

Input File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/mil.txt 
Command File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/command_intermediate.txt 
Command Buffer: 
j 500000 500000
k 32767

�Getting input from file cores...
Transforming data to a GPU suitable structure...
Tree transformation took 0.186465
Waiting for command
> 
******command: j count=500000, rSize=500000 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000005000000 s,  0.008223278448 % : MCPU: SET DEVICE
 0.060798000544 s, 99.991775512695 % : CPU/MCPU: KERNEL
Total time:
0.060802999884 s
> > > > > > > > > > > > > > > > 
 ******command: k count=32767 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000001000000 s,  0.040633890778 % : MCPU: SET DEVICE
 0.002460000105 s, 99.959365844727 % : CPU/MCPU: KERNEL
Total time:
0.002461000113 s
> > > > > > > > > > > > PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000404575000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000429712000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000205273000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000180335000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 24.693298339000
PACKAGE_ENERGY:PACKAGE1 (J) 26.227539062000
DRAM_ENERGY:PACKAGE0    (J) 3.132217407000
DRAM_ENERGY:PACKAGE1    (J) 2.751693725000

Total time:  0.489475995302 (s)
Power (W) 116.052164106493
==================================================================


----------------------------------------------------------

OMP_NUM_THREADS= 56 : 

Input File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/mil.txt 
Command File: /home/users/amarkou/DIPLOMA/rodinia_3.1/data/b+tree/command_intermediate.txt 
Command Buffer: 
j 500000 500000
k 32767

�Getting input from file cores...
Transforming data to a GPU suitable structure...
Tree transformation took 0.188410
Waiting for command
> 
******command: j count=500000, rSize=500000 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000006000000 s,  0.008563476615 % : MCPU: SET DEVICE
 0.070059001446 s, 99.991432189941 % : CPU/MCPU: KERNEL
Total time:
0.070064999163 s
> > > > > > > > > > > > > > > > 
 ******command: k count=32767 
Time spent in different stages of CPU/MCPU KERNEL:
 0.000001000000 s,  0.017752530053 % : MCPU: SET DEVICE
 0.005632000044 s, 99.982246398926 % : CPU/MCPU: KERNEL
Total time:
0.005632999819 s
> > > > > > > > > > > > PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000326625000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000304670000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000183676000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000160657000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 19.935607910000
PACKAGE_ENERGY:PACKAGE1 (J) 18.595581054000
DRAM_ENERGY:PACKAGE0    (J) 2.802673339000
DRAM_ENERGY:PACKAGE1    (J) 2.451431274000

Total time:  0.431023001671 (s)
Power (W) 101.584586918259
==================================================================


----------------------------------------------------------

