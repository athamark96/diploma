CASE:512x8 [size:512x512, layers=8, iterations:10000]

num of omp threads = 1 

1 threads running
CPU execution:134.882034 (sec)
Time: 164.521 (s)
Accuracy: 4.856862e-05
=========================================================

num of omp threads = 2 

2 threads running
Time: 82.703 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 3 

3 threads running
Time: 62.362 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 4 

4 threads running
Time: 41.585 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 5 

5 threads running
Time: 41.495 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 6 

6 threads running
Time: 41.145 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 7 

7 threads running
Time: 41.149 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 8 

8 threads running
Time: 21.260 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 9 

9 threads running
Time: 21.543 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 10 

10 threads running
Time: 21.534 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 11 

11 threads running
Time: 21.436 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 12 

12 threads running
Time: 21.671 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 13 

13 threads running
Time: 21.479 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 14 

14 threads running
Time: 21.412 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 15 

15 threads running
Time: 21.483 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 16 

16 threads running
Time: 21.496 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 17 

17 threads running
Time: 21.343 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 18 

18 threads running
Time: 21.552 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 19 

19 threads running
Time: 21.340 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 20 

20 threads running
Time: 21.280 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 21 

21 threads running
Time: 21.371 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 22 

22 threads running
Time: 21.577 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 23 

23 threads running
Time: 21.266 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 24 

24 threads running
Time: 21.338 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 25 

25 threads running
Time: 21.236 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 26 

26 threads running
Time: 21.298 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 27 

27 threads running
Time: 21.573 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 28 

28 threads running
Time: 21.537 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 29 

29 threads running
Time: 30.282 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 30 

30 threads running
Time: 33.232 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 31 

31 threads running
Time: 33.701 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 32 

32 threads running
Time: 32.743 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 33 

33 threads running
Time: 33.198 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 34 

34 threads running
Time: 32.357 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 35 

35 threads running
Time: 33.490 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 36 

36 threads running
Time: 33.343 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 37 

37 threads running
Time: 32.698 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 38 

38 threads running
Time: 33.576 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 39 

39 threads running
Time: 32.805 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 40 

40 threads running
Time: 33.716 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 41 

41 threads running
Time: 33.582 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 42 

42 threads running
Time: 33.419 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 43 

43 threads running
Time: 33.581 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 44 

44 threads running
Time: 33.566 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 45 

45 threads running
Time: 33.944 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 46 

46 threads running
Time: 34.068 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 47 

47 threads running
Time: 33.851 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 48 

48 threads running
Time: 33.954 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 49 

49 threads running
Time: 33.698 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 50 

50 threads running
Time: 34.049 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 51 

51 threads running
Time: 33.999 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 52 

52 threads running
Time: 34.009 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 53 

53 threads running
Time: 34.097 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 54 

54 threads running
Time: 34.046 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 55 

55 threads running
Time: 33.997 (s)
Accuracy: 4.015356e+02
=========================================================

num of omp threads = 56 

56 threads running
Time: 34.069 (s)
Accuracy: 4.015356e+02
=========================================================



CASE:512x4 [size:512x512, layers=4, iterations:10000] 

num of omp threads = 1 

1 threads running
CPU execution:67.028320 (sec)
Time: 82.096 (s)
Accuracy: 1.253826e-04
=========================================================

num of omp threads = 2 

2 threads running
Time: 41.402 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 3 

3 threads running
Time: 41.496 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 4 

4 threads running
Time: 21.373 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 5 

5 threads running
Time: 21.424 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 6 

6 threads running
Time: 21.245 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 7 

7 threads running
Time: 21.249 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 8 

8 threads running
Time: 21.145 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 9 

9 threads running
Time: 20.831 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 10 

10 threads running
Time: 20.971 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 11 

11 threads running
Time: 20.895 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 12 

12 threads running
Time: 20.882 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 13 

13 threads running
Time: 20.725 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 14 

14 threads running
Time: 20.995 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 15 

15 threads running
Time: 20.773 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 16 

16 threads running
Time: 21.133 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 17 

17 threads running
Time: 20.667 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 18 

18 threads running
Time: 20.654 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 19 

19 threads running
Time: 20.645 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 20 

20 threads running
Time: 21.002 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 21 

21 threads running
Time: 21.020 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 22 

22 threads running
Time: 20.811 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 23 

23 threads running
Time: 20.755 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 24 

24 threads running
Time: 20.916 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 25 

25 threads running
Time: 20.882 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 26 

26 threads running
Time: 20.854 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 27 

27 threads running
Time: 20.746 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 28 

28 threads running
Time: 20.809 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 29 

29 threads running
Time: 20.928 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 30 

30 threads running
Time: 27.597 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 31 

31 threads running
Time: 31.173 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 32 

32 threads running
Time: 30.023 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 33 

33 threads running
Time: 31.668 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 34 

34 threads running
Time: 32.398 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 35 

35 threads running
Time: 32.070 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 36 

36 threads running
Time: 33.139 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 37 

37 threads running
Time: 32.417 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 38 

38 threads running
Time: 33.415 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 39 

39 threads running
Time: 33.371 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 40 

40 threads running
Time: 33.414 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 41 

41 threads running
Time: 33.367 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 42 

42 threads running
Time: 33.573 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 43 

43 threads running
Time: 33.505 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 44 

44 threads running
Time: 33.599 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 45 

45 threads running
Time: 33.464 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 46 

46 threads running
Time: 33.552 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 47 

47 threads running
Time: 33.738 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 48 

48 threads running
Time: 33.170 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 49 

49 threads running
Time: 33.964 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 50 

50 threads running
Time: 33.623 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 51 

51 threads running
Time: 33.729 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 52 

52 threads running
Time: 33.844 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 53 

53 threads running
Time: 33.896 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 54 

54 threads running
Time: 33.830 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 55 

55 threads running
Time: 33.755 (s)
Accuracy: 6.931791e+02
=========================================================

num of omp threads = 56 

56 threads running
Time: 33.910 (s)
Accuracy: 6.931791e+02
=========================================================



CASE:512x2 [size:512x512, layers=2, iterations:10000] 

num of omp threads = 1 

1 threads running
CPU execution:32.565575 (sec)
Time: 40.786 (s)
Accuracy: 2.525852e-04
=========================================================

num of omp threads = 2 

2 threads running
Time: 20.952 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 3 

3 threads running
Time: 21.233 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 4 

4 threads running
Time: 20.725 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 5 

5 threads running
Time: 20.650 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 6 

6 threads running
Time: 21.156 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 7 

7 threads running
Time: 21.002 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 8 

8 threads running
Time: 20.803 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 9 

9 threads running
Time: 20.711 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 10 

10 threads running
Time: 20.581 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 11 

11 threads running
Time: 20.646 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 12 

12 threads running
Time: 20.769 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 13 

13 threads running
Time: 20.888 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 14 

14 threads running
Time: 20.831 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 15 

15 threads running
Time: 20.756 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 16 

16 threads running
Time: 20.694 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 17 

17 threads running
Time: 20.868 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 18 

18 threads running
Time: 20.791 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 19 

19 threads running
Time: 20.805 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 20 

20 threads running
Time: 20.858 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 21 

21 threads running
Time: 20.738 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 22 

22 threads running
Time: 20.724 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 23 

23 threads running
Time: 20.758 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 24 

24 threads running
Time: 21.084 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 25 

25 threads running
Time: 21.008 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 26 

26 threads running
Time: 20.863 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 27 

27 threads running
Time: 20.919 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 28 

28 threads running
Time: 20.882 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 29 

29 threads running
Time: 20.762 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 30 

30 threads running
Time: 20.843 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 31 

31 threads running
Time: 21.016 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 32 

32 threads running
Time: 20.987 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 33 

33 threads running
Time: 21.182 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 34 

34 threads running
Time: 30.491 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 35 

35 threads running
Time: 30.578 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 36 

36 threads running
Time: 27.164 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 37 

37 threads running
Time: 30.031 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 38 

38 threads running
Time: 30.946 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 39 

39 threads running
Time: 29.343 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 40 

40 threads running
Time: 32.499 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 41 

41 threads running
Time: 33.433 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 42 

42 threads running
Time: 32.512 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 43 

43 threads running
Time: 32.325 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 44 

44 threads running
Time: 32.290 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 45 

45 threads running
Time: 31.861 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 46 

46 threads running
Time: 32.989 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 47 

47 threads running
Time: 32.944 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 48 

48 threads running
Time: 32.595 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 49 

49 threads running
Time: 32.063 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 50 

50 threads running
Time: 33.754 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 51 

51 threads running
Time: 33.190 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 52 

52 threads running
Time: 33.587 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 53 

53 threads running
Time: 32.845 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 54 

54 threads running
Time: 33.819 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 55 

55 threads running
Time: 33.786 (s)
Accuracy: 1.088364e+03
=========================================================

num of omp threads = 56 

56 threads running
Time: 33.834 (s)
Accuracy: 1.088364e+03
=========================================================



CASE:64x8 [size:64x64, layers=8, iterations:10000] 

num of omp threads = 1 

1 threads running
CPU execution:2.085959 (sec)
Time: 2.568 (s)
Accuracy: 7.076655e-06
=========================================================

num of omp threads = 2 

2 threads running
Time: 1.297 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 3 

3 threads running
Time: 0.981 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 4 

4 threads running
Time: 0.737 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 5 

5 threads running
Time: 0.793 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 6 

6 threads running
Time: 0.809 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 7 

7 threads running
Time: 0.753 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 8 

8 threads running
Time: 0.466 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 9 

9 threads running
Time: 0.466 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 10 

10 threads running
Time: 0.446 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 11 

11 threads running
Time: 0.448 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 12 

12 threads running
Time: 0.501 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 13 

13 threads running
Time: 0.519 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 14 

14 threads running
Time: 0.493 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 15 

15 threads running
Time: 0.528 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 16 

16 threads running
Time: 0.489 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 17 

17 threads running
Time: 0.501 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 18 

18 threads running
Time: 0.484 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 19 

19 threads running
Time: 0.450 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 20 

20 threads running
Time: 0.465 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 21 

21 threads running
Time: 0.451 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 22 

22 threads running
Time: 0.467 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 23 

23 threads running
Time: 0.487 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 24 

24 threads running
Time: 0.496 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 25 

25 threads running
Time: 0.500 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 26 

26 threads running
Time: 0.493 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 27 

27 threads running
Time: 0.496 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 28 

28 threads running
Time: 0.760 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 29 

29 threads running
Time: 0.689 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 30 

30 threads running
Time: 0.624 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 31 

31 threads running
Time: 0.627 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 32 

32 threads running
Time: 0.641 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 33 

33 threads running
Time: 0.788 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 34 

34 threads running
Time: 0.801 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 35 

35 threads running
Time: 0.769 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 36 

36 threads running
Time: 0.717 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 37 

37 threads running
Time: 0.805 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 38 

38 threads running
Time: 0.786 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 39 

39 threads running
Time: 0.733 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 40 

40 threads running
Time: 0.733 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 41 

41 threads running
Time: 0.803 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 42 

42 threads running
Time: 0.750 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 43 

43 threads running
Time: 0.815 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 44 

44 threads running
Time: 0.809 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 45 

45 threads running
Time: 0.933 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 46 

46 threads running
Time: 0.831 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 47 

47 threads running
Time: 0.821 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 48 

48 threads running
Time: 0.834 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 49 

49 threads running
Time: 0.843 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 50 

50 threads running
Time: 0.852 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 51 

51 threads running
Time: 0.817 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 52 

52 threads running
Time: 0.863 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 53 

53 threads running
Time: 0.916 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 54 

54 threads running
Time: 0.875 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 55 

55 threads running
Time: 1.197 (s)
Accuracy: 8.618438e+01
=========================================================

num of omp threads = 56 

56 threads running
Time: 0.949 (s)
Accuracy: 8.618438e+01
=========================================================

