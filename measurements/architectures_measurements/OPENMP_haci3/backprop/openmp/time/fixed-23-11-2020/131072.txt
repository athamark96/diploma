_____________________________________________________________________________

    with OMP_NUM_THREADS= 1 
 

Random number generator seed: 7

NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.0713109970 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0064940453 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0190038681 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
=============================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 2 
 

Random number generator seed: 7

NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.1008059978 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0049979687 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0469801426 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 4 
 

Random number generator seed: 7

NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.0722420216 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0031430721 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0234980583 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 8 
 

Random number generator seed: 7

NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.0598220825 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0023300648 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0113220215 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 14 
 

Random number generator seed: 7

NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.0626819134 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0025408268 (sec)
        +-------error handle time 		     : 0.0000011921 (sec)
        +-------adjust weight time[parallel section] : 0.0136578083 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 28 
 

Random number generator seed: 7

NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.0567328930 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0022728443 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0085711479 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 42 
 

Random number generator seed: 7

NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.0777289867 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0157699585 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0151848793 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 56 
 

Random number generator seed: 7

NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.0736670494 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0153501034 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0119779110 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

