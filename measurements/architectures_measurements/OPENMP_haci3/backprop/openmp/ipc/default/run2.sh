cd /home/users/amarkou/DIPLOMA/rodinia_3.1/IPC/cpu/backprop


echo -e "omp htreads, total ins, total cycles, ipc(per thread)" >> ipc_succint.csv
echo -e "____________________________________________________________________________________________________\n"
echo -e "    with OMP_NUM_THREADS= 1 \n \n"
export OMP_NUM_THREADS=1
./backprop 65536 
echo -e "====================================================================================================\n"

for t in {2..56..2}
do
	echo -e "____________________________________________________________________________________________________\n"
	echo -e "    with OMP_NUM_THREADS= $t \n \n"
	export OMP_NUM_THREADS=$t
	./backprop 65536
	echo -e "====================================================================================================\n"
done

