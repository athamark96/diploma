_____________________________________________________________________________

    with OMP_NUM_THREADS= 1 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000030270000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000022848000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000037586000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000020906000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 1.847534179000
PACKAGE_ENERGY:PACKAGE1 (J) 1.394531250000
DRAM_ENERGY:PACKAGE0    (J) 0.573516845000
DRAM_ENERGY:PACKAGE1    (J) 0.319000244000

Total train kernel time:  0.056737899780 (s)
Power (W) 72.871617278959
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.1395020485 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0117459297 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0449919701 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
=============================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 2 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000048493000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000043788000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000061902000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000033432000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 2.959777832000
PACKAGE_ENERGY:PACKAGE1 (J) 2.672607421000
DRAM_ENERGY:PACKAGE0    (J) 0.944549560000
DRAM_ENERGY:PACKAGE1    (J) 0.510131835000

Total train kernel time:  0.084046125412 (s)
Power (W) 84.323537977031
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.1702971458 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0072660446 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0767800808 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 4 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000024401000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000026343000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000032286000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000016565000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 1.455200195000
PACKAGE_ENERGY:PACKAGE1 (J) 1.607849121000
DRAM_ENERGY:PACKAGE0    (J) 0.492645263000
DRAM_ENERGY:PACKAGE1    (J) 0.252761840000

Total train kernel time:  0.041190147400 (s)
Power (W) 92.460373642873
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.1267859936 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0045449734 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0366451740 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 8 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000017483000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000016519000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000021768000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000008933000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 1.067077636000
PACKAGE_ENERGY:PACKAGE1 (J) 1.008239746000
DRAM_ENERGY:PACKAGE0    (J) 0.332153320000
DRAM_ENERGY:PACKAGE1    (J) 0.136306762000

Total train kernel time:  0.022183895111 (s)
Power (W) 114.667755651668
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.1084821224 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0032839775 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0188999176 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 14 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000021838000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000021260000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000022687000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000010414000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 1.332885742000
PACKAGE_ENERGY:PACKAGE1 (J) 1.273010253000
DRAM_ENERGY:PACKAGE0    (J) 0.346176147000
DRAM_ENERGY:PACKAGE1    (J) 0.158905029000

Total train kernel time:  0.025403976440 (s)
Power (W) 122.460244690236
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.1126599312 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0037539005 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0216500759 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 28 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000019854000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000018082000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000016344000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000006482000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 1.211791992000
PACKAGE_ENERGY:PACKAGE1 (J) 1.103637695000
DRAM_ENERGY:PACKAGE0    (J) 0.249389648000
DRAM_ENERGY:PACKAGE1    (J) 0.098907470000

Total train kernel time:  0.016577005386 (s)
Power (W) 160.688058121341
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.1030678749 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0032567978 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0133202076 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 42 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000018787000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000017529000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000015281000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000006459000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 1.146667480000
PACKAGE_ENERGY:PACKAGE1 (J) 1.069885253000
DRAM_ENERGY:PACKAGE0    (J) 0.233169555000
DRAM_ENERGY:PACKAGE1    (J) 0.098556518000

Total train kernel time:  0.015959024429 (s)
Power (W) 159.676352228529
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.1028618813 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0034530163 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0125060081 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 56 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000039023000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000035998000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000021452000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000012406000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 2.381774902000
PACKAGE_ENERGY:PACKAGE1 (J) 2.162536621000
DRAM_ENERGY:PACKAGE0    (J) 0.327331542000
DRAM_ENERGY:PACKAGE1    (J) 0.189300537000

Total train kernel time:  0.030238151550 (s)
Power (W) 167.369476721568
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.1171460152 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0114920139 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0187461376 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

