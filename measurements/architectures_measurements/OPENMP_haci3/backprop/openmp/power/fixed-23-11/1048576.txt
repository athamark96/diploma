_____________________________________________________________________________

    with OMP_NUM_THREADS= 1 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000210304000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000278314000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000181956000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000397279000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 12.835937500000
PACKAGE_ENERGY:PACKAGE1 (J) 16.986938476000
DRAM_ENERGY:PACKAGE0    (J) 2.776428222000
DRAM_ENERGY:PACKAGE1    (J) 6.061996459000

Total train kernel time:  0.466080904007 (s)
Power (W) 82.949763280632
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.8692929745 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.1416368484 (sec)
        +-------error handle time 		     : 0.0000009537 (sec)
        +-------adjust weight time[parallel section] : 0.3244440556 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
=============================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 2 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000305747000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000278176000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000299220000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000116407000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 18.661315917000
PACKAGE_ENERGY:PACKAGE1 (J) 16.978515625000
DRAM_ENERGY:PACKAGE0    (J) 4.565734863000
DRAM_ENERGY:PACKAGE1    (J) 1.776229858000

Total train kernel time:  0.296612024307 (s)
Power (W) 141.537742311864
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.7227709293 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0876231194 (sec)
        +-------error handle time 		     : 0.0000009537 (sec)
        +-------adjust weight time[parallel section] : 0.2089889050 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 4 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000135247000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000141295000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000202216000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000075899000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 8.254821777000
PACKAGE_ENERGY:PACKAGE1 (J) 8.623962402000
DRAM_ENERGY:PACKAGE0    (J) 3.085571289000
DRAM_ENERGY:PACKAGE1    (J) 1.158126831000

Total train kernel time:  0.191487073898 (s)
Power (W) 110.307614341721
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.6111829281 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0481469631 (sec)
        +-------error handle time 		     : 0.0000009537 (sec)
        +-------adjust weight time[parallel section] : 0.1433401108 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 8 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000124094000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000113288000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000122908000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000042270000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 7.574096679000
PACKAGE_ENERGY:PACKAGE1 (J) 6.914550781000
DRAM_ENERGY:PACKAGE0    (J) 1.875427246000
DRAM_ENERGY:PACKAGE1    (J) 0.644989013000

Total train kernel time:  0.103662252426 (s)
Power (W) 164.081556409531
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.5184361935 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0226821899 (sec)
        +-------error handle time 		     : 0.0000009537 (sec)
        +-------adjust weight time[parallel section] : 0.0809800625 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 14 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000109923000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000111509000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000109475000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000037601000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 6.651794433000
PACKAGE_ENERGY:PACKAGE1 (J) 6.805969238000
DRAM_ENERGY:PACKAGE0    (J) 1.670455932000
DRAM_ENERGY:PACKAGE1    (J) 0.573745727000

Total train kernel time:  0.094871044159 (s)
Power (W) 165.508511782529
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.5112190247 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0235979557 (sec)
        +-------error handle time 		     : 0.0000009537 (sec)
        +-------adjust weight time[parallel section] : 0.0712730885 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 28 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000086299000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000084808000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000076449000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000027198000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 5.267272949000
PACKAGE_ENERGY:PACKAGE1 (J) 5.176269531000
DRAM_ENERGY:PACKAGE0    (J) 1.166519165000
DRAM_ENERGY:PACKAGE1    (J) 0.415008544000

Total train kernel time:  0.067152023315 (s)
Power (W) 179.072343546750
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.4879431725 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0138750076 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0532770157 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 42 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000067781000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000076563000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000071077000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000025599000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 4.137023925000
PACKAGE_ENERGY:PACKAGE1 (J) 4.673034667000
DRAM_ENERGY:PACKAGE0    (J) 1.084548950000
DRAM_ENERGY:PACKAGE1    (J) 0.390609741000

Total train kernel time:  0.063790082932 (s)
Power (W) 161.235364657570
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.4786570072 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0158510208 (sec)
        +-------error handle time 		     : 0.0000000000 (sec)
        +-------adjust weight time[parallel section] : 0.0479390621 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

___________________________________________________________________________

    with OMP_NUM_THREADS= 56 
 

Random number generator seed: 7
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.000081199000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.000078233000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.000074044000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.000028184000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 4.955993652000
PACKAGE_ENERGY:PACKAGE1 (J) 4.774963378000
DRAM_ENERGY:PACKAGE0    (J) 1.129821777000
DRAM_ENERGY:PACKAGE1    (J) 0.423599243000

Total train kernel time:  0.069471120834 (s)
Power (W) 162.432647040586
==================================================================



NUMBER OF OMP THREADS = 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total execution [setup()] time         : 0.4849820137 (sec)
Initialization time [bpnn_initialize()]: 0.0000000000 (sec)
backprop_face() time: 0.0000000000 (sec)
*-------bpnn_create time: 0.0000000000 (sec)
*-------bpnn_train_kernel() time: 0.0000000000 (sec)
	+-------layerforward time [parallel section] : 0.0221009254 (sec)
        +-------error handle time 		     : 0.0000009537 (sec)
        +-------adjust weight time[parallel section] : 0.0473701954 (sec)
*-------deallocation time: 0.0000000000 (sec)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
===========================================================================

