OMP_NUM_THREADS=1 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.029231882000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.024215877000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.031395284000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.020411390000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 1784.172485351000
PACKAGE_ENERGY:PACKAGE1 (J) 1478.019836425000
DRAM_ENERGY:PACKAGE0    (J) 479.054016113000
DRAM_ENERGY:PACKAGE1    (J) 311.453094482000

Total time: 56.617423057556 (s)
Power (W) 71.580429018310
==============================================


time = 56.617423
time pgain = 55.786936
time pgain_dist = 55.201743
time pgain_init = 0.177464
time pselect = 0.000279
time pspeedy = 0.619086
time pshuffle = 0.006121
time localSearch = 56.431722
loops=7
--------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=2 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.016438400000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.016379499000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.015985926000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.010675485000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 1003.320312500000
PACKAGE_ENERGY:PACKAGE1 (J) 999.725280761000
DRAM_ENERGY:PACKAGE0    (J) 243.925872802000
DRAM_ENERGY:PACKAGE1    (J) 162.894973754000

Total time: 28.296478033066 (s)
Power (W) 85.164890026277
==============================================


time = 28.296478
time pgain = 27.479843
time pgain_dist = 27.016838
time pgain_init = 0.176678
time pselect = 0.000276
time pspeedy = 0.614043
time pshuffle = 0.006160
time localSearch = 28.119619
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=4 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.011834214000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011408484000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.010778520000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.006462288000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 722.303100585000
PACKAGE_ENERGY:PACKAGE1 (J) 696.318603515000
DRAM_ENERGY:PACKAGE0    (J) 164.467163085000
DRAM_ENERGY:PACKAGE1    (J) 98.606689453000

Total time: 16.772601842880 (s)
Power (W) 100.264441521448
==============================================


time = 16.772602
time pgain = 15.940949
time pgain_dist = 15.556148
time pgain_init = 0.176828
time pselect = 0.000281
time pspeedy = 0.614869
time pshuffle = 0.006110
time localSearch = 16.581502
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=6 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012262159000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011997413000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.011071671000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.006383920000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 748.422790527000
PACKAGE_ENERGY:PACKAGE1 (J) 732.263977050000
DRAM_ENERGY:PACKAGE0    (J) 168.940292358000
DRAM_ENERGY:PACKAGE1    (J) 97.410888671000

Total time: 16.428383111954 (s)
Power (W) 106.342659329317
==============================================


time = 16.428383
time pgain = 15.602300
time pgain_dist = 14.543825
time pgain_init = 0.196410
time pselect = 0.000283
time pspeedy = 0.619489
time pshuffle = 0.006119
time localSearch = 16.247601
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=8 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.009629521000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.010422615000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.004995300000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.008161172000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 587.766662597000
PACKAGE_ENERGY:PACKAGE1 (J) 636.145935058000
DRAM_ENERGY:PACKAGE0    (J) 76.222229003000
DRAM_ENERGY:PACKAGE1    (J) 124.529602050000

Total time: 12.121098995209 (s)
Power (W) 117.535912318771
==============================================


time = 12.121099
time pgain = 11.281368
time pgain_dist = 9.662012
time pgain_init = 0.157400
time pselect = 0.000301
time pspeedy = 0.632208
time pshuffle = 0.006299
time localSearch = 11.939517
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=10 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.010257957000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.010918054000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.005168865000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.008678666000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 626.096008300000
PACKAGE_ENERGY:PACKAGE1 (J) 666.385131835000
DRAM_ENERGY:PACKAGE0    (J) 78.870620727000
DRAM_ENERGY:PACKAGE1    (J) 132.425933837000

Total time: 12.544524908066 (s)
Power (W) 119.875220920651
==============================================


time = 12.544525
time pgain = 11.727664
time pgain_dist = 9.399137
time pgain_init = 0.157100
time pselect = 0.000285
time pspeedy = 0.611323
time pshuffle = 0.006011
time localSearch = 12.364630
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=12 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.011482714000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.012611571000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.005687988000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.009552848000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 700.849243164000
PACKAGE_ENERGY:PACKAGE1 (J) 769.749206542000
DRAM_ENERGY:PACKAGE0    (J) 86.791809082000
DRAM_ENERGY:PACKAGE1    (J) 145.764892578000

Total time: 13.757168054581 (s)
Power (W) 123.801289960902
==============================================


time = 13.757168
time pgain = 12.920024
time pgain_dist = 10.591328
time pgain_init = 0.176886
time pselect = 0.000291
time pspeedy = 0.630256
time pshuffle = 0.006320
time localSearch = 13.576320
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=14 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.010906711000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011400206000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.008875204000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.004654057000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 665.692810058000
PACKAGE_ENERGY:PACKAGE1 (J) 695.813354492000
DRAM_ENERGY:PACKAGE0    (J) 135.424865722000
DRAM_ENERGY:PACKAGE1    (J) 71.015274047000

Total time: 11.842819929123 (s)
Power (W) 132.396364523219
==============================================


time = 11.842820
time pgain = 11.022398
time pgain_dist = 8.935022
time pgain_init = 0.177362
time pselect = 0.000282
time pspeedy = 0.613960
time pshuffle = 0.006135
time localSearch = 11.662148
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=16 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012655901000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.012071485000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.009441207000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.004978218000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 772.454895019000
PACKAGE_ENERGY:PACKAGE1 (J) 736.784973144000
DRAM_ENERGY:PACKAGE0    (J) 144.054702758000
DRAM_ENERGY:PACKAGE1    (J) 75.961578369000

Total time: 12.697688817978 (s)
Power (W) 136.186685158141
==============================================


time = 12.697689
time pgain = 11.876755
time pgain_dist = 9.693945
time pgain_init = 0.177695
time pselect = 0.000279
time pspeedy = 0.613808
time pshuffle = 0.006136
time localSearch = 12.516326
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=18 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.011221873000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011149717000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.008340408000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.004346525000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 684.928771972000
PACKAGE_ENERGY:PACKAGE1 (J) 680.524719238000
DRAM_ENERGY:PACKAGE0    (J) 127.264526367000
DRAM_ENERGY:PACKAGE1    (J) 66.322708129000

Total time: 11.081475019455 (s)
Power (W) 140.688917582624
==============================================


time = 11.081475
time pgain = 10.260285
time pgain_dist = 8.122812
time pgain_init = 0.158009
time pselect = 0.000276
time pspeedy = 0.613538
time pshuffle = 0.006158
time localSearch = 10.899742
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=20 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.011779496000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011608022000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.008301936000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.004311920000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 718.916564941000
PACKAGE_ENERGY:PACKAGE1 (J) 708.497436523000
DRAM_ENERGY:PACKAGE0    (J) 126.677490234000
DRAM_ENERGY:PACKAGE1    (J) 65.794677734000

Total time: 10.916026830673 (s)
Power (W) 148.395216919057
==============================================


time = 10.916027
time pgain = 10.098075
time pgain_dist = 7.967768
time pgain_init = 0.157736
time pselect = 0.000283
time pspeedy = 0.612146
time pshuffle = 0.006130
time localSearch = 10.735984
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=22 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012357727000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011722092000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.008101846000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.004235627000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 754.255798339000
PACKAGE_ENERGY:PACKAGE1 (J) 715.459716796000
DRAM_ENERGY:PACKAGE0    (J) 123.624359130000
DRAM_ENERGY:PACKAGE1    (J) 64.630538940000

Total time: 10.735816955566 (s)
Power (W) 154.433558253372
==============================================


time = 10.735817
time pgain = 9.916884
time pgain_dist = 7.884471
time pgain_init = 0.157684
time pselect = 0.000284
time pspeedy = 0.612792
time pshuffle = 0.006124
time localSearch = 10.555503
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=24 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.011525406000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.010921022000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007390626000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003756457000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 703.454956054000
PACKAGE_ENERGY:PACKAGE1 (J) 666.566284179000
DRAM_ENERGY:PACKAGE0    (J) 112.772003173000
DRAM_ENERGY:PACKAGE1    (J) 57.318984985000

Total time:  9.485496997833 (s)
Power (W) 162.364948166955
==============================================


time = 9.485497
time pgain = 8.668272
time pgain_dist = 6.750815
time pgain_init = 0.157841
time pselect = 0.000286
time pspeedy = 0.610899
time pshuffle = 0.006185
time localSearch = 9.305061
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=26 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012578619000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.012291039000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007845419000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.004064668000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 767.737976074000
PACKAGE_ENERGY:PACKAGE1 (J) 750.185485839000
DRAM_ENERGY:PACKAGE0    (J) 119.711593627000
DRAM_ENERGY:PACKAGE1    (J) 62.021911621000

Total time: 10.279426097870 (s)
Power (W) 165.345511605284
==============================================


time = 10.279426
time pgain = 9.460414
time pgain_dist = 7.551786
time pgain_init = 0.158161
time pselect = 0.000284
time pspeedy = 0.611538
time pshuffle = 0.006121
time localSearch = 10.097719
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=28 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012832280000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.012334077000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007682976000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003986034000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 783.220214843000
PACKAGE_ENERGY:PACKAGE1 (J) 752.812316894000
DRAM_ENERGY:PACKAGE0    (J) 117.232910156000
DRAM_ENERGY:PACKAGE1    (J) 60.822052001000

Total time: 10.035243034363 (s)
Power (W) 170.806774487135
==============================================


time = 10.035243
time pgain = 9.216737
time pgain_dist = 7.375578
time pgain_init = 0.157675
time pselect = 0.000276
time pspeedy = 0.612361
time pshuffle = 0.006117
time localSearch = 9.854867
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=30 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012684914000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.012251780000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007651947000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003952375000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 774.225708007000
PACKAGE_ENERGY:PACKAGE1 (J) 747.789306640000
DRAM_ENERGY:PACKAGE0    (J) 116.759445190000
DRAM_ENERGY:PACKAGE1    (J) 60.308456420000

Total time:  9.941109895706 (s)
Power (W) 170.914810728617
==============================================


time = 9.941110
time pgain = 9.122899
time pgain_dist = 7.004347
time pgain_init = 0.167900
time pselect = 0.000280
time pspeedy = 0.611309
time pshuffle = 0.006117
time localSearch = 9.760065
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=32 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012508725000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.012090624000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007631225000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003915778000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 763.471984863000
PACKAGE_ENERGY:PACKAGE1 (J) 737.953125000000
DRAM_ENERGY:PACKAGE0    (J) 116.443252563000
DRAM_ENERGY:PACKAGE1    (J) 59.750030517000

Total time:  9.848715066910 (s)
Power (W) 170.338808823858
==============================================


time = 9.848715
time pgain = 9.029430
time pgain_dist = 6.747020
time pgain_init = 0.182492
time pselect = 0.000287
time pspeedy = 0.611175
time pshuffle = 0.006150
time localSearch = 9.666547
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=34 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.011409275000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.010986213000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007123098000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003566649000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 696.366882324000
PACKAGE_ENERGY:PACKAGE1 (J) 670.545227050000
DRAM_ENERGY:PACKAGE0    (J) 108.689849853000
DRAM_ENERGY:PACKAGE1    (J) 54.422744750000

Total time:  8.940474033356 (s)
Power (W) 171.134628686206
==============================================


time = 8.940474
time pgain = 8.121188
time pgain_dist = 5.982839
time pgain_init = 0.189042
time pselect = 0.000283
time pspeedy = 0.611971
time pshuffle = 0.006128
time localSearch = 8.758992
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=36 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.010857827000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.010408600000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.006844961000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003370183000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 662.709167480000
PACKAGE_ENERGY:PACKAGE1 (J) 635.290527343000
DRAM_ENERGY:PACKAGE0    (J) 104.445816040000
DRAM_ENERGY:PACKAGE1    (J) 51.424911499000

Total time:  8.425701856613 (s)
Power (W) 172.551847561623
==============================================


time = 8.425702
time pgain = 7.608487
time pgain_dist = 5.430210
time pgain_init = 0.192538
time pselect = 0.000283
time pspeedy = 0.610717
time pshuffle = 0.006132
time localSearch = 8.245048
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=38 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.010768918000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.010382799000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.006793693000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003338070000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 657.282592773000
PACKAGE_ENERGY:PACKAGE1 (J) 633.715759277000
DRAM_ENERGY:PACKAGE0    (J) 103.663528442000
DRAM_ENERGY:PACKAGE1    (J) 50.934906005000

Total time:  8.342888116837 (s)
Power (W) 173.272944123472
==============================================


time = 8.342888
time pgain = 7.524258
time pgain_dist = 5.435403
time pgain_init = 0.197253
time pselect = 0.000285
time pspeedy = 0.611194
time pshuffle = 0.006154
time localSearch = 8.161345
loops=7
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=40 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.013695883000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.012926060000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.008446293000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.004183176000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 835.930358886000
PACKAGE_ENERGY:PACKAGE1 (J) 788.971008300000
DRAM_ENERGY:PACKAGE0    (J) 128.880203247000
DRAM_ENERGY:PACKAGE1    (J) 63.833160400000

Total time: 10.444545030594 (s)
Power (W) 174.025266348021
==============================================


time = 10.444545
time pgain = 9.625905
time pgain_dist = 7.566111
time pgain_init = 0.293099
time pselect = 0.000291
time pspeedy = 0.611940
time pshuffle = 0.006159
time localSearch = 10.263950
loops=9
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=42 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012467298000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011588956000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007676456000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003782495000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 760.943481445000
PACKAGE_ENERGY:PACKAGE1 (J) 707.333740234000
DRAM_ENERGY:PACKAGE0    (J) 117.133422851000
DRAM_ENERGY:PACKAGE1    (J) 57.716293334000

Total time:  9.441667079926 (s)
Power (W) 174.029323842348
==============================================


time = 9.441667
time pgain = 8.620941
time pgain_dist = 6.645365
time pgain_init = 0.274565
time pselect = 0.000285
time pspeedy = 0.610443
time pshuffle = 0.006145
time localSearch = 9.257382
loops=8
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=44 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012705125000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011954875000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007958182000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003814489000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 775.459289550000
PACKAGE_ENERGY:PACKAGE1 (J) 729.667663574000
DRAM_ENERGY:PACKAGE0    (J) 121.432220458000
DRAM_ENERGY:PACKAGE1    (J) 58.204483032000

Total time:  9.552967071533 (s)
Power (W) 176.360249543245
==============================================


time = 9.552967
time pgain = 8.736199
time pgain_dist = 6.615681
time pgain_init = 0.286668
time pselect = 0.000290
time pspeedy = 0.609945
time pshuffle = 0.006141
time localSearch = 9.372170
loops=9
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=46 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.013552437000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.012844447000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.008366862000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.004097933000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 827.175109863000
PACKAGE_ENERGY:PACKAGE1 (J) 783.962829589000
DRAM_ENERGY:PACKAGE0    (J) 127.668182373000
DRAM_ENERGY:PACKAGE1    (J) 62.529495239000

Total time: 10.231356143951 (s)
Power (W) 176.060298529332
==============================================


time = 10.231356
time pgain = 9.412220
time pgain_dist = 7.263371
time pgain_init = 0.305391
time pselect = 0.000282
time pspeedy = 0.611339
time pshuffle = 0.006161
time localSearch = 10.049699
loops=9
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=48 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012443085000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011450472000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007710625000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003677846000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 759.465637207000
PACKAGE_ENERGY:PACKAGE1 (J) 698.881347656000
DRAM_ENERGY:PACKAGE0    (J) 117.654800415000
DRAM_ENERGY:PACKAGE1    (J) 56.119476318000

Total time:  9.144626855850 (s)
Power (W) 178.478716225787
==============================================


time = 9.144627
time pgain = 8.323833
time pgain_dist = 6.246015
time pgain_init = 0.315632
time pselect = 0.000290
time pspeedy = 0.611865
time pshuffle = 0.006153
time localSearch = 8.961756
loops=9
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=50 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012617593000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011763217000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007822931000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003747431000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 770.116760253000
PACKAGE_ENERGY:PACKAGE1 (J) 717.969787597000
DRAM_ENERGY:PACKAGE0    (J) 119.368453979000
DRAM_ENERGY:PACKAGE1    (J) 57.181259155000

Total time:  9.327582836151 (s)
Power (W) 178.463841085638
==============================================


time = 9.327583
time pgain = 8.497345
time pgain_dist = 6.357737
time pgain_init = 0.314508
time pselect = 0.000295
time pspeedy = 0.621098
time pshuffle = 0.006377
time localSearch = 9.144819
loops=9
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=52 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.012283836000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.011533908000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.007556622000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003648627000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 749.745849609000
PACKAGE_ENERGY:PACKAGE1 (J) 703.973876953000
DRAM_ENERGY:PACKAGE0    (J) 115.304901123000
DRAM_ENERGY:PACKAGE1    (J) 55.673629760000

Total time:  9.143602132797 (s)
Power (W) 177.686893398102
==============================================


time = 9.143602
time pgain = 8.322128
time pgain_dist = 6.209283
time pgain_init = 0.310097
time pselect = 0.000282
time pspeedy = 0.613824
time pshuffle = 0.006176
time localSearch = 8.962066
loops=9
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=54 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.010160964000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.009389976000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.006359386000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.002992937000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 620.176025390000
PACKAGE_ENERGY:PACKAGE1 (J) 573.118652343000
DRAM_ENERGY:PACKAGE0    (J) 97.036529541000
DRAM_ENERGY:PACKAGE1    (J) 45.668594360000

Total time:  7.472887039185 (s)
Power (W) 178.779606145336
==============================================


time = 7.472887
time pgain = 6.648540
time pgain_dist = 4.649343
time pgain_init = 0.262637
time pselect = 0.000287
time pspeedy = 0.611142
time pshuffle = 0.006133
time localSearch = 7.285760
loops=8
---------------------------------------------------------------------------------------------------

OMP_NUM_THREADS=56 

PARSEC Benchmark Suite
PAPI measurements:(rapl)> > > > > > > > > > > >
Counting Energy Consumption:
PACKAGE_ENERGY_CNT:PACKAGE0 (J) 0.010240343000
PACKAGE_ENERGY_CNT:PACKAGE1 (J) 0.009549689000
DRAM_ENERGY_CNT:PACKAGE0 (J)    0.006398398000
DRAM_ENERGY_CNT:PACKAGE1 (J)    0.003046054000


Energy in total:
PACKAGE_ENERGY:PACKAGE0 (J) 625.046936035000
PACKAGE_ENERGY:PACKAGE1 (J) 582.866760253000
DRAM_ENERGY:PACKAGE0    (J) 97.638824462000
DRAM_ENERGY:PACKAGE1    (J) 46.479095458000

Total time:  7.568902969360 (s)
Power (W) 178.629799018584
==============================================


time = 7.568903
time pgain = 6.742332
time pgain_dist = 4.577882
time pgain_init = 0.264441
time pselect = 0.000281
time pspeedy = 0.612205
time pshuffle = 0.006143
time localSearch = 7.380603
loops=8
---------------------------------------------------------------------------------------------------

