BFS Openmp:
executed for 3 different inputs for 1 kernel execution and for 10000 executions.
Plots suggest that:
	i)	1 kernel execution does not give valid measurements
	ii)	10000 kernel execution improves the measurements but still for small inputs the measurements are "noisy" 
