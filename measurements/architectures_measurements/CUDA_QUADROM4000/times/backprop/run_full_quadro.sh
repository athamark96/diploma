cd ~/DIPLOMA/rodinia_3.1/cuda/backprop

echo -e "\n=========================\n\n\ndataset1:\n\n"
./run1.sh 65536 

echo -e "\n=========================\n\n\ndataset2:\n\n"
./run1.sh 131072 

echo -e "\n=========================\n\n\ndataset3:\n\n"
./run1.sh 196608

echo -e "\n=========================\n\n\ndataset4:\n\n"
./run1.sh 262144

echo -e "\n=========================\n\n\ndataset5:\n\n"
./run1.sh 327680 

echo -e "\n=========================\n\n\ndataset6:\n\n"
./run1.sh 393216

echo -e "\n=========================\n\n\ndataset7:\n\n"
./run1.sh 524288

echo -e "\n=========================\n\n\ndataset8:\n\n"
./run1.sh 1048576

