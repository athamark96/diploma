#!/bin/bash
cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/kmeans

echo -e "[][][][][][][][][][][][][][][][][][][][][][][]\n"
echo -e "THREADS_PER_DIM 32\nBLOCKS_PER_DIM 8\nTHREADS_PER_BLOCK THREADS_PER_DIM*THREADS_PER_DIM\n"
echo -e "\n\ndim3 blockSize(32,32,1)\ngridSize(8,8,1)\n"
echo -e "[][][][][][][][][][][][][][][][][][][][][][][]\n"

########################RUNS#############################
echo -e "100:\n"
./kmeans -i ./../../data/kmeans/100
echo -e "----------------------------------------------\n"

echo -e "204800.txt:\n"
./kmeans -i ../../data/kmeans/204800.txt
echo -e "----------------------------------------------\n"

echo -e "819200.txt:\n"
./kmeans -i ../../data/kmeans/819200.txt
echo -e "----------------------------------------------\n"

echo -e "kdd_cup:\n"
./kmeans -i ../../data/kmeans/kdd_cup
echo -e "----------------------------------------------\n"


