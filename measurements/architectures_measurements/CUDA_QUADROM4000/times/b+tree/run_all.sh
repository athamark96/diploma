#!/bin/bash
cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/b+tree
make clean

echo -e "QUADRO M4000 measurements\n\n\n\n"

for kern_dim in 256 512 1024  
do
	make KERNEL_DIM="-DRD_WG_SIZE_0=$kern_dim"

	echo -e "kernel_size: $kern_dim\n"
	echo -e "minimum dataset:\n"
	./run_min.sh 
	echo -e "\n----------------------\n"
	
        echo -e "\nintermediate dataset:\n"
        ./run_intermed.sh
        echo -e "\n----------------------\n"

        echo -e "\nmax dataset:\n"
        ./run_max.sh
        echo -e "\n===================================================\n"
        echo -e "++++++++++++++++++++++++++++++++++++++++++++++++++\n"
	echo -e "++++++++++++++++++++++++++++++++++++++++++++++++++\n"
	make clean	
        echo -e "++++++++++++++++++++++++++++++++++++++++++++++++++\n"
        echo -e "++++++++++++++++++++++++++++++++++++++++++++++++++\n"
        echo -e "==================================================\n"

done
