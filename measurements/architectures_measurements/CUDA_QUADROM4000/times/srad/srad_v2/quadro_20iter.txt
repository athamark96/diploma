rm -f srad srad.linkinfo result.txt
/usr/local/cuda/bin/nvcc  srad.cu -o srad -I/usr/local/cuda/include -L/usr/local/cuda/lib64 

=====================================

2K:



Block=(32,32,1)

-----------------------------------------------------------------------

WG size of kernel = 32 X 32
Start the SRAD main loop
Time spent in different stages of the application:
GPU:
  0.000000000000 s,  0.000000000000 % : CUDA MALLOCS
  0.000003099442 s,  0.000236883229 % : SET KERNEL SIZES
  0.099068880081 s,  7.571607977936 % : MEM COPIES:HOST -> DEV
  0.000364065170 s,  0.027824668509 % : KERNEL 1
  0.000104188919 s,  0.007962920850 % : KERNEL 2
  0.187258243561 s, 14.311719378652 % : MEM COPY  :DEV -> HOST
  0.000016927719 s,  0.001293746866 % : GPU FINAL SYNCHRONIZATION 
  0.009088993073 s,  0.694650958029 % : CUDA FREES

CPU:
0.000043869018555 s, 0.003352808779111 %: INIT
0.871155023574829 s, 66.580386505965080 %: RANDOMIZE
0.001124858856201 s, 0.085970390325252 %: CPU FREEs


  1.308425903320 s : TOTAL TIME
  0.295904397964 s : GPU TOTAL TIME
=======================================================================


=====================================

4K:



Block=(32,32,1)

-----------------------------------------------------------------------

WG size of kernel = 32 X 32
Start the SRAD main loop
Time spent in different stages of the application:
GPU:
  0.000000000000 s,  0.000000000000 % : CUDA MALLOCS
  0.000003337860 s,  0.000070953671 % : SET KERNEL SIZES
  0.417903184891 s,  8.883465438087 % : MEM COPIES:HOST -> DEV
  0.000513792038 s,  0.010921797145 % : KERNEL 1
  0.000114679337 s,  0.002437765395 % : KERNEL 2
  0.707946777344 s, 15.048989708423 % : MEM COPY  :DEV -> HOST
  0.000016927719 s,  0.000359836472 % : GPU FINAL SYNCHRONIZATION 
  0.025742053986 s,  0.547204843500 % : CUDA FREES

CPU:
0.000015974044800 s, 0.000339563994763 %: INIT
3.439244031906128 s, 73.108812268497815 %: RANDOMIZE
0.003839015960693 s, 0.081606857368171 %: CPU FREEs


  4.704281091690 s : TOTAL TIME
  1.152240753174 s : GPU TOTAL TIME
=======================================================================


=====================================

8K:



Block=(32,32,1)

-----------------------------------------------------------------------

WG size of kernel = 32 X 32
Start the SRAD main loop
Time spent in different stages of the application:
GPU:
  0.000000000000 s,  0.000000000000 % : CUDA MALLOCS
  0.000002145767 s,  0.000011702776 % : SET KERNEL SIZES
  1.694321870804 s,  9.240643393148 % : MEM COPIES:HOST -> DEV
  0.000515937805 s,  0.002813867514 % : KERNEL 1
  0.000110864639 s,  0.000604643435 % : KERNEL 2
  2.778407573700 s, 15.153126470121 % : MEM COPY  :DEV -> HOST
  0.000015974045 s,  0.000087120667 % : GPU FINAL SYNCHRONIZATION 
  0.032491922379 s,  0.177207337656 % : CUDA FREES

CPU:
0.000015020370483 s, 0.000081919433174 %: INIT
13.708391189575195 s, 74.764043750750687 %: RANDOMIZE
0.014646053314209 s, 0.079877948886669 %: CPU FREEs


 18.335540056229 s : TOTAL TIME
  4.505866289139 s : GPU TOTAL TIME
=======================================================================


=====================================

16K:



Block=(32,32,1)

-----------------------------------------------------------------------

WG size of kernel = 32 X 32
Start the SRAD main loop
Time spent in different stages of the application:
GPU:
  0.000000000000 s,  0.000000000000 % : CUDA MALLOCS
  0.000001192093 s,  0.000001634895 % : SET KERNEL SIZES
  6.795623779297 s,  9.319855735525 % : MEM COPIES:HOST -> DEV
  0.000515699387 s,  0.000707255734 % : KERNEL 1
  0.000108242035 s,  0.000148448499 % : KERNEL 2
 11.069519042969 s, 15.181287824735 % : MEM COPY  :DEV -> HOST
  0.000013828278 s,  0.000018964786 % : GPU FINAL SYNCHRONIZATION 
  0.033690929413 s,  0.046205412766 % : CUDA FREES

CPU:
0.000050067901611 s, 0.000068665605271 %: INIT
54.813352823257446 s, 75.173752592003979 %: RANDOMIZE
0.057931184768677 s, 0.079449702068113 %: CPU FREEs


 72.915546894073 s : TOTAL TIME
 17.899472713470 s : GPU TOTAL TIME
=======================================================================


=====================================

32K:



Block=(32,32,1)

-----------------------------------------------------------------------

WG size of kernel = 32 X 32
Start the SRAD main loop
Time spent in different stages of the application:
GPU:
  0.000000000000 s,  0.000000000000 % : CUDA MALLOCS
  0.000000000000 s,  0.000000000000 % : SET KERNEL SIZES
  1.360683917999 s,  0.614298139607 % : MEM COPIES:HOST -> DEV
  0.000086545944 s,  0.000039072272 % : KERNEL 1
  0.000045299530 s,  0.000020451052 % : KERNEL 2
  0.005482196808 s,  0.002475007792 % : MEM COPY  :DEV -> HOST
  0.000015974045 s,  0.000007211687 % : GPU FINAL SYNCHRONIZATION 
  0.000017166138 s,  0.000007749872 % : CUDA FREES

CPU:
0.000015974044800 s, 0.000007211686618 %: INIT
219.392010927200317 s, 99.047326402809205 %: RANDOMIZE
0.235200881958008 s, 0.106184443212263 %: CPU FREEs


221.502203941345 s : TOTAL TIME
  1.366331100464 s : GPU TOTAL TIME
=======================================================================

