cd ~/DIPLOMA/rodinia_3.1/cuda/nw
make clean
make

echo -e "\n\n\n512x512:\n"
./needle 512 10
echo -e "\n====================================\n"

echo -e "\n\n\n1024x1024:\n"
./needle 1024 10
echo -e "\n====================================\n"

echo -e "\n\n\n:2048x2048\n"
./needle 2048 10
echo -e "\n====================================\n"

echo -e "\n\n\n:4096x4096\n"
./needle 4096 10
echo -e "\n====================================\n"

echo -e "\n\n\n:8192x8192\n"
./needle 8192 10
echo -e "\n====================================\n"

echo -e "\n\n\n:16384x16384\n"
./needle 16384 10
echo -e "\n====================================\n"


