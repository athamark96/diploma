rm particlefilter_naive particlefilter_float
rm: cannot remove `particlefilter_naive': No such file or directory
rm: cannot remove `particlefilter_float': No such file or directory
make: *** [clean] Error 1
/usr/local/cuda/bin/nvcc -I/usr/local/cuda/include -L/usr/local/cuda/lib64 -lcuda -g -lm -O3 -use_fast_math -arch sm_35 ex_particle_CUDA_naive_seq.cu -o particlefilter_naive
ex_particle_CUDA_naive_seq.cu(622): warning: conversion from a string literal to "char *" is deprecated

ex_particle_CUDA_naive_seq.cu(622): warning: conversion from a string literal to "char *" is deprecated

/usr/local/cuda/bin/nvcc -I/usr/local/cuda/include -L/usr/local/cuda/lib64 -lcuda -g -lm -O3 -use_fast_math -arch sm_35 ex_particle_CUDA_float_seq.cu -o particlefilter_float
ex_particle_CUDA_float_seq.cu(721): warning: variable "indX" was declared but never referenced

ex_particle_CUDA_float_seq.cu(721): warning: variable "indY" was declared but never referenced

ex_particle_CUDA_float_seq.cu(806): warning: conversion from a string literal to "char *" is deprecated

ex_particle_CUDA_float_seq.cu(721): warning: variable "indX" was declared but never referenced

ex_particle_CUDA_float_seq.cu(721): warning: variable "indY" was declared but never referenced

ex_particle_CUDA_float_seq.cu(806): warning: conversion from a string literal to "char *" is deprecated

[[QUADRO ]] particle filter z=2: 




float | Block Size = 512, Npartiles=10K

-----------------------------------------------------------------------

VIDEO SEQUENCE TOOK 0.004464
TIME TO SEND TO GPU: 0.000189
GPU Execution: 0.008306
FREE TIME: 0.001191
TIME TO SEND BACK: 0.001548
SEND ARRAY X BACK: 0.000150
SEND ARRAY Y BACK: 0.000128
SEND WEIGHTS BACK: 0.000079
XE: 64.534101
YE: 64.422823
0.681207
PARTICLE FILTER TOOK 0.293606
ENTIRE PROGRAM TOOK 0.298070
=======================================================================




float | Block Size = 512, Npartiles=30K

-----------------------------------------------------------------------

VIDEO SEQUENCE TOOK 0.004087
TIME TO SEND TO GPU: 0.000690
GPU Execution: 0.030079
FREE TIME: 0.001503
TIME TO SEND BACK: 0.001991
SEND ARRAY X BACK: 0.000190
SEND ARRAY Y BACK: 0.000180
SEND WEIGHTS BACK: 0.000118
XE: 64.456690
YE: 64.414024
0.616427
PARTICLE FILTER TOOK 0.163025
ENTIRE PROGRAM TOOK 0.167112
=======================================================================




float | Block Size = 512, Npartiles=100K

-----------------------------------------------------------------------

VIDEO SEQUENCE TOOK 0.006053
TIME TO SEND TO GPU: 0.001868
GPU Execution: 0.190356
FREE TIME: 0.004533
TIME TO SEND BACK: 0.006073
SEND ARRAY X BACK: 0.000594
SEND ARRAY Y BACK: 0.000576
SEND WEIGHTS BACK: 0.000370
XE: 64.495629
YE: 64.442430
0.664373
PARTICLE FILTER TOOK 0.328272
ENTIRE PROGRAM TOOK 0.334325
=======================================================================




float | Block Size = 512, Npartiles=500k

-----------------------------------------------------------------------

VIDEO SEQUENCE TOOK 0.003863
TIME TO SEND TO GPU: 0.004666
GPU Execution: 3.567253
FREE TIME: 0.012756
TIME TO SEND BACK: 0.018116
SEND ARRAY X BACK: 0.002056
SEND ARRAY Y BACK: 0.001986
SEND WEIGHTS BACK: 0.001318
XE: 64.501377
YE: 64.452500
0.675378
PARTICLE FILTER TOOK 3.731383
ENTIRE PROGRAM TOOK 3.735246
=======================================================================




float | Block Size = 512, Npartiles=1M

-----------------------------------------------------------------------

VIDEO SEQUENCE TOOK 0.003870
TIME TO SEND TO GPU: 0.009274
GPU Execution: 13.732878
FREE TIME: 0.014924
TIME TO SEND BACK: 0.024454
SEND ARRAY X BACK: 0.003563
SEND ARRAY Y BACK: 0.003477
SEND WEIGHTS BACK: 0.002490
XE: 64.500619
YE: 64.451411
0.674086
PARTICLE FILTER TOOK 13.925646
ENTIRE PROGRAM TOOK 13.929516
=======================================================================


