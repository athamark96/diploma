#!/bin/bash
cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/bfs
make clean

echo -e "QUADRO M4000 measurements\n\n\n\n"

## RUN for kern_dim in 256 512 1024  
	make 

        echo -e "kernel_size: 256\n"

	echo -e "---------------------------\n"
	echo -e "for graph4096.txt\n--------------------------\n"
	time ./bfs ../../data/bfs/graph4096.txt

	echo -e "---------------------------\n"
	echo -e "for graph65536.txt\n--------------------------\n"
	time ./bfs ../../data/bfs/graph65536.txt
	echo -e "---------------------------\n"

	echo -e "for graph1MW_6.txt:\n"
	echo -e "---------------------------\n"
	time ./bfs ../../data/bfs/graph1MW_6.txt

        echo -e "\n===================================================\n"
        echo -e "++++++++++++++++++++++++++++++++++++++++++++++++++\n"
	echo -e "++++++++++++++++++++++++++++++++++++++++++++++++++\n"
	make clean	
        echo -e "++++++++++++++++++++++++++++++++++++++++++++++++++\n"
        echo -e "++++++++++++++++++++++++++++++++++++++++++++++++++\n"
        echo -e "==================================================\n"


