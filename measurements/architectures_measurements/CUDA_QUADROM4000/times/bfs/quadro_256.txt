rm -f bfs bfs.linkinfo result.txt
QUADRO M4000 measurements




/usr/local/cuda/bin/nvcc bfs.cu -o bfs -I/usr/local/cuda/include -L/usr/local/cuda/lib64 
kernel_size: 256

---------------------------

for graph4096.txt
--------------------------

Reading File
Read File
Copied Everything to GPU memory
Start traversing the tree
Kernel Executed 8 times
Result stored in result.txt

real	0m2.623s
user	0m0.016s
sys	0m0.728s
---------------------------

for graph65536.txt
--------------------------

Reading File
Read File
Copied Everything to GPU memory
Start traversing the tree
Kernel Executed 10 times
Result stored in result.txt

real	0m2.793s
user	0m0.068s
sys	0m0.676s
---------------------------

for graph1MW_6.txt:

---------------------------

Reading File
Read File
Copied Everything to GPU memory
Start traversing the tree
Kernel Executed 12 times
Result stored in result.txt

real	0m4.015s
user	0m1.060s
sys	0m0.840s

===================================================

++++++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++++++++++++++++++++++++++++++++++++++

rm -f bfs bfs.linkinfo result.txt
++++++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++++++++++++++++++++++++++++++++++++++

==================================================

