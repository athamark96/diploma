
cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/streamcluster
export CUDA_VISIBLE_DEVICES=1
make clean
make

echo -e "\n\nBlock Size 256,\nMax threads(grid size) set to: 65535\n\n"
echo -e "-----------------------------------------------------------------------\n"
./sc_gpu 10 20 256 65536 65536 1000 none output.txt 1
echo -e "=======================================================================\n"

echo -e "\n\nBlock Size 256,\nMax threads(grid size) set to: 65535\n\n"
echo -e "-----------------------------------------------------------------------\n"
./sc_gpu 10 20 256 131072 65536 1000 none output.txt 1
echo -e "=======================================================================\n"

echo -e "\n\nBlock Size 256,\nMax threads(grid size) set to: 65535\n\n"
echo -e "-----------------------------------------------------------------------\n"
./sc_gpu 10 20 256 196608 65536 1000 none output.txt 1
echo -e "=======================================================================\n"

echo -e "\n\nBlock Size 256,\nMax threads(grid size) set to: 65535\n\n"
echo -e "-----------------------------------------------------------------------\n"
./sc_gpu 10 20 256 262144 65536 1000 none output.txt 1
echo -e "=======================================================================\n"


