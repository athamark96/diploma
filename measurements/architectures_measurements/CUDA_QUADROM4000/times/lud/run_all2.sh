cd ~/DIPLOMA/rodinia_3.1/cuda/lud

export CUDA_VISIBLE_DEVICE=1

make clean
make KERNEL_DIM="-DRD_WG_SIZE_0=16"

echo -e "==============================\n"
echo -e "WG= 16x16\n\n\n"
echo -e "\t64:\n"
cuda/lud_cuda -s 64 -v
echo -e "---------------------\n"


echo -e "\t256:\n"
cuda/lud_cuda -s 256 -v
echo -e "---------------------\n"

echo -e "\t512:\n"
cuda/lud_cuda -s 512 -v
echo -e "---------------------\n"
 
echo -e "\t1024:\n"
cuda/lud_cuda -s 1024 -v
echo -e "---------------------\n"

echo -e "\t2048:\n"
cuda/lud_cuda -s 2048 -v
echo -e "---------------------\n"

echo -e "\t4096:\n"
cuda/lud_cuda -s 4096 -v
echo -e "---------------------\n"


#echo -e "\t8192:\n"
#cuda/lud_cuda -s 8192 -v
#echo -e "---------------------\n"


