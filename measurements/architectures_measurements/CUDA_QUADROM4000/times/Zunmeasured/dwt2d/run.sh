#!/bin/bash

## Give the Job a descriptive name
#PBS -N dwt2d

## Output and error files
#PBS -o dwt2d.out
#PBS -e dwt2d.time

## Limit memory, runtime etc.
#PBS -l walltime=40:00:00

cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/dwt2d
echo -e "RGB_color_solid_cube_1.bmp 1024x1024:\n"
time ./dwt RGB_color_solid_cube_1.bmp -d 1024x1024 -f -5
echo -e "-------------------------------------\n"

echo -e "192.bmp 192x192:\n"
time ./dwt2d 192.bmp -d 192x192 -f -5 -l 3
echo -e "-------------------------------------\n"

echo -e "rgb.bmp 1024x1024:\n"
time ./dwt2d rgb.bmp -d 1024x1024 -f -5 -l 3
echo -e "-------------------------------------\n"
