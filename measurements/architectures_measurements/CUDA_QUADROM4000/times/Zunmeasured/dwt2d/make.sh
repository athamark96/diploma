#!/bin/bash

## Give the Job a descriptive name
#PBS -N dwt2d

## Output and error files
#PBS -o make.out
#PBS -e make.err

## How many machines should we get?
#PBS -l nodes=dungani

cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/dwt2d

##make clean
##make OUTPUT=Y
make
