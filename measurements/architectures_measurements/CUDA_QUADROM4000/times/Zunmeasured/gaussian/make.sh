#!/bin/bash

## Give the Job a descriptive name
#PBS -N make_1024-4

## Output and error files
#PBS -o make-1024-4.out
#PBS -e make-1024-4.err

## How many machines should we get?
#PBS -l nodes=dungani

## Start 
## Run make in the src folder (modify properly)
cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/gaussian

make clean
make KERNEL_DIM="-DRD_WG_SIZE_0=1024 -DRD_WG_SIZE_1=4 "

