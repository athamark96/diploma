#!/bin/bash

## Give the Job a descriptive name
#PBS -N gaussian-1024-4 

## Output and error files
#PBS -o 1024-4.out
#PBS -e 1024-4.time

## Limit memory, runtime etc.
#PBS -l walltime=40:00:00

cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/gaussian
echo -e "Results for given ./../../data/gaussian :\n matrix1024.txt  matrix16.txt  matrix208.txt  matrix3.txt  matrix4.txt\n"
echo -e "-------------------------------------\n matrix1024.txt:\n- - - - - - - - - - - - - - - - - - -\n"
./gaussian -f ./../../data/gaussian/matrix1024.txt -q
echo -e "-------------------------------------\n\n"


echo -e "matrix16.txt:\n- - - - - - - - - - - - - - - \n"
./gaussian -f ./../../data/gaussian/matrix16.txt -q
echo -e "-------------------------------------\n\n"

echo -e "matrix208.txt:\n- - - - - - - - - - - - - - - \n"
./gaussian -f ./../../data/gaussian/matrix208.txt -q
echo -e "-------------------------------------\n\n"

echo -e "matrix3.txt:\n- - - - - - - - - - - - - - - \n"
./gaussian -f ./../../data/gaussian/matrix3.txt -q
echo -e "-------------------------------------\n\n"

echo -e "matrix4.txt:\n- - - - - - - - - - - - - - - \n"
./gaussian -f ./../../data/gaussian/matrix4.txt -q
echo -e "-------------------------------------\n\n"

