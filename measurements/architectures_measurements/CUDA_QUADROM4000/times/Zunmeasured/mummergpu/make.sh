#!/bin/bash

## Give the Job a descriptive name
#PBS -N mummergpu

## Output and error files
#PBS -o make.out
#PBS -e make.err

## How many machines should we get?
#PBS -l nodes=dungani
cat $PROFILE

cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/mummergpu

make                                     
