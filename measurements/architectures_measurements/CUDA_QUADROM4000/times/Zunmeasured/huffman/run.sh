#!/bin/bash

## Give the Job a descriptive name
#PBS -N huffman 

## Output and error files
#PBS -o run-512.out
#PBS -e run.err

## Limit memory, runtime etc.
#PBS -l walltime=20:00:00

cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/huffman
export CUDA_VISIBLE_DEVICES=2

echo -e "Default settings results:\n"
./pavle ../../data/huffman/test1024_H2.206587175259.in 

