#!/bin/bash

## Give the Job a descriptive name
#PBS -N huffman

## Output and error files
#PBS -o make.out
#PBS -e make.err

## How many machines should we get?
#PBS -l nodes=dungani

cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/huffman

export CUDA_VISIBLE_DEVICES=2
echo $CUDA_VISIBLE_DEVICES

make clean
make 


