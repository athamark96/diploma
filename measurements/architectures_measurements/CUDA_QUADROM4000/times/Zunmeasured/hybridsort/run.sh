#!/bin/bash

## Give the Job a descriptive name
#PBS -N hybridsort 

## Output and error files
#PBS -o run-default.out
#PBS -e run-default.err

## Limit memory, runtime etc.
#PBS -l walltime=80:00:00

cd /home/users/amarkou/DIPLOMA/rodinia_3.1/cuda/hubridsort
export CUDA_VISIBLE_DEVICES=2

echo -e "~~~~~~~~~~~~Default settings results:~~~~~~~~~~~~~\n"
echo -e "--------------------------------------------------\n"

echo -e "Random Input of 4194304 floats:\n"
./hybridsort r
echo -e "--------------------------------------------------\n"

echo -e "Specified Input:(../../data/500000.txt)\n"
./hybridsort ./../../data/500000..txt
echo -e "--------------------------------------------------\n"


