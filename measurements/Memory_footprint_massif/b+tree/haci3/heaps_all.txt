dataset1:

#-----------
snapshot=51
#-----------
time=11877557638
mem_heap_B=77849869
mem_heap_extra_B=20146363
mem_stacks_B=0
heap_tree=peak

	TOTAL BYTES USED = 77849869 + 20146363 = 97996232 bytes (~ 93,45MB)

------------------------------------------------------------------------------

dataset2:

#-----------
snapshot=51
#-----------
time=24169411266
mem_heap_B=101561873
mem_heap_extra_B=20146359
mem_stacks_B=0
heap_tree=peak

	TOTAL BYTES USED = 101561873 + 20146359 = 121708232 bytes (~116.1MB)

------------------------------------------------------------------------------

dataset3:

#-----------
snapshot=52
#-----------
time=36485440409
mem_heap_B=125561875
mem_heap_extra_B=20160437
mem_stacks_B=0
heap_tree=peak

	TOTAL BYTES USED = 125561875 + 20160437 = 145722312 bytes (~139.0MB)

------------------------------------------------------------------------------
