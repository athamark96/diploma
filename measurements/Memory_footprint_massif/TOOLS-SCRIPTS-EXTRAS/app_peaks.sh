#!/bin/bash
##enter the folder you want to find the peak for each dataset
## for example: ~/path_to_Memory_footprin/Memory_footprint/bfs/haci3

z=1

for li in massif.out.*
do
	echo -e "dataset$z\n"
        echo -e "file:$li\n"
	~/DIPLOMA_2019_2020/diploma/measurements/Memory_footprint_massif/massif_peak.py $li
	let "z+=1"
	echo -e "---------------------------------------\n"
done
