#!/usr/bin/python

import sys, csv

ifile = sys.argv[1] 

str1  = "peak"
check = 0

with open(ifile) as fp:
    for i,line in enumerate(fp):
        if str1 in line:
            check =i
            break

total_bytes = 0
mem_heap_B  = 0
mem_heap_extra_B = 0

with open(ifile) as fp:
    for j,line in enumerate(fp):
        if j == check-6:
            print line
        elif j == check-3:
            print line
            x = line.split("=")
            mem_heap_B = int(x[1])
            total_bytes = mem_heap_B
        elif j==check-2:
            print line
            x = line.split("=")
            mem_heap_extra_B = int(x[1])
            total_bytes = total_bytes + mem_heap_extra_B
            break


print "Total:", total_bytes, "bytes, (~", format(total_bytes/(1024*1024*1.0),'.2f'), "MB)"

