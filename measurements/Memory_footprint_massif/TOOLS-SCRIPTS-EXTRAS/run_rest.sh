cd ~/DIPLOMA/rodinia_3.1/openmp/memory_footprint/haci3/

cd ./lud
make
./run.sh
cd ..
echo "lud DONE"

cd ./myocyte
make
./run_valgrind.sh
cd ..
echo "myocyte DONE"

cd ./nn
make
./run_valgrind.sh
cd ..
echo "nn DONE"

cd ./nw
make
./run_valgrind.sh
cd ..
echo "nw DONE"

cd ./particlefilter
make
./run_valgrind.sh
cd ..
echo "particlefilter DONE"

cd ./pathfinder
make
./run_valgrind.sh
cd ..
echo "pathfinder DONE"

cd ./streamcluster
make
./run_valgrind.sh
cd ..
echo "streamcluster DONE"

exit
