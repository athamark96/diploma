dataset1

file:massif.out.37267

snapshot=4

mem_heap_B=2107256

mem_heap_extra_B=88

Total: 2107344 bytes, (~ 2.01 MB)
---------------------------------------

dataset2

file:massif.out.37269

snapshot=4

mem_heap_B=8406904

mem_heap_extra_B=8120

Total: 8415024 bytes, (~ 8.03 MB)
---------------------------------------

dataset3

file:massif.out.37271

snapshot=3

mem_heap_B=33589112

mem_heap_extra_B=8120

Total: 33597232 bytes, (~ 32.04 MB)
---------------------------------------

dataset4

file:massif.out.37273

snapshot=3

mem_heap_B=134285176

mem_heap_extra_B=8120

Total: 134293296 bytes, (~ 128.07 MB)
---------------------------------------

dataset5

file:massif.out.37275

snapshot=3

mem_heap_B=537003896

mem_heap_extra_B=8120

Total: 537012016 bytes, (~ 512.13 MB)
---------------------------------------

dataset6

file:massif.out.37277

snapshot=3

mem_heap_B=2147747704

mem_heap_extra_B=8120

Total: 2147755824 bytes, (~ 2048.26 MB)
---------------------------------------

