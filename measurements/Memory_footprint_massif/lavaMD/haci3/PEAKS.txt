dataset1

file:massif.out.18729

snapshot=12

mem_heap_B=7857904

mem_heap_extra_B=80

Total: 7857984 bytes, (~ 7.49 MB)
---------------------------------------

dataset2

file:massif.out.18732

snapshot=12

mem_heap_B=62849904

mem_heap_extra_B=13136

Total: 62863040 bytes, (~ 59.95 MB)
---------------------------------------

dataset3

file:massif.out.18743

snapshot=12

mem_heap_B=212113904

mem_heap_extra_B=7376

Total: 212121280 bytes, (~ 202.29 MB)
---------------------------------------

dataset4

file:massif.out.18809

snapshot=12

mem_heap_B=502785904

mem_heap_extra_B=16208

Total: 502802112 bytes, (~ 479.51 MB)
---------------------------------------

dataset5

file:massif.out.19448

snapshot=12

mem_heap_B=982001904

mem_heap_extra_B=11728

Total: 982013632 bytes, (~ 936.52 MB)
---------------------------------------

