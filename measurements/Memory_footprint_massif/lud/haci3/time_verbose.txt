=====================================

PROBLEM SIZE=64

--------------------------------

    with OMP_NUM_THREADS= 1 
 

==26540== Massif, a heap profiler
==26540== Copyright (C) 2003-2013, and GNU GPL'd, by Nicholas Nethercote
==26540== Using Valgrind-3.10.0 and LibVEX; rerun with -h for copyright info
==26540== Command: ./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
==26540== 
Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/64.dat
running OMP on host
Time consumed(ms): 9.751000
==26540== 
====================================

PROBLEM SIZE=256

--------------------------------

    with OMP_NUM_THREADS= 1 
 

==26542== Massif, a heap profiler
==26542== Copyright (C) 2003-2013, and GNU GPL'd, by Nicholas Nethercote
==26542== Using Valgrind-3.10.0 and LibVEX; rerun with -h for copyright info
==26542== Command: ./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
==26542== 
Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/256.dat
running OMP on host
Time consumed(ms): 27.643000
==26542== 
====================================

PROBLEM SIZE=512

--------------------------------

    with OMP_NUM_THREADS= 1 
 

==26544== Massif, a heap profiler
==26544== Copyright (C) 2003-2013, and GNU GPL'd, by Nicholas Nethercote
==26544== Using Valgrind-3.10.0 and LibVEX; rerun with -h for copyright info
==26544== Command: ./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
==26544== 
Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/512.dat
running OMP on host
Time consumed(ms): 133.533000
==26544== 
====================================

PROBLEM SIZE=2048

--------------------------------

    with OMP_NUM_THREADS= 1 
 

==26546== Massif, a heap profiler
==26546== Copyright (C) 2003-2013, and GNU GPL'd, by Nicholas Nethercote
==26546== Using Valgrind-3.10.0 and LibVEX; rerun with -h for copyright info
==26546== Command: ./omp/lud_omp -n 1 -i /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
==26546== 
Reading matrix from file /home/users/amarkou/DIPLOMA/rodinia_3.1/data/lud/2048.dat
running OMP on host
Time consumed(ms): 6821.933000
==26546== 
